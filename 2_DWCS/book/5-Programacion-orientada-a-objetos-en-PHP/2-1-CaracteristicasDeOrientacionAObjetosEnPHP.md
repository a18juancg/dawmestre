# Características de orientación a objetos en PHP

La programación orientada a objetos (POO, o OOP en lenguaje inglés), es una metodología de programación basada en objetos. Un objeto es una estructura que contiene datos y el código que los maneja.

La estructura de los objetos se define en las clases. En ellas se escribe el código que define el comportamiento de los objetos y se indican los miembros que formarán parte de los objetos de dicha clase. Entre los miembros de una clase puede haber:

* **Métodos**. Son los miembros de la clase que contienen el código de la misma. Un método es como una función. Puede recibir parámetros y devolver valores.

* **Atributos o propiedades**. Almacenan información acerca del estado del objeto al que pertenecen (y por tanto, su valor puede ser distinto para cada uno de los objetos de la misma clase).

A la creación de un objeto basado en una clase se le llama instanciar una clase y al objeto obtenido también se le conoce como instancia de esa clase. 

Los pilares fundamentales de la POO son:

* **Herencia**. Es el proceso de crear una clase a partir de otra, heredando su comportamiento y características y pudiendo redefinirlos.

* **Abstracción**. Hace referencia a que cada clase oculta en su interior las peculiaridades de su implementación, y presenta al exterior una serie de métodos (interface) cuyo comportamiento está bien definido. Visto desde el exterior, cada objeto es un ente abstracto que realiza un trabajo.

* **Polimorfismo**. Un mismo método puede tener comportamientos distintos en función del objeto con que se utilice.

* **Encapsulación**. En la POO se juntan en un mismo lugar los datos y el código que los manipula.

Las ventajas más importantes que aporta la programación orientada a objetos son:

* **Modularidad**. La POO permite dividir los programas en partes o módulos más pequeños, que son independientes unos de otros pero pueden comunicarse entre ellos.

* **Extensibilidad**. Si se desean añadir nuevas características a una aplicación, la POO facilita esta tarea de dos formas: añadiendo nuevos métodos al código, o creando nuevos objetos que extiendan el comportamiento de los ya existentes.

* **Mantenimiento**. Los programas desarrollados utilizando POO son más sencillos de mantener, debido a la modularidad antes comentada. También ayuda seguir ciertas convenciones al escribirlos, por ejemplo, escribir cada clase en un fichero propio. No debe haber dos clases en un mismo fichero, ni otro código aparte del propio de la clase.

Seguramente todo, o la mayoría de lo que acabas de ver, ya lo conocías, y es incluso probable que sepas utilizar algún lenguaje de programación orientado a objetos, así que vamos a ver directamente las peculiaridades propias de PHP en lo que hace referencia a la POO.

Como ya has visto en las unidades anteriores, especialmente con las extensiones para utilizar bases de datos, con PHP puedes utilizar dos estilos de programación: estructurada y orientada a objetos.

```php
// utilizando programación estructurada
$dwes = mysqli_connect('localhost', 'dwes', 'abc123.', 'dwes'); // utilizando POO
$dwes = new mysqli();
$dwes->connect('localhost', 'dwes', 'abc123.', 'dwes');
```

Sin embargo, el lenguaje PHP original no se diseñó con características de orientación a objetos. Sólo a partir de la versión 3, se empezaron a introducir algunos rasgos de POO en el lenguaje. Esto se potenció en la versión 4, aunque todavía de forma muy rudimentaria. Por ejemplo, en PHP4:

* Los objetos se pasan siempre por valor, no por referencia.
* No se puede definir el nivel de acceso para los miembros de la clase. Todos son públicos. 
* No existen los interfaces.
* No existen métodos destructores.

En la versión actual, PHP5, se ha reescrito y potenciado el soporte de orientación a objetos del lenguaje, ampliando sus características y mejorando su rendimiento y su funcionamiento general. Aunque iremos detallando y explicando cada una posteriormente con detenimiento, las características de POO que soporta PHP5 incluyen:

* Métodos estáticos.
* Métodos constructores y destructores. 
* Herencia.
* Interfaces.
* Clases abstractas.

Entre las características que no incluye PHP5, y que puedes conocer de otros lenguajes de programación, están:

* Herencia múltiple.
* Sobrecarga de métodos (incluidos los métodos constructores). Sobrecarga de operadores.


## Creación de clases

La declaración de una clase en PHP se hace utilizando la palabra `class`. A continuación y entre llaves, deben figurar los miembros de la clase. Conviene hacerlo de forma ordenada, primero las propiedades o atributos, y después los métodos, cada uno con su código respectivo.

```php
class Producto { 
	private $codigo; 
	public $nombre; 
	public $PVP;
	
	public function muestra() {
		print "<p>" . $this->codigo . "</p>";
	} 
}
```

Como comentábamos antes, es preferible que cada clase figure en su propio fichero (`producto.php`). Además, muchos programadores prefieren utilizar para las clases nombres que comiencen por letra mayúscula, para, de esta forma, distinguirlos de los objetos y otras variables.

Una vez definida la clase, podemos usar la palabra `new` para instanciar objetos de la siguiente forma:

```php
$p = new Producto();
```

Para que la línea anterior se ejecute sin error, previamente debemos haber declarado la clase. Para ello, en ese mismo fichero tendrás que incluir la clase poniendo algo como:

```php
require_once('producto.php');
```

Los atributos de una clase son similares a las variables de PHP. Es posible indicar un valor en la declaración de la clase. En este caso, todos los objetos que se instancien a partir de esa clase, partirán con ese valor por defecto en el atributo.

Para acceder desde un objeto a sus atributos o a los métodos de la clase, debes utilizar el operador flecha (fíjate que sólo se pone el símbolo `$` delante del nombre del objeto):

```php
$p->nombre = 'Samsung Galaxy S'; 
$p->muestra();
```

Cuando se declara un atributo, se debe indicar su nivel de acceso. Los principales niveles son:

* **public**. Los atributos declarados como `public` pueden utilizarse directamente por los objetos de la clase. Es el caso del atributo `$nombre` anterior.

* **private**. Los atributos declarados como `private` sólo pueden ser accedidos y modificados por los métodos definidos en la clase, no directamente por los objetos de la misma. Es el caso del atributo `$codigo`.

En PHP4 no se podía definir nivel de acceso para los atributos de una clase, por lo que todos se precedían de la palabra `var`.

Hoy en día, aunque aún es aceptado por PHP5, no se recomienda su uso. Si encuentras algún código que lo utilice, ten en cuenta que tiene el mismo efecto que `public`.

---

Uno de los motivos para crear atributos privados es que su valor forma parte de la información interna del objeto y no debe formar parte de su interface. Otro motivo es mantener cierto control sobre sus posibles valores.

Por ejemplo, no quieres que se pueda cambiar libremente el valor del código de un producto. O necesitas conocer cuál será el nuevo valor antes de asignarlo. En estos casos, se suelen definir esos atributos como privados y además se crean dentro de la clase métodos para permitirnos obtener y/o modificar los valores de esos atributos. Por ejemplo:

```php
private $codigo;

public function setCodigo($nuevo_codigo) {
	if (noExisteCodigo($nuevo_codigo)) { 
		$this->codigo = $nuevo_codigo;
		return true;
	}
	return false;
}

public function getCodigo() { 
	return $this->codigo;
}
```

Aunque no es obligatorio, el nombre del método que nos permite obtener el valor de un atributo suele empezar por `get`, y el que nos permite modificarlo por `set`.

En PHP5 se introdujeron los llamados *métodos mágicos*, entre ellos [`__set`](https://www.php.net/manual/es/language.oop5.overloading.php#object.set) y [`__get`](https://www.php.net/manual/es/language.oop5.overloading.php#object.get). Si se declaran estos dos métodos en una clase, PHP los invoca automáticamente cuando desde un objeto se intenta usar un atributo no existente o no accesible. Por ejemplo, el código siguiente simula que la clase Producto tiene cualquier atributo que queramos usar.

```php
class Producto {
	private $atributos = array();

	public function __get($atributo) { 
		return $this->atributos[$atributo];
	}

	public function __set($atributo, $valor) {
		$this->atributos[$atributo] = $valor; 
	}
}
```

En la documentación de PHP tienes más información sobre los [*métodos mágicos*](https://www.php.net/manual/es/language.oop5.magic.php).

---

Cuando desde un objeto se invoca un método de la clase, a éste se le pasa siempre una referencia al objeto que hizo la llamada. Esta referencia se almacena en la variable `$this`. Se utiliza, por ejemplo, en el código anterior para tener acceso a los atributos privados del objeto (que sólo son accesibles desde los métodos de la clase).

```php
print "<p>" . $this->codigo . "</p>";
```

> Una [referencia](https://www.php.net/manual/es/language.references.php) es una forma de utilizar distintos nombres de variables para acceder al mismo contenido. En los puntos siguientes aprenderás a crearlas y a utilizarlas.

Además de métodos y propiedades, en una clase también se pueden definir **constantes**, utilizando la palabra `const`. Es importante que no confundas los atributos con las constantes. Son conceptos distintos: las constantes no pueden cambiar su valor (obviamente, de ahí su nombre), no usan el carácter `$` y, además, su valor va siempre entre comillas y está asociado a la clase, es decir, no existe una copia del mismo en cada objeto. Por tanto, para acceder a las constantes de una clase, se debe utilizar el nombre de la clase y el operador `::`, llamado **operador de resolución de ámbito** (que se utiliza para acceder a los elementos de una clase).

```php
class DB {
	const USUARIO = 'dwes'; ...
}
echo DB::USUARIO;
```

Es importante resaltar que no es necesario que exista ningún objeto de una clase para poder acceder al valor de las constantes que defina. Además, sus nombres suelen escribirse en mayúsculas.

Tampoco se deben confundir las constantes con los miembros estáticos de una clase. En PHP5, una clase puede tener atributos o métodos estáticos, también llamados a veces atributos o métodos de clase. Se definen utilizando la palabra clave `static`.

```php
class Producto {
	private static $num_productos = 0;
	
	public static function nuevoProducto() {
		self::$num_productos++; 
	}
	
	...
}
```

Los atributos y métodos estáticos no pueden ser llamados desde un objeto de la clase utilizando el operador `->`. Si el método o atributo es público, deberá accederse utilizando el nombre de la clase y el operador de resolución de ámbito.

```php
Producto::nuevoProducto();
```

Si es privado, como el atributo `$num_productos` en el ejemplo anterior, sólo se podrá acceder a él desde los métodos de la propia clase, utilizando la palabra `self`. De la misma forma que `$this` hace referencia **al objeto actual**, `self` hace referencia **a la clase actual**.

```php
self::$num_productos++;
```

Los atributos estáticos de una clase se utilizan para guardar información general sobre la misma, como puede ser el número de objetos que se han instanciado. Sólo existe un valor del atributo, que se almacena a nivel de clase.

Los métodos estáticos suelen realizar alguna tarea específica o devolver un objeto concreto. Por ejemplo, las clases matemáticas suelen tener métodos estáticos para realizar logaritmos o raíces cuadradas. No tiene sentido crear un objeto si lo único que queremos es realizar una operación matemática.

Los métodos estáticos se llaman desde la clase. No es posible llamarlos desde un objeto y por tanto, no podemos usar `$this` dentro de un método estático.

---

Como ya viste, para instanciar objetos de una clase se utiliza `new`:

```php
$p = new Producto();
```

En PHP5 puedes definir en las clases métodos constructores, que se ejecutan cuando se crea el objeto. El constructor de una clase debe llamarse `__construct`. Se pueden utilizar, por ejemplo, para asignar valores a atributos.

```php
class Producto {
	private static $num_productos = 0;
	private $codigo;

	public function __construct() { 
		self::$num_productos++;
	}
	...
}
```

El constructor de una clase puede llamar a otros métodos o tener parámetros, en cuyo caso deberán pasarse cuando se crea el objeto. Sin embargo, sólo puede haber un método constructor en cada clase.

```php
class Producto {
	private static $num_productos = 0;
	private $codigo;
	
	public function __construct($codigo) { 
		$this->$codigo = $codigo; 
		self::$num_productos++;
	}
	...
}

$p = new Producto('GALAXYS');
```

Por ejemplo, si como en este ejemplo, definimos un constructor en el que haya que pasar el código, siempre que instancies un nuevo objeto de esa clase tendrás que indicar su código.

> Una de las posibilidades de los [*métodos mágicos de PHP5*](https://www.php.net/manual/es/language.oop5.overloading.php#object.call) es utilizar `__call` para capturar llamadas a métodos que no estén implementados en la clase. Entonces, en función del nombre del método y del número de parámetros que se pasen, se podrían realizar unas acciones u otras.

También es posible definir un método destructor, que debe llamarse `__destruct` y permite definir acciones que se ejecutarán cuando se elimine el objeto.

```php
class Producto {
	private static $num_productos = 0; 
	private $codigo;

	public function __construct($codigo) { 
		$this->$codigo = $codigo; 
		self::	$num_productos++;
	}
	
	public function __destruct() { 
		self::$num_productos--;
	}
	...
}

$p = new Producto('GALAXYS');
```


Los métodos constructores también existen en PHP4, pero en lugar de llamarse `__construct`, se deben llamar del mismo modo que la clase. Los métodos destructores son nuevos en PHP5; no existían en versiones anteriores del lenguaje.

## Utilización de Objetos

Ya sabes cómo instanciar un objeto utilizando `new`, y cómo acceder a sus métodos y atributos públicos con el operador flecha:

```php
$p = new Producto();
$p->nombre = 'Samsung Galaxy S'; 
$p->muestra();
```

Una vez creado un objeto, puedes utilizar el operador `instanceof` para comprobar si es o no una instancia de una clase determinada.

```php
if ($p instanceof Producto) { 
	...
}
```

Además, en PHP5 se incluyen una serie de funciones útiles para el desarrollo de aplicaciones utilizando POO.

**Funciones de utilidad para objetos y clases en PHP5:**

| Función       | Ejemplo         | Significado   |
|:------------- |:---------------:| :-------------|
| `get_class`      			| `echo "La clase es: " . get_class($p);`                 | Devuelve el nombre de la clase del objeto. |
| `class_exists`     		| `if (class_exists('Producto') { ... }`                  | Devuelve `true` si la clase está definida o `false` en caso contrario. |
| `get_declared_classes` 	| `print_r(get_declared_classes());`                      | Devuelve un array con los nombres de las clases definidas. |
| `class_alias` 				| `class_alias('Producto', 'Articulo'); new Articulo();`  | Crea un alias para una clase. |
| `get_class_methods` 		| `print_r(get_class_methods('Producto'));`               | Devuelve un array con los nombres de los métodos de una clase que son accesibles desde dónde se hace la llamada. |
| `method_exists` 			| `if (method_exists('Producto', 'vende') { ... }`        | Devuelve `true` si existe el método en el objeto o la clase que se indica, o `false` en caso contrario, independientemente de si es accesible o no. |
| `get_class_vars` 			| `print_r(get_class_vars('Producto'));`                  | Devuelve un array con los nombres de los atributos de una clase que son accesibles desde dónde se hace la llamada. |
| `get_object_vars` 		| `print_r(get_object_vars($p));`                         | Devuelve un array con los nombres de los métodos de un objeto que son accesibles desde dónde se hace la llamada. |
| `property_exists` 		| `if (property_exists('Producto', 'codigo') { ... }`     | Devuelve true si existe el atributo en el objeto o la clase que se indica, o false en caso contrario, independientemente de si es accesible o no. |

Desde PHP5, puedes indicar en las funciones y métodos de qué clase deben ser los objetos que se pasen como parámetros. Para ello, debes especificar el tipo antes del parámetro.

```php
public function vendeProducto(Producto $p) { 
	...
}
```

Si cuando se realiza la llamada, el parámetro no es del tipo adecuado, se produce un error que podrías capturar. Además, ten en cuenta que sólo funciona con objetos (y a partir de `PHP5.1` también con arrays).

---

Una característica de la POO que debes tener muy en cuenta es qué sucede con los objetos cuando los pasas a una función, o simplemente cuando ejecutas un código como el siguiente:

```php
$p = new Producto();
$p->nombre = 'Samsung Galaxy S'; 
$a = $p;
```

En PHP4, la última línea del código anterior crea un nuevo objeto con los mismos valores del original, de la misma forma que se copia cualquier otro tipo de variable. Si después de hacer la copia se modifica, por ejemplo, el atributo 'nombre' de uno de los objetos, el otro objeto no se vería modificado.

Sin embargo, en PHP5 este comportamiento varía. El código anterior simplemente crearía un **nuevo identificador del mismo objeto**. Esto es, en cuanto se utilice uno cualquiera de los identificadores para cambiar el valor de algún atributo, este cambio se vería también reflejado al acceder utilizando el otro identificador. Recuerda que, aunque haya dos o más identificadores del mismo objeto, en realidad todos se refieren a la única copia que se almacena del mismo.

Para crear nuevos identificadores en PHP5 a un objeto ya existente, se utiliza el operador `=`. Sin embargo, como ya sabes, este operador aplicado a variables de otros tipos, crea una copia de la misma. En PHP puedes crear referencias a variables (como números enteros o cadenas de texto), utilizando el operador `&`:

```
$a = 'Samsung Galaxy S'; 
$b = &$a;
```

En el ejemplo anterior, `$b` es una referencia a la variable `$a`. Cuando se cambia el valor de una de ellas, este cambio se refleja en la otra.

[Las referencias](https://www.php.net/manual/es/language.references.php) se pueden utilizar para pasarlas como parámetros a las funciones. Si utilizamos el operador & junto al parámetro, en lugar de pasar
una copia de la variable, se pasa una referencia a la misma.

```php
function suma(&$v) { 
	$v++;
}

$a = 3;
suma ($a);
echo $a; // Muestra 4
```

De esta forma, dentro de la función se puede modificar el contenido de la variable que se pasa, no el de una copia.

Por tanto, a partir de PHP5 no puedes copiar un objeto utilizando el operador =. Si necesitas copiar un objeto, debes utilizar clone. Al utilizar clone sobre un objeto existente, se crea una copia de todos los atributos del mismo en un nuevo objeto.

```php
$p = new Producto();
$p->nombre = 'Samsung Galaxy S'; 
$a = clone($p);
```

Además, existe una forma sencilla de personalizar la copia para cada clase particular. Por ejemplo, puede suceder que quieras copiar todos los atributos menos alguno. En nuestro ejemplo, al menos el código de cada producto debe ser distinto y, por tanto, quizás no tenga sentido copiarlo al crear un nuevo objeto. Si éste fuera el caso, puedes crear un método de nombre `__clone` en la clase. Este método se llamará automáticamente después de copiar todos los atributos en el nuevo objeto.

```php
class Producto { ...
	public function __clone($atributo) { 
		$this->codigo = nuevo_codigo();
	}
	...
}
```

---

A veces tienes dos objetos y quieres saber su relación exacta. Para eso, en PHP5 puedes utilizar los operadores `==` y `===`.

Si utilizas el operador de comparación `==`, comparas los valores de los atributos de los objetos. Por tanto dos objetos serán iguales si son instancias de la misma
clase y, además, sus atributos tienen los mismos valores.

```php
$p = new Producto();
$p->nombre = 'Samsung Galaxy S';
$a = clone($p);

// El resultado de comparar $a == $p da verdadero // pues $a y $p son dos copias idénticas
```

Sin embargo, si utilizas el operador `===`, el resultado de la comparación será `true` sólo cuando las dos variables sean referencias al mismo objeto.

```php
$p = new Producto();
$p->nombre = 'Samsung Galaxy S';
$a = clone($p);
// El resultado de comparar $a === $p da falso pues $a y $p no hacen referencia al mismo objeto
$a = & $p;
// Ahora el resultado de comparar $a === $p da verdadero // pues $a y $p son referencias al mismo objeto.
```


## Mecanismos de mantenimiento del estado

En la unidad anterior aprendiste a usar la sesión del usuario para almacenar el estado de las variables, y poder recuperarlo cuando sea necesario. El proceso es muy sencillo; se utiliza el array superglobal `$_SESSION`, añadiendo nuevos elementos para ir guardando la información en la sesión.

El procedimiento para almacenar objetos es similar, pero hay una diferencia importante. Todas las variables almacenan su información en memoria de una forma u otra según su tipo. Los objetos, sin embargo, no tienen un único tipo. Cada objeto tendrá unos atributos u otros en función de su clase. Por tanto, para almacenar los objetos en la sesión del usuario, hace falta convertirlos a un formato estándar. Este proceso se llama serialización.

En PHP, para serializar un objeto se utiliza la función `serialize`. El resultado obtenido es un string que contiene un flujo de bytes, en el que se encuentran definidos todos los valores del objeto.

```php
$p = new Producto(); 
$a = serialize($p);
```

```php
$p = unserialize($a);
```

> Las funciones *serialize* y *unserialize* se utilizan mucho con objetos, pero sirven para convertir en una cadena cualquier tipo de dato, excepto el tipo `resource`. Cuando se aplican a un objeto, convierten y recuperan toda la información del mismo, incluyendo sus atributos privados. La única información que **no se puede mantener** utilizando estas funciones es la que contienen los **atributos estáticos de las clases**.

Si simplemente queremos almacenar un objeto en la sesión del usuario, deberíamos hacer por tanto:

```php
session_start();
$_SESSION['producto'] = serialize($p);
```

Pero en PHP esto aún es más fácil. Los objetos que se añadan a la sesión del usuario son serializados automáticamente. Por tanto, no es necesario usar `serialize` ni `unserialize`.

```php
session_start(); 
$_SESSION['producto'] = $p;
```

Para poder deserializar un objeto, debe estar definida su clase. Al igual que antes, si lo recuperamos de la información almacenada en la sesión del usuario, no será necesario utilizar la función `unserialize`.

```php
session_start();
$p = $_SESSION['producto'];
```

> Como ya viste en el tema anterior, el mantenimiento de los datos en la sesión del usuario no es perfecta; tiene sus limitaciones. Si fuera necesario, es posible almacenar esta información en una base de datos. Para ello tendrás que usar las funciones `serialize` y `unserialize`, pues en este caso PHP ya no realiza la serialización automática.

En PHP además tienes la opción de personalizar el proceso de serialización y deserialización de un objeto, utilizando los métodos mágicos [`__sleep`](https://www.php.net/manual/es/language.oop5.magic.php#object.sleep) y [`__wakeup`](https://www.php.net/manual/es/language.oop5.magic.php#object.wakeup).

Si en la clase está definido un método con nombre `__sleep`, se ejecuta antes de serializar un objeto. Igualmente, si existe un método `__wakeup`, se ejecuta con cualquier llamada a la función `unserialize`.


## Herencia

La herencia es un mecanismo de la POO que nos permite definir nuevas clases en base a otra ya existente. Las nuevas clases que heredan también se conocen con el nombre de subclases. La clase de la que heredan se llama clase base o superclase.

Por ejemplo, en nuestra tienda web vamos a tener productos de distintos tipos. En principio hemos creado para manejarlos una clase llamada `Producto`, con algunos atributos y un método que genera una salida personalizada en formato HTML del código.


```php
class Producto {
	public $codigo;
	public $nombre; 
	public $nombre_corto; 
	public $PVP;
	
	public function muestra() {
		print "<p>" . $this->codigo . "</p>";
	} 
}
```

Esta clase es muy útil si la única información que tenemos de los distintos productos es la que se muestra arriba. Pero, si quieres personalizar la información que vas a tratar de cada tipo de producto (y almacenar, por ejemplo para los televisores, las pulgadas que tienen o su tecnología de fabricación), puedes crear nuevas clases que hereden de `Producto`. Por ejemplo, TV, Ordenador, Movil.

```php
class TV extends Producto { 
	public $pulgadas;
	public $tecnologia;
}
```

Como puedes ver, para definir una clase que herede de otra, simplemente tienes que utilizar la palabra `extends` indicando la *superclase*. Los nuevos objetos que se instancien a partir de la *subclase* son también objetos de la *clase base*; se puede comprobar utilizando el operador `instanceof`.

```php
$t = new TV();
if ($t instanceof Producto) {
	// Este código se ejecuta pues la condición es cierta 
}
```

Antes viste algunas funciones útiles para programar utilizando objetos y clases. Las de la siguiente tabla están además relacionadas con la herencia.

**Funciones de utilidad en la herencia en PHP5**

| Función       | Ejemplo         | Significado   |
|:------------- |:---------------:| :-------------|
| `get_parent_class` | `echo "La clase padre es: " . get_parent_class($t);`  | Devuelve el nombre de la clase padre del objeto o la clase que se indica. |
| `is_subclass_of`   | `if (is_subclass_of($t, 'Producto') { ... }`          | Devuelve `true` si el objeto o la clase del primer parámetro, tiene como clase base a la que se indica en el segundo parámetro, o `false` en caso contrario. |

---

La nueva clase hereda todos los atributos y métodos públicos de la clase base, pero no los privados. Si quieres crear en la clase base un método no visible al exterior (como los privados) que se herede a las subclases, debes utilizar la palabra `protected` en lugar de `private`. Además, puedes redefinir el comportamiento de los métodos existentes en la clase base, simplemente creando en la subclase un nuevo método con el mismo nombre.

```php
class TV extends Producto { 
	public $pulgadas;
	public $tecnologia;
	
	public function muestra() {
		print "<p>" . $this->pulgadas . " pulgadas</p>";
	} 
}
```

Existe una forma de evitar que las clases heredadas puedan redefinir el comportamiento de los métodos existentes en la superclase: utilizar la palabra final. Si en nuestro ejemplo hubiéramos hecho:

```php
class Producto {
	public $codigo;
	public $nombre; 
	public $nombre_corto; public $PVP;
	
	public final function muestra() {
		print "<p>" . $this->codigo . "</p>";
	} 
}
```

En este caso el método `muestra` no podría redefinirse en la clase `TV`.
Incluso se puede declarar una clase utilizando `final`. En este caso no se podrían crear clases heredadas utilizándola como base.

```php
final class Producto { ... }
```

Opuestamente al modificador `final`, existe también `abstract`. Se utiliza de la misma forma, tanto con métodos como con clases completas, pero en lugar de prohibir la herencia, obliga a que se herede. Es decir, una clase con el modificador `abstract` no puede tener objetos que la instancien, pero sí podrá utilizarse de clase base y sus subclases sí podrán utilizarse para instanciar objetos.

```php
abstract class Producto { ... }
```

Y un método en el que se indique `abstract`, debe ser redefinido obligatoriamente por las subclases, y no podrá contener código.

```php
class Producto {
	abstract public function muestra(); 
}
```

Obviamente, no se puede declarar una clase como `abstract` y `final` simultáneamente. `abstract` obliga a que se herede para que se pueda utilizar, mientras que `final` indica que no se podrá heredar.

---

Vamos a hacer una pequeña modificación en nuestra clase `Producto`. Para facilitar la creación de nuevos objetos, crearemos un constructor al que se le pasará un array con los valores de los atributos del nuevo producto.

```php
class Producto {
	public $codigo;
	public $nombre; 
	public $nombre_corto; 
	public $PVP;
	
	public function muestra() {
		print "<p>" . $this->codigo . "</p>";
	}
	
	public function __construct($row) { 
		$this->codigo = $row['cod'];
		$this->nombre = $row['nombre']; 
		$this->nombre_corto = $row['nombre_corto']; 
		$this->PVP = $row['PVP'];
	} 
}
```

¿Qué pasa ahora con la clase TV, qué hereda de `Producto`? Cuando crees un nuevo objeto de esa clase, ¿se llamará al constructor de `Producto`? ¿Puedes crear un nuevo constructor específico para `TV` que redefina el comportamiento de la *clase base*?

Empezando por esta última pregunta, obviamente puedes definir un nuevo constructor para las *clases heredadas* que redefinan el comportamiento del que existe en la *clase base*, tal y como harías con cualquier otro método. Y dependiendo de si programas o no el constructor en la *clase heredada*, se llamará o no automáticamente al constructor de la *clase base*.

En PHP5, si la *clase heredada* no tiene constructor propio, se llamará automáticamente al constructor de la *clase base* (si existe). Sin embargo, si la *clase heredada* define su propio constructor, deberás ser tú el que realice la llamada al constructor de la *clase base* si lo consideras necesario, utilizando para ello la palabra `parent` y el operador de resolución de ámbito.

```php
class TV extends Producto { 
	public $pulgadas;
	public $tecnologia;
	
	public function muestra() {
		print "<p>" . $this->pulgadas . " pulgadas</p>";
	}

	public function __construct($row) { 
		parent::__construct($row); 
		$this->pulgadas = $row['pulgadas']; 
		$this->tecnologia = $row['tecnologia'];
	} 
}
```

Ya viste con anterioridad cómo se utilizaba la palabra clave `self` para tener acceso a la clase actual. La palabra `parent` es similar. Al utilizar parent haces referencia a la *clase base* de la actual, tal y como aparece tras `extends`.


## Interfaces

Una de las dudas más comunes en POO, es qué solución adoptar en algunas situaciones: *interfaces* o *clases abstractas*. Ambas permiten definir reglas para las clases que los implementen o hereden respectivamente. Y ninguna permite instanciar objetos. Las diferencias principales entre ambas opciones son:

* En las *clases abstractas*, los métodos pueden contener código. Si van a existir varias subclases con un comportamiento común, se podría programar en los métodos de la clase abstracta. Si se opta por un *interface*, habría que repetir el código en todas las clases que lo implemente.

* Las *clases abstractas* pueden contener atributos, y los *interfaces* no.

* No se puede crear una clase que herede de dos clases abstractas, pero sí se puede crear una clase que implemente varios interfaces.

Por ejemplo, en la tienda online va a haber dos tipos de usuarios: clientes y empleados. Si necesitas crear en tu aplicación objetos de tipo `Usuario` (por ejemplo, para manejar de forma conjunta a los clientes y a los empleados), tendrías que crear una clase no abstracta con ese nombre, de la que heredarían `Cliente` y `Empleado`.

```php
class Usuario { ... }
class Cliente extends Usuario { ... }
class Empleado extends Usuario { ... }
```

Pero si no fuera así, tendrías que decidir si crearías o no Usuario, y si lo harías como una clase abstracta o como un interface.

Si por ejemplo, quisieras definir en un único sitio los atributos comunes a `Cliente` y a `Empleado`, deberías crear una clase abstracta `Usuario` de la que hereden.

```php
abstract class Usuario { 
	public $dni; 
	protected $nombre; 
	...
}
```

Pero esto no podrías hacerlo si ya tienes planificada alguna relación de herencia para una de estas dos clases.

Para finalizar con los interfaces, a la lista de funciones de PHP relacionadas con la POO puedes añadir las siguientes.

**Funciones de utilidad para interfaces en PHP5**

| Función       | Ejemplo         | Significado   |
|:------------- |:---------------:| :-------------|
| `get_declared_interfaces` | `print_r (get_declared_interfaces());`     | Devuelve un array con los nombres de los interfaces declarados. |
| `interface_exists`        | `if (interface_exists('iMuestra') { ... }` | Devuelve true si existe el interface que se indica, o false en caso contrario. |

## Ejemplo de POO en PHP

Es hora de llevar a la práctica lo que has aprendido. Vamos a aplicar los principios de la POO a la aplicación de tienda web con la que trabajamos en la unidad anterior. Concretamente, vamos a crear en un subdirectorio include las siguientes clases:

* **`DB`**. Va a ser la clase encargada de interactuar con la base de datos.

* **`Producto`**. Las instancias de esta clase representan los productos que se venden en la tienda.

* **`CestaCompra`**. Con esta clase vas a gestionar los productos que escoge el cliente de la tienda para comprar.

![](img/DWES05_CONT_R17_Diagrama_clases.jpg)

Obviamente en una tienda web real, la lista de clases que deberíamos crear sería mucho más amplia y debería haberse estudiado el diagrama resultante. Veamos estas tres clases una a una.

Las instancias de la clase `Producto` van a almacenar la siguiente información de cada producto: `código`, `nombre`, `nombre_corto` y `PVP`. Cada valor se almacenará en un atributo de tipo `protected`, para limitar el acceso a su contenido y, a la vez, permitir su herencia (en caso de que en el futuro creemos clases heredadas). Además, en nuestra aplicación no será necesario cambiar sus valores, pero sí acceder a ellos, por lo que se creará un método de tipo `get` para cada uno.

También necesitamos un constructor. En nuestro caso, para facilitar la creación de objetos de la clase `Producto` a partir del contenido de la base de datos, le pasaremos como parámetro un array obtenido de una fila de la base de datos.

Por último, vamos a crear también un método para mostrar el código del producto. La clase [`Producto`](doc/tiendaOnLine/include/Producto.php) quedará por tanto como sigue:

```php
class Producto {
	protected $codigo; 
	protected $nombre; 
	protected $nombre_corto; 
	protected $PVP;
	
	public function getcodigo() { return $this->codigo; }
	public function getnombre() { return $this->nombre; }
	public function getnombrecorto() { return $this->nombre_corto; } 
	public function getPVP() { return $this->PVP; }

	public function muestra() { print "<p>" . $this->codigo . "</p>"; }
	
	public function __construct($row) { 
		$this->codigo = $row['cod'];
		$this->nombre = $row['nombre']; 
		$this->nombre_corto = $row['nombre_corto'];
		$this->PVP = $row['PVP'];
	} 
}
```

La clase [`DB`](doc/tiendaOnLine/include/DB.php) no necesita almacenar ninguna información; simplemente deberá contener métodos para realizar acciones sobre la base de datos. Por tanto, vamos a definir en la misma únicamente métodos estáticos. No hará falta instanciar ningún objeto de esta clase.

Los métodos son los siguientes:

* **`obtieneProductos`**. Devuelve un array con todos los productos de la base de datos.

* **`obtieneProducto($codigo)`**. Devuelve el producto que coincide con el código que se indica.

* **`verificaCliente($nombre, $contrasena)`**. Devuelve `true` o `false`, según sean correctas o no las credenciales que se proporcionen.

También necesitamos dos funciones para guardar la cesta en la sesión del usuario, y para recuperarla. Y programaremos otra más para mostrar el contenido de
la cesta en formato HTML.

* **`guarda_cesta`**. Guarda la cesta en la sesión del usuario.

* **`carga_cesta`**. Recupera el contenido de la cesta de la sesión del usuario. 

* **`muestra`**. Genera una salida en formato HTML con el contenido de la cesta.

---

El resto de ficheros que componen la tienda web tendremos que reescribirlos para que utilicen las clases que acabas de definir. En general, el resultado obtenido es mucho más claro y conciso. Veamos algunos ejemplos:

En [`login.php`](doc/tiendaOnLine/login.php), para autentificar al usuario basta con hacer:

```php
if (DB::verificaCliente($_POST['usuario'], $_POST['password'])) { 
	...
}
```

En [`productos.php`](doc/tiendaOnLine/productos.php), el código para vaciar o añadir un nuevo producto a la cesta de la compra del usuario será:

```php
// Recuperamos la cesta de la compra
$cesta = CestaCompra::carga_cesta();

// Comprobamos si se ha enviado el formulario de vaciar la cesta if (isset($_POST['vaciar'])) {
	unset($_SESSION['cesta']);
	$cesta = new CestaCompra();
}

// Comprobamos si se quiere añadir un producto a la cesta 
if (isset($_POST['enviar'])) {
	$cesta->nuevo_articulo($_POST['cod']);
	$cesta->guarda_cesta();
}
```

Para tener el código más organizado, hemos creado una función para mostrar el listado de todos los productos:

```
function creaFormularioProductos() {
	$productos = DB::obtieneProductos(); 
	foreach ($productos as $p) {
		// Creamos el formulario en HTML para cada producto
		...
	}
}
```

Prueba a [instalar y ejecutar](doc/tiendaOnLine/README.md) la aplicación de tienda online resultante. Revisa el código y comprueba que entiendes su funcionamiento.

![Pantalla TiendaWeb](img/DWES05_CONT_R18_Tienda_web.jpg)


## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)