# Lenguajes

Una de las diferencias más notables entre un lenguaje de programación web y otro es la manera en que se ejecutan en el servidor web. Debes distinguir tres grandes grupos:

* **Lenguajes de guiones (scripting)**. Son aquellos en los que los programas se ejecutan directamente a partir de su código fuente original. Se almacenan normalmente en un fichero de texto plano. Cuando el servidor web necesita ejecutar código programado en un lenguaje de guiones, le pasa la petición a un intérprete, que procesa las líneas del programa y genera como resultado una página web.

> De los lenguajes que estudiaste anteriormente, pertenecen a este grupo Perl, Python, PHP y ASP (el precursor de ASP.Net).

* **Lenguajes compilados a código nativo**. Son aquellos en los que el código fuente se traduce a código binario, dependiente del procesador, antes de ser ejecutado. El servidor web almacena los programas en su modo binario, que ejecuta directamente cuando se les invoca.

![Ilustración - Esquema Lenguajes compilados](img/DWES01_CONT_R09_Esquema_compilador.png)

> El método principal para ejecutar programas binarios desde un servidor web es CGI. Utilizando CGI podemos hacer que el servidor web ejecute código programado en cualquier lenguaje de propósito general como puede ser C.

* **Lenguajes compilados a código intermedio**. Son lenguajes en los que el código fuente original se traduce a un código intermedio, independiente del procesador, antes de ser ejecutado. Es la forma en la que se ejecutan por ejemplo las aplicaciones programadas en Java, y lo que hace que puedan ejecutarse en varias plataformas distintas.

>En la programación web, operan de esta forma los lenguajes de las arquitecturas Java EE (servlets y páginas JSP) y ASP.Net.
>
>En la plataforma ASP.Net y en muchas implementaciones de Java EE, se utiliza un procedimiento de compilación [JIT](https://es.wikipedia.org/wiki/Compilación_en_tiempo_de_ejecución). Este término hace referencia a la forma en que se convierte el código intermedio a código binario para ser ejecutado por el procesador. Para acelerar la ejecución, el compilador puede traducir todo o parte del código intermedio a código nativo cuando se invoca a un programa. El código nativo obtenido suele almacenarse para ser utilizado de nuevo cuando sea necesario.

Cada una de estas formas de ejecución del código por el servidor web tiene sus **ventajas e inconvenientes**.

* **Los lenguajes de guiones** tienen la ventaja de que no es necesario traducir el código fuente original para ser ejecutados, lo que aumenta su portabilidad. Si se necesita realizar alguna modificación a un programa, se puede hacer en el momento. Por el contrario el proceso de interpretación ofrece un peor rendimiento que las otras alternativas.

* **Los lenguajes compilados a código nativo** son los de mayor velocidad de ejecución, pero tienen problemas en lo relativo a su integración con el servidor web. Son programas de propósito general que no están pensados para ejecutarse en el entorno de un servidor web. Por ejemplo, no se reutilizan los procesos para atender a varias peticiones: por cada petición que se haga al servidor web, se debe ejecutar un nuevo proceso. Además los programas no son portables entre distintas plataformas.

* **Los lenguajes compilados a código intermedio** ofrecen un equilibrio entre las dos opciones anteriores. Su rendimiento es muy bueno y pueden portarse entre distintas plataformas en las que exista una implementación de la arquitectura (como un contenedor de servlets o un servidor de aplicaciones Java EE).

## Código embebido en el lenguaje de marcas

Cuando la web comenzó a evolucionar desde las páginas web estáticas a las dinámicas, una de las primeras tecnologías que se utilizaron fue la ejecución de código utilizando CGI. Los guiones CGI son programas estándar, que se ejecutan por el sistema operativo, pero que generan como salida el código HTML de una página web. Por tanto, los guiones CGI deben contener, mezcladas dentro de su código, sentencias encargadas de generar la página web.

Por ejemplo, si quieres generar una página web utilizando CGI a partir de un guión de sentencias en Linux, tienes que hacer algo como lo siguiente:

![Ilustración - Guión Bash CGI](img/DWES01_CONT_R10_Guion_bash_cgi.jpg)

Esta es una de las principales formas de realizar páginas web dinámicas: integrar las etiquetas HTML en el código de los programas. Es decir, los programas como el guión anterior incluyen dentro de su código sentencias de salida (echo en el caso anterior) que son las que incluyen el código HTML de la página web que se obtendrá cuando se ejecuten. De esta forma se programan, por ejemplo, los guiones CGI y los servlets.

Un enfoque distinto consiste en integrar el código del programa en medio de las etiquetas HTML de la página web. De esta forma, el contenido que no varía de la página se puede introducir directamente en HTML, y el lenguaje de programación se utilizará para todo aquello que pueda variar de forma dinámica.

Por ejemplo, puedes incluir dentro de una página HTML un pequeño código en lenguaje PHP que muestre el nombre del servidor:

![Ilustración - Ejemplo con PHP](img/DWES01_CONT_R11_Ejemplo_PHP.jpg)

Esta metodología de programación es la que se emplea en los lenguajes ASP, PHP y con las páginas JSP de Java EE.

Los servlets de Java EE se diferencian de las páginas JSP en que los primeros son programas Java compilados y almacenados en el contenedor de servlets. Sin embargo, las páginas JSP contienen código Java embebido en lenguaje HTML y se almacenan de forma individual en el servidor web. La primera vez que se necesita una página JSP, se convierte a un servlet y éste se guarda para ser utilizado en posteriores llamadas a la misma página.

En la arquitectura ASP.Net, cada página se divide en dos ficheros: uno contiene las etiquetas HTML y otro el código en el lenguaje de programación utilizado. De esta forma se logra cierta independencia entre el aspecto de la aplicación y la gestión del contenido dinámico. A partir de esos ficheros se obtiene un código intermedio (MSIL en la terminología de la plataforma) que es lo que almacena el servidor.

## Herramientas de programación

A la hora de ponerte a programar una aplicación web, debes tener en cuenta con que herramientas cuentas que te puedan ayudar de una forma u otra a realizar el trabajo. Además de las herramientas que se tengan que utilizar en el servidor una vez que la aplicación se ejecute, como por ejemplo el servidor de aplicaciones o el gestor de bases de datos, de las que ya conoces su objetivo, existen otras que resultan de gran ayuda en el proceso previo, en el desarrollo de la aplicación.

Desde hace tiempo, existen entornos integrados de desarrollo (IDE) que agrupan en un único programa muchas de estas herramientas. Algunos de estos entornos de desarrollo son específicos de una plataforma o de un lenguaje, como sucede por ejemplo con Visual Studio, el IDE de Microsoft para desarrollar aplicaciones en lenguaje C# o Visual Basic para la plataforma .Net. Otros como Eclipse o NetBeans te permiten personalizar el entorno para trabajar con diferentes lenguajes y plataformas, como Java EE o PHP.

No es imprescindible utilizar un IDE para programar. En muchas ocasiones puedes echar mano de un simple editor de texto para editar el código que necesites. Sin embargo, si tu objetivo es desarrollar una aplicación web, las características que te aporta un entorno de desarrollo son muy convenientes. Entre estas características se encuentran:

* **Resaltado de texto**. Muestra con distinto color o tipo de letra los diferentes elementos del lenguaje: sentencias, variables, comentarios, etc. También genera indentado automático para diferenciar de forma clara los distintos bloques de un programa.

* **Completado automático**. Detecta qué estás escribiendo y cuando es posible te muestra distintas opciones para completar el texto.

* **Navegación en el código**. Permite buscar de forma sencilla elementos dentro del texto, por ejemplo, definiciones de variables.

* **Comprobación de errores al editar**. Reconoce la sintaxis del lenguaje y revisa el código en busca de errores mientras lo escribes.

* **Generación automática de código**. Ciertas estructuras, como la que se utiliza para las clases, se repiten varias veces en un programa. La generación automática de código puede encargarse de crear la estructura básica, para que sólo tengas que rellenarla.

* **Ejecución y depuración**. Esta característica es una de las más útiles. El IDE se puede encargar de ejecutar un programa para poder probar su funcionamiento. Además, cuando algo no funciona, te permite depurarlo con herramientas como la ejecución paso a paso, el establecimiento de puntos de ruptura o la inspección del valor que almacenan las variables.

* **Gestión de versiones**. En conjunción con un sistema de control de versiones, el entorno de desarrollo te puede ayudar a guardar copias del estado del proyecto a lo largo del tiempo, para que si es necesario puedas revertir los cambios realizados.

Los dos IDE de código abierto más utilizados en la actualidad son Eclipse y NetBeans. Ambos permiten el desarrollo de aplicaciones informáticas en varios lenguajes de programación. Aunque en sus orígenes se centraron en la programación en lenguaje Java, hoy en día admiten directamente o a través de módulos, varios lenguajes entre los que se incluyen C, C++, PHP, Python y Ruby.

Ambos además ofrecen para la descarga versiones personalizadas del IDE que pueden ser usadas directamente para programar en un lenguaje determinado, sin necesidad de cambiar la configuración o instalar módulos.

### Instalación de NetBeans para PHP en Linux

Vamos a ver cómo instalar el IDE NetBeans en un sistema Linux. Lo primero que debes hacer es comprobar que tienes la última versión de Java, pues tanto NetBeans como Eclipse necesitan de la máquina virtual de Java para ejecutarse. Para ello, desde una consola escribe:

`java -version`

En caso de no tener Java, deberemos instalarlo antes del IDE. Si estamos trabajando en Ubuntu, lo primero sería añadir previamente el repositorio necesario. Por ejemplo, si tu versión de Ubuntu es la `10.04`:

sudo add-apt-repository “deb http://archive.canonical.com/ lucid partner”

O si fuera la `10.10`:

sudo add-apt-repository “deb http://archive.canonical.com/ maverick partner”

Una vez añadido el repositorio, actualiza la lista de paquetes e instala Java:

```
sudo apt-get update
sudo apt-get install sun-java-jdk
```

Mientras instalas Java (o en el caso de que ya lo tuvieras instalado), puedes aprovechar para descargar NetBeans desde su página oficial. En la página de descarga puedes escoger el idioma, la plataforma, y el paquete concreto. Escoge el paquete en español, para sistemas Linux (x86/x64) y lenguaje de programación PHP.

![Ilustración - Descarga NetBeans](img/DWES01_CONT_R14_Descarga_NetBeans_7.jpg)

Al finalizar la instalación de Java, puedes empezar con la de NetBeans. Debes marcar el archivo como ejecutable y abrirlo desde la línea de comandos.

![Ilustración - Instalar NetBeans Linux](img/DWES01_CONT_R15_Instalar_NetBeans_Linux.jpg)

No es necesario modificar ningún parámetro durante el proceso de instalación. Una vez finalizado, puedes acceder a NetBeans mediante una nueva entrada que se ha generado en el menú.

![Ilustración - NetBeans Linux Instalado](img/DWES01_CONT_R16_NetBeans_Linux_Instalado.jpg)


### Instalación de una plataforma LAMP en Ubuntu

Una vez instalado el entorno de desarrollo, es necesario instalar el resto de la plataforma de desarrollo. Si vas a programar en lenguaje PHP, necesitas todos los componentes de una arquitectura LAMP; recuerda:

* `L` de Linux, el sistema operativo.
* `A` de Apache, el servidor web.
* `M` de MySQL, el gestor de bases de datos.
* `P` del lenguaje de programación, que puede ser PHP, Perl o Python. Vamos a instalar el más común de estos tres, PHP.

Puedes instalar los componentes uno a uno, o utilizar el comando tasksel desde la consola. Este comando viene con algunas tareas predefinidas, que nos permiten instalar con un solo comando grupos de aplicaciones. Entre las tareas que incluye tasksel se encuentra `lamp-server`, que incorpora los componentes de una arquitectura LAMP antes mencionados. Para instalar LAMP ejecuta desde una consola:

```
sudo tasksel install lamp-server
```

Durante el proceso de instalación tendrás que introducir la contraseña para el usuario administrador (root) de MySQL.

![Ilustración - ](img/DWES01_CONT_R17_Instalar_LAMP.jpg)

Con la configuración predeterminada, la carpeta raíz de dónde el servidor web Apache extrae los documentos que va a publicar es /var/www. Por tanto, para probar el correcto funcionamiento de la plataforma puedes crear en ese directorio un fichero “prueba.php” con el siguiente contenido:

```
<?php phpinfo(); ?>
```

Lo que estás haciendo es llamar a una función del lenguaje PHP que genera por sí misma una página web completa con información sobre la instalación de PHP. Si todo va bien, al abrir esa página con un navegador web (desde la propia máquina de Ubuntu la URL será `http:/localhost/prueba.php`) deberás ver algo como lo siguiente:

![Ilustración - phpInfo](img/DWES01_CONT_R18_phpinfo.jpg)

## Programación web con Java

Java es el lenguaje de programación más utilizado hoy en día. Es un lenguaje orientado a objetos, basado en la sintaxis de C y C++ y eliminando algunas características de éstos que daban lugar a errores de programación, como los punteros. Todo el código que escribas en Java debe pertenecer a una clase.

El código fuente se escribe en archivos con extensión `.java`. El compilador genera por cada clase un archivo `.class`. Para ejecutar una aplicación programada en Java necesitamos tener instalado un entorno de ejecución (JRE). Para crear aplicaciones en Java necesitamos el kit de desarrollo de Java (JDK), que incluye el compilador.

Como ya viste, existen básicamente dos tecnologías que te permiten programar páginas web dinámicas utilizando [Java EE](https://en.wikipedia.org/wiki/Jakarta_EE): servlets (clases Java compiladas que contienen instrucciones de salida para generar las etiquetas HTML de las páginas) y JSP (páginas web que contienen instrucciones para añadir contenido de forma dinámica).
Aunque no es así en todos los casos, la mayoría de implementaciones disponibles para
JSP compilan cada página y generan un servlet a partir de la misma la primera vez que se va a ejecutar. Este servlet se almacena para ser usado en futuras peticiones.

Por ejemplo, si quieres calcular la suma de dos números y enviar el resultado al navegador, lo podríamos realizar con una página JSP, incluyendo el código en Java dentro de las etiquetas HTML utilizando los delimitadores `<%` y `%>` de la siguiente manera:

![Ilustración - Ejemplo JSP](img/DWES01_CONT_R20_Ejemplo_JSP.jpg)

O también utilizando el método `println` dentro de un servlet como el siguiente, que obtiene los valores a sumar de otra página:

![Ilustración - Ejemplo Servlet](img/DWES01_CONT_R21_Ejemplo_servlet.jpg)

No hay nada que se pueda hacer con JSP que no pueda hacerse también con servlets. De hecho, como ya viste, las primeras se suelen convertir en servlets para ser ejecutadas.

El problema de utilizar servlets directamente es que, aunque son muy eficientes, son muy tediosos de programar puesto que hay que generar la salida en código HTML con gran cantidad de funciones como println. Este problema se resuelve fácilmente utilizando JSP, puesto que aprovecha la eficiencia del código Java, para generar el contenido dinámico, y la lógica de presentación se realiza con HTML normal.

De esta forma estas dos tecnologías se suelen combinar para crear aplicaciones web. Los servlets se encargan de procesar la información y obtener resultados, y las páginas JSP se encargan del interface, incluyendo los resultados obtenidos por los servlets dentro de una página web.

## Programación web con PHP

PHP es un lenguaje de guiones de propósito general, pero diseñado para el desarrollo de páginas web dinámicas utilizando código embebido dentro del lenguaje de marcas. Su sintaxis está basada en la de C / C++, y por lo tanto es muy similar a la de Java. Aunque lo puedes hacer de otras formas, los delimitadores recomendados para incluir código PHP dentro de una página web son `<?php` y `?>`.

El código se ejecuta por un entorno de ejecución con el que se integra el servidor web (normalmente utilizando Apache con el módulo `mod_php`). La configuración tanto del servidor web Apache, como de PHP, se realiza por medio de ficheros de configuración. El de Apache es `httpd.conf`, y el de PHP es `php.ini`. Este fichero, `php.ini`, puede encontrarse en distintas ubicaciones. La función `phpinfo()` que ejecutaste antes te informa, entre otras muchas cosas, del lugar en que se encuentra almacenado el fichero `php.ini` en tu ordenador. En nuestro caso es en `/etc/php5/apache2/php.ini`.

Dependiendo de cómo se integre PHP con Apache, los cambios que realices en su fichero de configuración se aplicarán en un momento o en otro. Si como es nuestro caso, utilizamos `mod_php` para ejecutar PHP como un módulo de Apache, las opciones de configuración de PHP se aplicarán cada vez que se reinicie Apache. Por tanto, no te olvides de hacerlo cada vez que hagas cambios en `php.ini`. Por ejemplo: `sudo /etc/init.d/apache2 restart` o también `sudo service apache2 restart`.

Algunas de las directivas más utilizadas que figuran en el fichero `php.ini` son:

* `short_open_tags`. Indica si se pueden utilizar en PHP los delimitadores cortos `<?` y `?>`. Es preferible no usarlos, pues puede causarnos problemas si utilizamos páginas con XML. Para prohibir la utilización de estos delimitadores con PHP le asignamos a esta directiva el valor `Off`.

* `max_execution_time`. Permite que puedas ajustar el número máximo de segundos que podrá durar la ejecución de un script PHP. Evita que el servidor se bloquee si se produce algún error en un script.

* `error_reporting`. Indica qué tipo de errores se mostrarán en el caso de que se produzcan. Por ejemplo, si haces `error_reporting = E_ALL`, te mostrará todos los tipos de errores. Si no quieres que te muestre los avisos pero sí otros tipos de errores, puedes hacer `error_reporting = E_ALL & ~E_NOTICE`.

* `file_uploads`. Indica si se pueden o no subir ficheros al servidor por HTTP.

* `upload_max_filesize`. En caso de que se puedan subir ficheros por HTTP, puedes indicar el límite máximo permitido para el tamaño de cada archivo. Por ejemplo, `upload_max_filesize = 1M`.

Iremos viendo otras directivas a lo largo del módulo cuando las vayamos necesitando.

A medida que vayas escribiendo código en PHP, será útil que introduzcas en el mismo algunos comentarios que ayuden a revisarlo cuando lo necesites. En una página web los comentarios al HTML van entre los delimitadores `<!--` y `-->`. Dentro del código PHP, hay tres formas de poner comentarios:

* Comentarios de una línea utilizando //. Son comentarios al estilo del lenguaje C. Cuando una línea comienza por los símbolos //, toda ella se considera que contiene un comentario, hasta la siguiente línea.

* Comentarios de una línea utilizando #. Son similares a los anteriores, pero utilizando la sintaxis de los scripts de Linux.

* Comentarios de varias líneas. También iguales a los del lenguaje C. Cuando en una línea aparezcan los caracteres `/*`, se considera que ahí comienza un comentario. El comentario puede extenderse varias líneas, y acabará cuando escribas la combinación de caracteres opuesta: `*/`.

Recuerda que cuando pongas comentarios al código PHP, éstos no figurarán en ningún caso en la página web que se envía al navegador (justo al contrario de lo que sucede con los comentarios a las etiquetas HTML).

### Variables y tipos de datos en PHP

Como en todos los lenguajes de programación, en PHP puedes crear variables para almacenar valores. Las variables en PHP siempre deben comenzar por el signo `$`. Los nombres de las variables deben comenzar por letras o por el carácter `_`, y pueden contener también números. Sin embargo, al contrario que en muchos otros lenguajes, en PHP no es necesario declarar una variable ni especificarle un tipo (entero, cadena,...) concreto. Para empezar a usar una variable, simplemente asígnale un valor utilizando el operador `=`.

```php
$mi_variable = 7;
```

Dependiendo del valor que se le asigne, a la variable se le aplica un tipo de datos, que puede cambiar si cambia su contenido. Esto es, el tipo de la variable se decide en función del contexto en que se emplee.

```php
// Al asignarle el valor 7, la variable es de tipo “entero” $mi_variable = 7;

// Si le cambiamos el contenido 
$mi_variable = “siete”;

// La variable puede cambiar de tipo
// En este caso pasa a ser de tipo “cadena”
```

Los tipos de datos simples en PHP son:

* booleano (boolean). Sus posibles valores son true y false. Además, cualquier número entero se considera como true, salvo el 0 que es false.

* entero (integer). Cualquier número sin decimales. Se pueden representar en formato decimal, octal (comenzando por un 0), o hexadecimal (comenzando por 0x).

* real (float). Cualquier número con decimales. Se pueden representar también en notación científica.

* cadena (string). Conjuntos de caracteres delimitados por comillas simples o dobles.

* null. Es un tipo de datos especial, que se usa para indicar que la variable no tiene valor.

Por ejemplo:

```php
$mi_booleano = false; $mi_entero = 0x2A; $mi_real = 7.3e-1; $mi_cadena = “texto”; $mi_variable = Null;
```

Si realizas una operación con variables de distintos tipos, ambas se convierten primero a un tipo común. Por ejemplo, si sumas un entero con un real, el entero se convierte a real antes de realizar la suma:

```php
$mi_entero = 3;
$mi_real = 2.3;
$resultado = $mi_entero + $mi_real;
// La variable $resultado es de tipo real
```

Estas conversiones de tipo, que en el ejemplo anterior se lleva a cabo de forma automática, también se pueden realizar de forma forzada:

```php
$mi_entero = 3;
$mi_real = 2.3;
$resultado = $mi_entero + (int) $mi_real;

// La variable $mi_real se convierte a entero (valor 2) antes de sumarse. 
// La variable $resultado es de tipo entero (valor 5)
```

### Expresiones y operadores

Como en muchos otros lenguajes, en PHP se utilizan las expresiones para realizar acciones dentro de un programa. Todas las expresiones deben contener al menos un operando y un operador. Por ejemplo:

```php
$mi_variable = 7; 
$a = $b + $c; 
$valor++;
$x += 5;
```

Los operadores que puedes usar en PHP son similares a los de muchos otros lenguajes como Java. Entre ellos tenemos operadores para:

##### Realizar operaciones aritméticas: negación, suma, resta, multiplicación, división y módulo. 

Entre éstos se incluyen operadores de pre y post incremento y decremento, `++` y `--`.

Estos operadores incrementan o decrementan el valor del operando al que se aplican. Si se utilizan junto a una expresión de asignación, modifican el operando antes o después de la asignación en función de su posición (antes o después) con respecto al operando. Por ejemplo:

```php
$a = 5;
$b = ++$a;

// Antes se le suma uno a $a (pasa a tener valor 6)
// y después se asigna a $b (que también acaba con valor 6).

$a = 5;
$b = $a++;

// Antes se asigna a $b el valor de $a (5).
// y después se le suma uno a $a (pasa a tener valor 6)
```

##### Realizar asignaciones

Además del operador `=`, existen operadores con los que realizar operaciones y asignaciones en un único paso (`+=`, `-=`, etc.)

##### Comparar operandos

Además de los que nos podemos encontrar en otros lenguajes (`>`, `>=`, etc.), en PHP tenemos dos operadores para comprobar igualdad (`==`, `===`) y tres para comprobar diferencia (`<>`, `!=` y `!==`).

Los operadores `<>` y `!=` son equivalentes. Comparan los valores de los operandos.

El operador `===` devuelve verdadero (true) sólo si los operandos son del mismo tipo y además tienen el mismo valor. El operador `!==` devuelve verdadero (true) si los valores de los operandos son distintos o bien si éstos no son del mismo tipo. Por ejemplo:

```php
$x = 0;

// La expresión $x == false es cierta (devuelve true).
// Sin embargo, $x === false no es cierta (devuelve false) pues $x es de tipo entero, no booleano.
```

##### Comparar expresiones booleanas

Tratan a los operandos como variables booleanas (true o false). Existen operadores para realizar un `Y` lógico (operadores `and` o `&&`), `O` lógico (operadores `or` o `||`), No lógico (operador `!`) y `O` lógico exclusivo (operador `xor`).

##### Realizar operaciones con los bits que forman un entero

Operaciones con bits: Invertirlos, desplazarlos, etc.

### Ámbito de utilización de las variables

En PHP puedes utilizar variables en cualquier lugar de un programa. Si esa variable aún no existe, la primera vez que se utiliza se reserva espacio para ella. En ese momento, dependiendo del lugar del código en que aparezca, se decide desde qué partes del programa se podrá utilizar esa variable. A esto se le llama visibilidad de la variable.

Si la variable aparece por primera vez dentro de una función, se dice que esa variable es local a la función. Si aparece una asignación fuera de la función, se le considerará una variable distinta. Por ejemplo:

```php
$a = 1;

function prueba() {
    // Dentro de la función no se tiene acceso a la variable $a anterior
    $b = $a;
    
    // Por tanto, la variable $a usada en la asignación anterior es una variable n
    // que no tiene valor asignado (su valor es null)
}
```

Si en la función anterior quisieras utilizar la variable `$a` externa, podrías hacerlo utilizando la palabra global. De esta forma le dices a PHP que no cree una nueva variable local, sino que utilice la ya existente.

```php
$a = 1;

function prueba(){
    global $a;
    $b = $a;
    
	// En este caso se le asigna a $b el valor 1
}
```

Las variables locales a una función desparecen cuando acaba la función y su valor se pierde. Si quisieras mantener el valor de una variable local entre distintas llamadas a la función, deberás declarar la variable como estática utilizando la palabra `static`.

```php
function contador(){
	static $a=0;
	$a++;
   
	// Cada vez que se ejecuta la función, se incrementa el valor de $a
}
```

Las variables estáticas deben inicializarse en la misma sentencia en que se declaran como estáticas. De esta forma, se inicializan sólo la primera vez que se llama a la función.

## Licencias

[Materiales formativos de FP Online propiedad del Ministerio de Educación, Cultura y Deporte](../LICENSE.md)