# Bienvenido

En esta wiki se tratará de desarrollar ciertos contenidos del módulo Desenvolvemento Web en Contarna de Servidor (DWCS)

## Alcance

Estos textos no pretender ser un libro o manual de un curso. Existen muchos materiales accesibles a través de Internet y manuales espécificos de herramientas que debieran ser estudiados primero, antes de detenerse a estudiar este texto.

## Colaborar

Lo que pretende esta Wiki es sintetizar ciertos contenidos, resumiéndolos y estructurándolos para poder ver ciertos conceptos desde un punto de vista o perspectiva diferente.

Esta wiki usa la sintaxis [Markdown](http://daringfireball.net/projects/markdown/). El [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) muestra cómo varios elementos son dibujados o renderizados según esta sintaxis. La [documentación de Bitbucket](https://confluence.atlassian.com/x/FA4zDQ) muestra más información sobre como usar la wiki.

La wiki en sí misma es un repositorio git, lo que significa que tú puedes clonarlo, editarlo de forma local y desconectada, añadir imágenes o cualquier otro tipo de fichero, y realizar un push para contribuir en él, y también para verlo online de forma inmediata.

Puedes clonar el repo de la siguiente manera:

```
$ git clone <url>
```

Las páginas de la Wiki son ficheros normales, con la extensión `.md`. Puedes editarlos localmente tanto como crear unmos nuevos.

