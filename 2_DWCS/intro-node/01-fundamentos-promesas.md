# Promesas

[Una Promise](https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Usar_promesas) (promesa en castellano) es un objeto que representa la terminación o el fracaso de una operación asíncrona. Dado que la mayoría de las personas consumen promises ya creadas, esta guía explicará primero cómo consumirlas, y luego cómo crearlas.

Esencialmente, una promesa es un objeto devuelto al cuál se adjuntan funciones callback, en lugar de pasar callbacks a una función.

- [1. De Callbacks a Promises](#1-de-callbacks-a-promises)
- [2. El problema de las Promises hell (Infierno de promesas)](#2-el-problema-de-las-promises-hell-infierno-de-promesas)
- [3. Promesas en cadena. Solución al Promises hell](#3-promesas-en-cadena-solución-al-promises-hell)
- [`async` y `await`](#async-y-await)

## 1. De Callbacks a Promises

Para comprenderlo, vamos a transformar el [ejemplo previo](src/01-fundamentos-callbacks03.js) usando promesas.

Vamos a convertir el siguiente método que admite un *callback* como parámetro:

```js
let getEmpleado = (id, callback) => {
    
    let empleadoDB = empleados.find(empleado => empleado.id === id)

    if (!empleadoDB) {
        callback(`No existe un empleado con el ID ${ id }`)
    } else {
        callback(null, empleadoDB);
    }
}
```

Por una promesa. Vamos a contruir una `new Promise()` sin necesidad de importar nada, pues forma parte de ECMAScript 6 que fue la segunda mayor revisión de Javascript también conocida como *ES6* y *ECMAScript 2015*:

```js
let getEmpleado = (id, callback) => {

    // Se inyecta una función de flecha que va a ser el cuerpo de la promesa 
    // que recibe dos argumentos (resolv y reject) que serán 2 callbacks
    const promesa = new Promise( (resolve, reject) => {
        
        let empleadoDB = empleados.find(empleado => empleado.id === id)

        if (empleadoDB){
            // Se invocará al callback correspondiente al éxito de la promesa
            resolve(empleadoDB);
        } else {
            // Se invocará al callback correspondiente al fracaso de la promesa
            reject(`No existe empleado con id ${id}`)
        }
    });
    return promesa;
}
```

La llamada a `getEmpleado` se simplifica en este caso con el uso de la promesa quedando más clara la intención:

```js
const id_empleado = 3
getEmpleado(id_empleado)
    .then( empleado => console.log(empleado))
    .catch( err => console.log(err));
```

## 2. El problema de las Promises hell (Infierno de promesas)

Ahora toca refactorizar el método `getSalario` y convertirlo también en una promesa:

```js
let getSalario = (empleado) => {
    
    return new Promise( (resolve, reject) => {

        let salarioDB = salarios.find(salario => salario.id === empleado.id_salario);
        
        if(salarioDB){
            resolve({
                nombre: empleado.nombre,
                salario: salarioDB.salario,
                id: empleado.id
            });
        } else {
            reject(`No se encontró un salario para el usuario ${ empleado.nombre }`)
        }
    });
}
```

La llamada a `getEmpleado`, de nuevo, se va complicando. Debo de encadenar dos promesas:

```js
const id_empleado = 1
getEmpleado(id_empleado)
    .then( empleado => {
        getSalario(empleado).then( salario => {
            console.log(`El empleado ${empleado.nombre} tiene un salario de ${salario.salario}`);
        })
    })
    .catch( err => console.log(err));
```

Si ejecutamos este código arrojaría con el `id_empleado=1`:

```js
El empleado Fernando tiene un salario de 1000
```

Si ejecutamos este código arrojaría con el `id_empleado=3` un feo error:

```js
(node:67634) UnhandledPromiseRejectionWarning: No se encontró un salario para el usuario Juan
(Use `node --trace-warnings ...` to show where the warning was created)
<node_internals>/internal/process/warning.js:43
(node:67634) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch().
```

Esto se produjo porque no manejamos el error con el `cath` de la segunda promesa :

```js
const id_empleado = 3
getEmpleado(id_empleado)
    .then( empleado => {
        
        getSalario(empleado)
            .then( salario => {
                console.log(`El empleado ${empleado.nombre} tiene un salario de ${salario.salario}`)
            })
            .catch( err => console.log(err))
    })
    .catch( err => console.log(err));
```

Esto solució el problema mostrando el mensaje `No se encontró un salario para el usuario Juan` en ese caso. Sin embargo, parece que no hemos ganado nada de legibilidad en relación al manejo de los callbacks sin usar promesas. Más bien al contrario. Es todavía más ilegible. Más difícil de mantener.

Puedes depurar este ejemplo directamente transcrito [en el script](src/01-fundamentos-01-fundamentos-promesas01.js) además de ver ciertas refactorizaciones para reducir líneas mejorando la legibilidad.

## 3. Promesas en cadena. Solución al Promises hell

En este punto vamos a solucionar la ilegibilidad que provoca el `Promises hell`. Para ello vamos a utilizar lo que se conoce como *Encadenamiento de Promesas* o *Promesas en cadena*.

En este momento, no vamos a realizar ningún cambio sobre los métodos `getEmpleado` y `getSalario`. Sólo refactorizar la forma de llamarlos o usarlos como promesas.

En lugar de el el primer `then`, invocar a `getSalario` para luego encadenar un segundo `then`, vamos emplear un `return`. De esta forma retornamos la promesa generada por `getEmpleado` sin procesarla:

```js
getEmpleado(id_empleado)
    .then( (empleado) => {
        return getSalario(empleado)
    });
```

De esta forma ya podremos procesarla a continuación del primer `then`:

```js
getEmpleado(id_empleado)
    .then( (empleado) => {
        return getSalario(empleado)
    })
    .then( (salario) => {
        console.log(salario);
    });
```

Podríamos optimizar un poco mejor el espacio eliminando unas cuantas cosas innecesarias:

```js
getEmpleado(id_empleado)
    .then( (empleado) => getSalario(empleado))
    .then( (salario) => console.log(salario););
```

Esto tiene mejor pinta. Sin embargo, lo que queremos es mostrar los posibles mensajes que mostrábamos antes.

```js
getEmpleado(id_empleado)
    .then( (empleado) => getSalario(empleado))
    // La próxima línea fallará: ReferenceError: empleado is not defined
    .then( (salario) => console.log(`El empleado ${empleado.nombre} tiene un salario de ${salario.salario}`)); 
```

Sin embargo este código precedente producirá un error, puesto que la variable empleado se originó en un *scope* distinto al del segundo `then`. Por que la usa solución prosible será declarar una variable en un scope superior para almacenar el valor que necesitamos y que sea accesible en el segundo `then`. Para ello tendremos que volver a colocar los `{` `}` porque necesitamos ejecutar 2 instrucciones, no olvidándose del `return` que devolverá la promesa:

```js
let nombre;
getEmpleado(id_empleado)
    .then( (empleado) => {
        nombre = empleado.nombre;
        return getSalario(empleado);
    })
    .then( (salario) => console.log(`El empleado ${nombre} tiene un salario de ${salario.salario}`));
```

Este código es mucho más legible que el [código anterior]((#12-el-problema-de-las-promises-hell-infierno-de-promesas)). Sin embargo todavía queda manejar el posible error al consultar el `id_empleado=3` que no tiene salario asociado. En otro caso retornaría un feo error de `Unhandled promise rejection`. Lo podemos hacer con un simple `catch` global (nótese la duplicidad del catch con el *promises hell*):

```js
let nombre;
getEmpleado(id_empleado)
    .then( (empleado) => {
        nombre = empleado.nombre;
        return getSalario(empleado);
    })
    .then( (salario) => console.log(`El empleado ${nombre} tiene un salario de ${salario.salario}`))
    .catch( err => console.log(err));
```

Puedes depurar este ejemplo directamente transcrito [en el script](src/01-fundamentos-01-fundamentos-promesas02.js).

## `async` y `await`

La declaración de [función `async`](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Sentencias/funcion_asincrona) define una función como *asíncrona*, la cual devuelve un objeto AsyncFunction.

Vamos a comprobar esto definiendo una simple función:

```js
const getInfoUsuario = () => {
    return "Hola mundo";
}
```

Si analizamos con el *suggest* del IDE, vemos que retorna métodos propios de un `string`:

![Suggest del IDE function return string](img/01-fundamentos-promesas-suggest1.png)

Sin embargo, si definimos la función con `async` tendremos:

![Suggest del IDE function return string](img/01-fundamentos-promesas-suggest2.png)

Observa que introducciendo `async` ya no devuelve un `string`, sino una `Promisse`.

De esta forma podríamos recuperar el valor que nos retorna la promesa:

```js
const getInfoUsuario = async() => {
    return "Hola mundo";
}

getInfoUsuario().then(msg => console.log(msg));
```

El operador `await` es usado para esperar a una `Promise`. Bien que termine con su callback `resolve` o `reject`, evitando que se siga ejecutando la siguiente instrucción. El resultado retornado por `resolve` puede ser asignado directamente a una variable.  **Es importante advertir que `away` sólo puede ser usado dentro de una función `async`**.

El operador `await` puede ser usado sin que la función retorne explícitamente una promesa, pero no tendrá efecto:

```js
let saluda = () => "hola mundo"

const getInfoUsuario = async(id) => {
    // En este caso await no tiene efecto
    // porque saluda retorna string y no Promisse
    let empleado = await saluda()
}
```

En el siguiente ejemplo, puedes ver cómo se crea un nuevo método `getInfoUsuario` marcado como `async` (para poder utilizar dentro `await`). En las instrucciones donde se asigna empleado y salario, `await` forzará a que se espere a la resolución de la promesa asignando directametne a la variable retornada por el callback `resolve` de las promesas que retornan los métodos `getEmpleado` y `getSalario`.

```js
const getInfoUsuario = async(id) => {
    const empleado = await getEmpleado(id);
    const salario = await getSalario(empleado);

    //Nótese que este método no retorna un string como puede parecer, sino una promesa!
    return `El empleado ${empleado.nombre} tiene un salario de ${salario.salario}`;
    
}

const id_empleado = 3
getInfoUsuario(id_empleado)
    .then( msg => console.log(msg));
```

Nótese que el método `getInfoUsuario` no retorna un string como puede parecer, sino una promesa!

Pero si con `await` tenemos directamente asignado a la variable el valor que retorna la ejecucución correcta de la promesa, ¿cómo capturamos el posible error? En ese caso podemos utilizar un `try`/`catch`:

```js
const getInfoUsuario = async(id) => {
    // await forzará a que se espere a la resolución de la promesa
    // asignando directametne a la variable empleado el valor devuelto
    // por el callback `resolve`
    try{
        const empleado = await getEmpleado(id);
        const salario = await getSalario(empleado);
        //Nótese que este método no retorna un string como puede parecer, sino una promesa!
        return `El empleado ${empleado.nombre} tiene un salario de ${salario.salario}`;
    }
    catch(error){
        // No debo retornar el error (que es un string)
        throw error;
    }
}

//////////////////////////////////////////////////////////////////////////
// A partir de este momento se desarrollan las llamadas a los métodos
//////////////////////////////////////////////////////////////////////////
const id_empleado = 3
getInfoUsuario(id_empleado)
    .then( msg => console.log(msg))
    .catch( err => console.log(err));
```

Nótese que el código es todavía más legible que antes.

Puedes depurar este ejemplo directamente transcrito [en el script](src/01-fundamentos-01-fundamentos-**promesas03**.js).
