const http= require('http');

http.createServer( (request, response) => {

    console.log(request.headers);
    console.log('El cliente solicita: ', request.url);
    console.log('El cliente envía el mensaje: ', request.method);

    response.write('Hola mundo');
    response.end();
}).listen(8080);