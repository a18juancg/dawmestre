const saluda = (nombre) => {
    imprime("Voy a saludar...");
    console.log(`Hola ${nombre}!`);
}

const saluda_cortesmente = (nombre) => {
    imprime("Voy a saludar cortesmente...");
    console.log(`Buenos días Don ${nombre}!`);
}

// Esta función no la quiero 'publicar'. Quiero que permanezca en el ámbito privado del script
const imprime = (mensaje) => console.log(`Imprimiendo mensaje: ${mensaje}`);

// Podríamos retornar sólo una funcion
// module.exports = saluda

// Podríamos retornar varias funciones que queremos hacer 'públicas'
module.exports = {
    // Sabemos que en ES6 si clave y valor tienen el mismo nombre podemos omitir valor
    // saluda: saluda,
    saluda,
    saluda_cortesmente
}