// Función expresada de la forma tradicional
function unaFuncionQueImprime(parametro){
    console.log(parametro);
}

// Utilizando la función global que ejecuta una función en un cierto momento de tiempo
setTimeout(unaFuncionQueImprime, 1000, "mensaje 1");
setTimeout(unaFuncionQueImprime, 1000, "mensaje 2");

// También podríamos definir la función con una función de fecha
let unaFuncionQueImprimeHola = () => console.log("Hola");
setTimeout(unaFuncionQueImprimeHola, 500);

// O introduciendo la función de flecha directamente en el parámetro sin usar una variable intermedia
setTimeout(() => {
    console.log(`Hola Mundo`);
}, 1500);

// Utilizando la función global con una función de flecha parametrizada
setTimeout((parametro) => {
    console.log(parametro);
}, 2000, "Adios mundo");
console.log(`Buenos días`);
console.log("Estamos aprendiendo");

// Trata de determinar en qué orden se ejecutan los diferentes console.logs
// ¿Es determinístico?