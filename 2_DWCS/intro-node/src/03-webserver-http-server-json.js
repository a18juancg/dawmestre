const http= require('http');

http.createServer( (request, response) => {

    const objeto = {
        id: 1,
        nombre: "Pedro",
    }

    response.writeHead(201, {'Content-Type': 'application/json'});
    response.write( JSON.stringify(objeto));
    response.end();
}).listen(8080);