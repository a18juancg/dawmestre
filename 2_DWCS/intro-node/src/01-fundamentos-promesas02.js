
// Se define este array donde almacenaremos una seria de empleados
let empleados = [{
    id: 1,
    nombre: 'Fernando',
    id_salario: 23
}, {
    id: 2,
    nombre: 'Melissa',
    id_salario: 46
}, {
    id: 3,
    nombre: 'Juan',
    id_salario: 58
}];

// En otro array almacenaremos cuantías de salarios de dada empleado por su campo id
let salarios = [{
    id: 23,
    salario: 1000
}, {
    id: 46,
    salario: 2000
}];


let getEmpleado = (id) => {
    // Se inyecta una función de flecha que va a ser el cuerpo de la promesa 
    // que recibe dos argumentos (resolv y reject) que serán 2 callbacks
    return new Promise( (resolve, reject) => {
        
        let empleadoDB = empleados.find(empleado => empleado.id === id);

        // Utilizamos una ternaria
        (empleadoDB)
            ? resolve(empleadoDB)
            : reject(`No existe empleado con id ${id}`);
    });
}


let getSalario = (empleado) => {
    
    return new Promise( (resolve, reject) => {

        let salarioDB = salarios.find(salario => salario.id === empleado.id_salario);
        
        // Utilizamos una ternaria
        (salarioDB)
            ? resolve(salarioDB)
            : reject(`No se encontró un salario para el usuario ${ empleado.nombre }`)
    });
}

//////////////////////////////////////////////////////////////////////////
// A partir de este momento se desarrollan las llamadas a los métodos
//////////////////////////////////////////////////////////////////////////
const id_empleado = 1

let nombre;
getEmpleado(id_empleado)
    .then( (empleado) => {
        nombre = empleado.nombre;
        return getSalario(empleado);
    })
    .then( (salario) => console.log(`El empleado ${nombre} tiene un salario de ${salario.salario}`))
    .catch( err => console.log(err));