const http= require('http');

http.createServer( (request, response) => {

    console.log(`${request.method} ${request.url}`);

    if(request.url === '/download_csv'){
        console.log('GET ', request.url);
        console.log("Hola");
        response.setHeader('Content-Disposition', 'attachment; filename=fichero.csv')

        response.writeHead(201, {'Content-Type': 'application/csv'});


        response.write( 'id, nombre\n');
        response.write( '1, Mateo\n');
        response.write( '2, Julia\n');
        response.end();
    }
    else if(request.url === '/alumnos' && request.method === 'GET'){
        
        response.writeHead(200, {'Content-Type': 'application/json'});
        const objeto = { id: 1, nombre: "Pedro"};
        response.write( JSON.stringify(objeto))
        response.end();
    }
    else if(request.url === '/alumnos'  && request.method === 'POST'){
        response.writeHead(201, {'Content-Type': 'application/json'});
        // console.log(request);
        request.on('data', (data) => {
            // Too much POST data, kill the connection!
            const cadena = String(data);
            console.log(`Recibo el cuerpo de la petición: ${cadena}`);
            // La parseo
            const [ param1, param2 ] = cadena.split('&');
            const nombre  = param1.split('=')[1];
            const apellidos = param2.split('=')[1];
            const objeto = { msg: "Alumno creado", alumno: { id: 3, nombre, apellidos } };
            response.write( JSON.stringify(objeto))
            response.end();
        });
    }
    else {
        response.writeHead(404, {'Content-Type': 'text/html'});
        response.write('<h1>404 Not Found :( </h1>')
        response.end();
    }

}).listen(8080);