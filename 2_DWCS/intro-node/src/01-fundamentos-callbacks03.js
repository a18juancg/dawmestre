
// Se define este array donde almacenaremos una seria de empleados
let empleados = [{
    id: 1,
    nombre: 'Fernando',
    id_salario: 23
}, {
    id: 2,
    nombre: 'Melissa',
    id_salario: 46
}, {
    id: 3,
    nombre: 'Juan',
    id_salario: 58
}];

// En otro array almacenaremos cuantías de salarios de dada empleado por su campo id
let salarios = [{
    id: 23,
    salario: 1000
}, {
    id: 46,
    salario: 2000
}];


let getEmpleado = (id, callback) => {
    // Método find del array para buscar el registro
    // https://developer.mozilla.org/en-us/docs/Web/JavaScript/Reference/Global_Objects/Array/find
    let empleadoDB = empleados.find(empleado => empleado.id === id)

    if (!empleadoDB) {
        callback(`No existe un empleado con el ID ${ id }`)
    } else {
        callback(null, empleadoDB);
    }
}


let getSalario = (empleado, callback) => {
    // Método find del array para buscar el registro
    // https://developer.mozilla.org/en-us/docs/Web/JavaScript/Reference/Global_Objects/Array/find
    let salarioDB = salarios.find(salario => salario.id === empleado.id_salario);

    if (!salarioDB) {
        callback(`No se encontró un salario para el usuario ${ empleado.nombre }`);
    } else {
        callback(null, {
            nombre: empleado.nombre,
            salario: salarioDB.salario,
            id: empleado.id
        });
    }
}

//////////////////////////////////////////////////////////////////////////
// A partir de este momento se desarrollan las llamadas a los métodos
//////////////////////////////////////////////////////////////////////////
getEmpleado(3, (err, empleado) => {
    // null es considerado como false en el if
    if (err) {
        return console.log(err);
    }
    getSalario(empleado, (err, resp) => {
        if (err) {
            return console.log(err);
        };
        console.log(`El salario de ${ resp.nombre } es de ${ resp.salario } €`);
    });
});
