let nombre = 'Manuel';

if (true) {
    let nombre = 'Maria'; // sí que java porque se incicia en otro ámbito
}
console.log(nombre); // Imprimiría Manuel y no María. Tenemos 2 variables "nombre" en ámbitos distintos

let i = 'Hola Mundo';

for (let i = 0; i <= 2; i++) {
    console.log(`i: ${ i }`);
}

console.log(i); // Imprimirá 'Hola mundo' y NO '3'

for (let z = 0; z < 2; z++) {}
//console.log(z); // Lanzaría error. No está inicializada z en este ámbito

console.log("----------------------------------------------")

// Con var
var unaVariable = 1
var unaVariable = 2 // No falla! Lint protesta!

if (true) {
    var unaVariable = 3 // Cuidado porque puedo estar modificando una variable de otro ámbito
}
console.log(`Valor de unaVariable=${unaVariable}`);

for (var v = 0; v < 2; v++) {
    console.log(v);
}

console.log(`El valor de v fuera del for v: $ { v }`)