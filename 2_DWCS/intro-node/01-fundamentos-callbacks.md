# Callback

Comenta sobre [qué es un callback en la wikipedia](https://es.wikipedia.org/wiki/Callback_(informática)):

> En programación de computadoras, una devolución de llamada o retrollamada (en inglés: callback) es una función "A" que se usa como argumento de otra función "B". Cuando se llama a "B", ésta ejecuta "A". Para conseguirlo, usualmente lo que se pasa a "B" es el puntero a "A".
>
> Esto permite desarrollar capas de abstracción de código genérico a bajo nivel que pueden ser llamadas desde una subrutina (o función) definida en una capa de mayor nivel. Usualmente, el código de alto-nivel inicia con el llamado de alguna función, definida a bajo-nivel, pasando a esta un puntero, o un puntero inteligente (conocido como handle), de alguna función. Mientras la función de bajo-nivel se ejecuta, esta puede ejecutar la función pasada como puntero para realizar alguna tarea. En otro escenario, las funciones de bajo nivel registran las funciones pasadas como un handle y luego pueden ser usadas de modo asincrónico.
>
> Una retrollamada puede ser usada como una aproximación simple al polimorfismo y a la programación genérica, donde el comportamiento de una función puede ser dinámicamente determinado por el paso punteros a funciones o handles a funciones de bajo nivel que aunque realicen tareas diferentes los argumentos sean compatibles entre sí. Esta es una técnica de mucha importancia por lo que se la llama código reutilizable.

- [1. Ejemplo con la función `setTimeout`](#1-ejemplo-con-la-función-settimeout)
- [2. Ejemplo realizando nuestra propia función `callback`](#2-ejemplo-realizando-nuestra-propia-función-callback)
- [3. Gestión de errores en los callbacks](#3-gestión-de-errores-en-los-callbacks)
- [4. El problema de los Callbacks hell (Infierno de callbacks)](#4-el-problema-de-los-callbacks-hell-infierno-de-callbacks)

## 1. Ejemplo con la función `setTimeout`

La función `setTimeout` es una función global que ejecuta una función no inmediatamente, sino al cado de un número de milisegundos que especifiquemos.

Su firma así lo especifica:

```js
function setTimeout(handler: TimerHandler, timeout?: number, ...arguments: any[]): number
```

Por tanto vamos a definir una función que queramos invocar:

```js
function unaFuncionQueImprime(parametro){
    console.log(parametro);
}
```

Y posteriormente invocarla con un *delay* de 1000ms:

```js
// Nótese que el parámetro/s de la función se pasan a continuación del nº de ms. 
setTimeout(unaFuncionQueImprime, 1000, "mensaje para imprimir");
```

También podríamos definir la función con una función de fecha

```js
let unaFuncionQueImprimeHola = () => console.log("Hola");
setTimeout(unaFuncionQueImprimeHola, 1200);
```

O introduciendo la función de flecha directamente en el parámetro sin usar una variable intermedia

```js
setTimeout(() => {
    console.log('Hola Mundo');
}, 1500);

// Utilizando la función global con una función de flecha parametrizada
setTimeout((parametro) => {
    console.log(parametro);
}, 2000, "Adios mundo");
```

Puedes depurar estos ejemplos directamente transcritos [al script](src/01-fundamentos-callbacks01.js).

## 2. Ejemplo realizando nuestra propia función `callback`

En el siguiente ejemplo realizaremos nuestra propia función que invocará a otra pasada como parámetro de la primera.

En el siguientre fragmento tendremos:

- `getUsuarioById`: que muestra asíncronamente infromación sobre un usuario, simulando algo parecido a una carga de db.

- `getUsuarioById(10);` que invocará a este método con el supuesto id de usuario a cargar.

```js
getUsuarioById = (id) => {
    let usuario = {
        // En ECMAScript6 o superior es redundante poner id: id (una propiedad igual al de una variable)
        id,
        nombre: null,
    }

    setTimeout(() => {
        //Después de realizar todas las tareas que con este timeout simulamos que durarán 1s
        usuario.nombre = "Manuel"
        // Imprimirá el usuario
        console.log(usuario);
    }, 1000);
}

// Instrucción que invoca a nuestra función `getUsuarioById`
getUsuarioById(10);
```

El resultado obtenido se ejecutamos el script sería:

```js
{id: 10, nombre: 'Manuel'}
```

El problema es que la impresión del usuario se está realizando en la ejecución en el proceso asíncrono. Supongamos que necesitara imprimir ese usuario en un lugar diferente, para mantener la presentación de la información en pantalla separada de la lógica de carga de datos de una db. Tendría que hacer algo así:

```js
getUsuarioById = (id, callback) => {
    let usuario = {
        // En ECMAScript6 o superior es redundante poner id: id (una propiedad igual al de una variable)
        id,
        nombre: null,
    }

    setTimeout(() => {
        //Después de realizar todas las tareas que con este timeout simulamos que durarán 1s
        usuario.nombre = "Manuel"
        // Imprimirá el usuario
        callback(usuario);
    }, 1000);
}

// Instrucción que invoca a nuestra función `getUsuarioById`
getUsuarioById(10, (unUsuario) => {
    console.log(unUsuario.nombre.toUpperCase());
});
```

El resultado obtenido se ejecutamos el script sería:

```js
MANUEL
```

Este fragmento de código funciona exactamente igual al anterior. Con la salvedad que nos habíamos propuesto. Que la responsabilidad de representar la información (esta vez en consola), se ubicase en un lugar diferente.

**Vemos que los *callbacks* no son más que una función que se manda como argumento.**

## 3. Gestión de errores en los callbacks

Pero qué sucedería si no siempre pudiese localizar el id que se le envía como parámetro, o lo que es lo mismo, que no existe un id para ese usuario. Vamos a simularlo:

```js
if (id === 10) {
    usuario.nombre = "Manuel";
    callback(usuario);
} else {
    callback(`El usuario con id ${ id }, no existe en la BD`);
}
```

El problema es que habíamos indicado que el *callback* en este caso, esperaba recibir el usuario como parámetro.

```js
getUsuarioById(10, (usuario) => {
    console.log('Usuario de base de datos', usuario);
});
```

A este problema se le aplica una solución asumida normalmente por la comunidad de desarrolladores y que de alguna forma ya constituye un estándar, un patrón o una buena práctica en programación. **Vamos a asumir que el callback siempre va a recibir un error como primer argumento**.

```js
getUsuarioById(10, (err, usuario) => {
    if (err) {
        return console.log(err);
    }
    console.log('Usuario de base de datos', usuario);
});
```

Y que invoquemos así al *callback* desde la clase que lo invoca (generalmente la asíncrona)

```js
if (id === 10) {
    usuario.nombre = "Manuel";
    callback(null, usuario);
} else {
    callback(`El usuario con id ${ id }, no existe en la BD`);
}
```

Nota que se le envía `null` al *callback* cuando no se produce ningún error y sólamente se le envía un parámetro (el primero), cuando efectivemente sí se produce el error.

Puedes depurar estos ejemplos directamente transcritos [al script](src/01-fundamentos-callbacks02.js).

Este enfoque es totalmente válido y de hecho muchos desarrollos asumen este patrón de codificación. Sin embargo, el uso de *callbacks*, puede super un problema, concretamente uno denominado *callbacks hell*.

## 4. El problema de los Callbacks hell (Infierno de callbacks)

Supongamos que tenemos una colección de empleados

```js
let empleados = [{
    id: 1,
    nombre: 'Fernando',
    id_salario: 23
}, {
    id: 2,
    nombre: 'Melissa',
    id_salario: 46
}, {
    id: 3,
    nombre: 'Juan',
    id_salario: 58
}];
```

Y otra colección relacionada de salarios:

```js
let salarios = [{
    id: 23,
    salario: 1000
}, {
    id: 46,
    salario: 2000
}];
```

Se puede definir la función `getEmpleado` de la siguiente forma:

```js
let getEmpleado = (id, callback) => {
    
    let empleadoDB = empleados.find(empleado => empleado.id === id)

    if (!empleadoDB) {
        callback(`No existe un empleado con el ID ${ id }`)
    } else {
        callback(null, empleadoDB);
    }
}
```

E invocarla de la siguiente forma:

```js
getEmpleado(1, (err, empleado) => {
    // null es considerado como false en el if
    if (err) {
        return console.log(err);
    }
    console.log(`El id del salario de ${ empleado.nombre } es: ${empleado.id_salario}`);
});
```

Cuyo resultado sería:

```js
El id del salario de Fernando es: 23
```

Si este mensaje no es suficiente porque no interesa también conocer el importe del salario y no el id de éste, supongamos que hiciésemos otra función similar para obtener el salario:

```js
let getSalario = (empleado, callback) => {
    // Método find del array para buscar el registro
    // https://developer.mozilla.org/en-us/docs/Web/JavaScript/Reference/Global_Objects/Array/find
    let salarioDB = salarios.find(salario => salario.id === empleado.id_salario);

    if (!salarioDB) {
        callback(`No se encontró un salario para el usuario ${ empleado.nombre }`);
    } else {
        callback(null, {
            nombre: empleado.nombre,
            salario: salarioDB.salario,
            id: empleado.id
        });
    }
}
```

Para realizar la consulta de un salario de un empleado por su id de empleado, tendríamos que hacer la siguinte llamada:

```js
getEmpleado(3, (err, empleado) => {
    // null es considerado como false en el if
    if (err) {
        return console.log(err);
    }
    getSalario(empleado, (err, resp) => {
        if (err) {
            return console.log(err);
        };
        console.log(`El salario de ${ resp.nombre } es de ${ resp.salario } €`);
    });
});
```

Nótese que tendremos declaración de funciones de flecha anónimas y anidadas. Esta anidación, que en función de su posible crecimiento por la complegidad del propio problema, puede ir creciendo, **hace que nuestro código se convierta completamente en código ilegible**. Muy dificil de seguir y leer a primera vista. **Esto es lo que se denomina como *callbacks hell*** (*el infierno de los callbacks*).

Puedes depurar estos ejemplos directamente transcritos [al script](src/01-fundamentos-callbacks03.js).

Las promesas de EcmaScript pueden ayudarnos mucho con el problema de los *callbacks hell*. Aunque si no se emplean bien, pueden incluso resultar ser más ilegibles todavía.
