# Docker

[Docker](https://www.docker.com) es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos.

Utilizaremos docker para evitar crear pesadas instancias de [Máquinas Virtuales](https://es.wikipedia.org/wiki/Máquina_virtual) que nos ralentizarón nuestro proceso de desarrollo.

Uno de los usos que le vamos a dar a docker es la creación de un entorno de desarrollo en PHP para un proyecto [LAMP](https://es.wikipedia.org/wiki/LAMP). Esto significa tener tecnologí­as como Linux + Apache2 + MySQL + PHP fácilmente desplegadas en nuestra máquina, sin necesidad de hacer ninguna instalación (salvo la del propio docker). Por otra parte, cuando dejemos de utilizar estas herramientas, podrán ser fácilmente replegadas sin dejar ningún rastro de que han estado ejecutándose en nuestro equipo.

En el curso no vamos a profundizar en el uso de docker. Sin embargo sí­ tendremos unas ["recetas"](lib/recetas-docker.md), para introducir un poco esta tecnología y facilitar un arranque rápido de pruebas básicas con ella.
