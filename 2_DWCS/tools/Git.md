# Git

* [Doc Oficial Git (v2.1.2) Tema 1 (resaltada)](lib/progit_v2.1.2-tema1.pdf)
* [Doc Oficial Git (v2.1.2) Tema 2 (resaltada)](lib/progit_v2.1.2-tema2.pdf)
* [Doc Oficial Git (v2.1.2) Tema 3 (resaltada)](lib/progit_v2.1.2-tema3.pdf)

## Configuración

### ¿Cómo configuro git con mi nombre y email?

```Shell
# Configuro el email
git config --global user.email="xxx@gmail.com"
# Configuro el nombre
git config --global user.name="Nombre"
```

### ¿Cómo cambio el editor para realizar los commits?

Git utiliza el editor de texto por defecto de tu sistema, pero si no tienes ninguno configurado, Git utilizará Vi como editor por defecto para crear y editar las etiquetas y los mensajes de los commits. Para cambiar el editor, puedes utilizar el ajuste 'core.editor':

```Shell
#Ejemplos de configuración:
$ git config --global core.editor gedit
$ git config --global core.editor notepad.exe
$ git config --global core.editor emacs
```

## Historia del Repo

### ¿Cómo se pueden revisar los ficheros que han cambiado en los commits?

```Shell
# Hay muchas opciones para ver el historial, el siguiente comando es una.
# Esta opción imprime una lista de archivos modificados y cuantas líneas se han modificado.
git log --stat
```

### ¿Cómo puedo listar los ficheros modificados recientemente?

Para ver el histórico de confirmaciones (commits), el comando a utilizar es `$ git log`, sin embargo por si solo no nos muestra los nombres de los ficheros  modificados. Para ello necesitamos acompañarlo de opciones que filtren la búsqueda.

* La opción `--name-only` muestra la lista de archivos afectados.

* La opción `--pretty=format:' '` muestra los commits usando un formato alternativo. Posibles opciones son `oneline`, `short`, `full`, `fuller` y `format` (mediante el cual puedes especificar tu propio formato).

* La opción `--since=1.weeks` limita los commits a los realizados en la última semana. Puede ser sustituido por otro número o por otro modificador como `.hours` , `.days` o `.years`

* La opción `-3` despues de `$ git log` acota la búsqueda a un número determinado (concretamente 3 en este ejemplo) de commits.

```
# Listar los nombres de los ficheros modificados en la última semana:
$  git log --name-only --pretty=format:'' --since=1.weeks
```

## Remotos

### ¿Cómo puedo ver los remotos junto a su url?

```Shell
git remote -v
```

### ¿Cómo puedo cambiar la url de mis remotos?

```Shell
$ git remote set-url <nombre_remoto> <url_nueva>
```

### ¿Cómo puedo renombrar remotos?

```Shell
git remote rename <old> <new>
```

### ¿Cómo puedo eliminar remotos? ¿Y qué pasa con sus ramas? ¿Y que pasa con los commits?

Para eliminar el remoto:

```Shell
$ git remote remove <nombre_remoto>
o
$ git remote rm <nombre_remoto>
```

//TODO: ¿Y qué pasa con sus ramas? ¿Y que pasa con los commits?

## Ramas

### ¿Cómo compruebo mis ramas locales y su seguimiento?

```Shell
$ git remote -vv
```

### Sobre hacer seguimiento de Ramas

Si te obtienes una rama remota y quieres trabajar con ella. Esto trakea las ramas local y remota y la selecciona para empezar a trabajar con ella. Simplemente:

```Shell
git checkout rama # La rama que existe es origin/rama
```

Si ya tienes una rama local y quieres asignarla a una rama remota que acabas de traerte, o quieres cambiar la rama a la que le haces seguimiento, puedes usar en cualquier momento las opciones -u o --set-upstream-to del comando git branch.

```Bash
git branch -u origin/serverfix
  Branch serverfix set up to track remote branch serverfix from origin.
```

Más ejemplos:

```Shell
# Si tienes una rama en local y quieres hacer un seguimiento (track) de la rama su utiliza el comando:
git branch -u <nombreRemoto/nombreRamaRemota> <nombreRamaLocal>

# Asegurarte de que estás haciendo el seguimiento de una rama:
git branch --track <nombreRama> <nombreRepo/nombreRamaRepo>

# Si se crea una rama local y quieres subirla al repo y que se le haga seguimiento:
git push -u <nombreRepo> <nombreRama>
```
 
Si se crea una rama local y quieres subirla al repo y que se le haga seguimiento:
```
git push -u <nombreRepo> <nombreRama>
```

### ¿Cómo puedo ver mis ramas remotas?

```Shell
# Para ver las ramas remotas en un repositorio local:
git branch --all

# Para ver las ramas remotras de un repositoiro remoto:
git ls-remote

#para ver las ramas remotas de un repositorio remoto cuando hay varios repos remotos:
git ls-remote --heads <nombreRepoRemoto>
```

### Sobre eliminar ramas y tags en local y remoto

Para borrar **en LOCAL**

```Shell
# Borrar una rama en LOCAL, opción -D para forzar el borrado si esta no está merged
git branch -d feature/login
```

Para borrar **en REMOTO**

```
# Borrar una rama en REMOTO
git push <remoto> --delete <rama>
```

Para borrar **remote-tracking branch** (referencias a ramas remotas). [Buena fuente completa](https://stackoverflow.com/a/23961231/1820838)

```bash
git branch --delete --remotes <remote>/<branch>
git branch -dr <remote>/<branch> # Shorter

git fetch <remote> --prune # Delete multiple obsolete tracking branches
git fetch <remote> -p      # Shorter
git remote prune <remote> --dry-run # Checkear las ramas que pueden ser eliminadas
```

### Renombrar una rama en local y en REMOTO

```Shell
git branch -m old_branch new_branch         # Rename branch locally    
git push origin :old_branch                 # Delete the old branch    
git push --set-upstream origin new_branch   # Push the new branch, set local branch to track the new remote
```

### Crear una ráma 'huérfana'

```Shell
# Crear una rama huérfana (sin que deribe de un commit)
git checkout --orphan YourBranchName
```


## Log

```Shell
# Ver los ficheros que han cambiado
git log --name-only

# Ver los ficheros que han cambiado junto con sus estados
git log --name-status

# Ver los ficheros que han cambiado junto a estadísticas y sumario
git log --stat
```

## Deshacer

### Deshacer el último commit

### Nos hemos equivocado, ¿cómo se deshace el último commit?

Puede ser que te hayas olvidado de incluir algún fichero en el commit, arrepentido del mensaje, o que se haya colado algún fichero que debieras haber incluído en el .gitignore. De ser así, podrías querer deshacer el último commit.

**Es muy importante** decidir si aplicar la opción `--soft` o la opción `--hard`. Ya que esta última también descarta los últimos cambios y depositados previamente, *por lo que se perderán*.

```Shell

# Desace el commit de forma que los ficheros vuelven a estar en la stage area
git reset --soft HEAD~1


# PRECAUCIÓN porque elimnina los cambios efectuados!
git reset --hard HEAD~1
```

La misma técnica te permite volver a cualquier versión previa: `git reset --hard 0ad5a7a6`

Sin embargo, siempre ten en cuenta que al usar el comando `reset` **se deshacen todas las confirmaciones** que vinieron después del commit al que regresaste:

![Ilustración GIT](img/02-reset-concept.png)

**Estamos modificando la historia** y esto ya se sabe que no es muy conveniente si no está muy justificado en un sistema de control de versiones.

### Revisando un commit se comprueba que el correo-e es incorrecto, ¿cómo lo soluciono?

Hai 3 posibiliades:

1.La corrección la queremos hacer en el último commit:

```Shell
$ git commit --amend --author="Nombre usuario <nuevo_correo>">
```

2.La corrección la queremos hacer en otro commit anterior **(CUIDADO!)**: [Con Rebase](https://confluence.atlassian.com/bitbucketserverkb/how-do-you-make-changes-on-a-specific-commit-779171729.html) TODO: Pasar esta guía con un ejemplo.

```Shell
$ git rebase -p <id_commit>
$ git commit --amend --author="Nombre usuario <nuevo_correo>"
```

3. Empleando mecanismos más avanzados **(CUIDADO!)**:

```Shell
$ git filter-branch --commit-filter '
        if [ "$GIT_AUTHOR_EMAIL" = "<correo_@_antiguo" ];
        then
                GIT_AUTHOR_EMAIL=" <nuevo_correo>";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' HEAD
```

## Acceso a Git usando SSH

### ¿Cómo acceder a otro repo en una red local usando ssh?

TODO: (Puedes colaborar haciendo esto ;)

### ¿Cómo acceder a un repo en Bitbucket usando autenticación por par de claves?

TODO: (Puedes colaborar haciendo esto ;)

## Referencias Externas

* [Reference Manual](https://git-scm.com/docs)
* [Pro Git book](https://git-scm.com/book/en/v2)
