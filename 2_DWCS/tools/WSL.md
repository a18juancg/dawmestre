# Windows Subsystem for Linux (WSL)

## Referencias externas

* [Vídeo explicativo de puesta en marcha de WSL](https://youtu.be/CN4IkGNgtXk). Los temas tratados son:
  1. Versión de Windows soportada
  2. Instalación de Windows Terminal
  3. Activar características de Windows necesarias 
  4. Actualización del WSL 2 Kernel for Linux
  5. Descarga de Distribución de Linux de la Store
  6. Establecer versión de WSL por defecto
  7. Instalación y uso de distribución desde Windows Terminal
  8. Soporte para VS Code e instalación de complemento
  9. Instalación de Docker y soporte para WSL