# VS Code

VS Code es un IDE ligero muy popular en la actualidad por su gran capacidad de extensión con los plugins que ofrede y la gran comunidad que los sostienen.

## Entornos de desarrollo

Dependiendo del lenguaje, framework y tecnologías con las que se desarrolla, se recomendarán unas extensiones determinadas.

### Para documentar en Markdown

Id | Descripción
---|------------
 `yzhang.markdown-all-in-one` | Markdown All in One | Opciones muy útiles como la generación de toc
 `shd101wyy.markdown-preview-enhanced` | Markdown Preview Enhanced | Me pareció aceptable como previsualizador por el buen funcionamiento de su scroll
 `davidanson.vscode-markdownlint` | markdownlint | Nos fuerza a tratar de escribir de forma más homogénea

### Entorno de desarrollo para programar en PHP

Id | Nombre | Descripción
---|--------|------------
 `felixfbecker.php-pack` | PHP Extension Pack | Incluye a su vez dos extensiones para que funcione en IntelliSense con PHP y la depuración con soporte para XDebug.
 `sonarsource.sonarlint-vscode` | SonarLint | Mejoraremos al escribir código en PHP atendiendo a los avisos que obtendremos, y además también su explicación.

### Entorno de desarrollo para programar en Node Js

Id | Nombre | Descripción
---|--------|------------
 `ecmel.vscode-html-css` | HTML CSS Support | Ágiliza mucho sobre todo con la apertura y clausura de etiquetas.
 `xabikos.javascriptsnippets` | JavaScript (ES6) code snippets | Definición de code snippets muy útiles en ES6
 `pmneo.tsimporter` | TypeScript Importer | Nos ayudará con las importaciones

## Transversales o genéricas

Id | Nombre | Descripción
---|--------|------------
 `mhutchie.git-graph` | Git Graph | Múy útil para ver el gráfico de la historia de forma más agradeble que por terminal
 `coenraads.bracket-pair-colorizer` | Bracket Pair Colorizer | En ocasiones nos hacemos un lío con la apertura y cierres de llaves o paréntesis. Esto nos ayuda coroleándolos.
