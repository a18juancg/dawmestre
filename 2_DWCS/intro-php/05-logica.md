# Operadores lógicos

Los [operadores lógicos](https://es.wikipedia.org/wiki/Conectiva_lógica) son un concepto que proviene del Álgebra. Trataremos en PHP con operaciones lógicas como la [negación](https://es.wikipedia.org/wiki/Negación_lógica), [conjunción](https://es.wikipedia.org/wiki/Conjunción_lógica) y la [disyunción](https://es.wikipedia.org/wiki/Disyunción_lógica).

La sintaxis de los operadores lógicos es [la siguiente](https://www.php.net/manual/es/language.operators.logical.php).

### Ejemplos

```php
$a = 8;
$b = 3;
$c = 3;

// ¿Que imprime? ¿Que tipo de dato es?
echo ($a == $b) && ($c > $b),"</p>";

// ¿Que imprime? ¿Que tipo de dato es?
echo ($a == $b) || ($b == $c),"</p>";

// ¿Que imprime? ¿Que tipo de dato es?
echo !($b <= $c),"</p>";
```

### Aspectos reseñables

* La razón para tener las dos variaciones diferentes de los operadores `and` y `or` es que ellos operan con [precedencias](https://www.php.net/manual/es/language.operators.precedence.php) diferentes.

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/05-logica.php](http://localhost:9090/05-logica.php)

### [Volver al índice](README.md#Conceptos-basicos)