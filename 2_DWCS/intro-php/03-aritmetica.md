# Operadores aritméticos

¿Recuerda la [aritmética básica de la escuela](https://www.php.net/manual/es/language.operators.arithmetic.php)? Estos funcionan igual que aquellos.

## Ejemplo de declaración de variables

```php
//Inicialización de variables
$a = 8;
$b = 3;

//Operaciones con ellas e Impresión de valores
echo $a + $b,"<br>";
echo $a - $b,"<br>";
echo $a * $b,"<br>";
echo $a / $b,"<br>";
$a++;
echo $a,"<br>";
$b--;
echo $b,"<br>";
```

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/03-aritmetica.php](http://localhost:9090/03-aritmetica.php)

### [Volver al índice](README.md#Conceptos-basicos)