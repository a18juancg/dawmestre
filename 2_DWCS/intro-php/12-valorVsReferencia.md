# Valor y referencia en PHP

## Valor de una variable

```php
$a = [1,2,3];

//En $b tendremos una copia del array $a
$b = $a;
$b[count($b)] = 77;

//Al imprimir el contenido del array $a, veremos que NO se ha introducido el valor
print_r($a,1);
```

En PHP las asignaciones (`=`) se realizan por valor, al menos de tipos primitivos. Esto quiere decir que cuando se asigna la variable `$b`, **se está realizando una copia de todo el array** contenido en `$a` en la la variable `$b`.

## Referencias

Hay tres operaciones básicas que se realizan usando **referencias**:

* Asignar por Referencia
* Pasar por referencia
* [Devolver por referencia](https://www.php.net/manual/es/language.references.return.php)

### Asignar por referencia

En la primera de estas operaciones, las referencias de PHP permiten hacer que dos variables hagan referencia al mismo contenido. Es decir, cuando se hace:

```php
$a = &$b;
```

significa que `$a` y `$b` apuntan al mismo contenido. Nótese que `$a` y `$b` aquí son completamente iguales. `$a` no está apuntando a `$b` o viceversa. `$a` y `$b` están apuntando al mismo lugar (al contenido).

### Pasar por referencia

Esto se lleva a cabo haciendo que una variable local en una función y una variable en el ámbito de la llamada referencien al mismo contenido. Ejemplo de ello es lo siguiente:

```php
$x = [1,2,3];
$y = [4,5,6];

//Atención al paso por referencia que se hace con el operador '&', en la definición del parámetro $pOtroArray
function modificaArrays($pUnArray, &$pOtroArray){
    //Este array que se va a modificar NO fue pasado como referencia
    $pUnArray[count($pUnArray)] = 77;
    //Este array que se va a modificar SÍ fue pasado como referencia
    $pOtroArray[count($pOtroArray)] = 77;

}

modificaArrays($x,$y);

//Al imprimir el contenido del array $x, veremos que NO se ha introducido el valor (la función hace la modificación sobre una copia distinta dearray)
print_r($x,1);

//Al imprimir el contenido del array $y, veremos que Sí se ha introducido el valor (la función hace la modificación sobre el valor de $y porque se lpasa su referencia, no su valor)
print_r($y,1);
```

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/12-valorVsReferencia.php](http://localhost:9090/12-valorVsReferencia.php)

### [Volver al índice](README.md#Conceptos-basicos)