<?php include "cabecera.html" ?>

<pre class="parte">
	Formulario generado por PHP
</pre>

<div class="script-php">
<?php

//Las variables $_POST y $_GET son establecidas automáticamente por php
//y almacenarán los datos enviados por los formularios dependiendo del método de envío GET o POST
    if($_GET['nombre']!=NULL and $_GET['apellidos']!=NULL){
        echo "Hola ".$_GET['nombre']." ".$_GET['apellidos']."!";
        imprimeBotonSalir();
    }
    else{
        if($_GET['nombre']!=NULL or $_GET['apellidos']!=NULL){
            echo '<div style="color:red; border:solid 1px;">Datos incompletos</div>';
       }
        imprimeFormulario();
    }

   function imprimeFormulario(){
       echo '<FORM ACTION="10-forms.php" METHOD="GET">
               Introduce tu nombre:<INPUT TYPE="text" NAME="nombre"><BR>
               Introduce tus apellidos:<INPUT TYPE="text" NAME="apellidos"><BR>
               <INPUT TYPE="submit" VALUE="Enviar">
           </FORM> ';
   }

   function imprimeBotonSalir(){
       echo '<FORM ACTION="10-forms.php" METHOD="GET">
               <INPUT TYPE="submit" VALUE="Volver">
           </FORM> ';
   }

   //Trata de mejorar este código para que si introduces algo Así
   //como '<p>Carolina</p>' dentro del input nombre, no se vea afectada la
   //visualización de la página
   //htmlspecialchars($_POST['nombre'])
?>
</div>