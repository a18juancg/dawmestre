<?php include "cabecera.html" ?>

<pre class="parte">
//******************************
// Copia de una variable en otra
//******************************
$a = [1,2,3];

//En $b tendremos una copia del array $a
$b = $a;
$b[count($b)] = 77;

// ¿Cómo se imprimirá el array?
</pre>

<div class="script-php">
    <?php
        $a = [1,2,3];
				
        //En $b tendremos una copia del array $a
        $b = $a;
        $b[count($b)] = 77;
        
        //Al imprimir el contenido del array $a, veremos que no se ha introducido el valor
        print_r($a,1);
    ?>
</div>


<pre class="parte">
//************************************
// Utilizar una variable de referencia
//************************************
$a = [1,2,3];
				
//En $b tendremos una referencia al array $a. La variable $b no tiene su propio valor, sino que hace referencia al valor de $a
$b = &$a;
$b[count($b)] = 77;

// ¿Cómo se imprimirá el array?
</pre>

<div class="script-php">
    <?php
        $a = [1,2,3];
				
        //En $b tendremos una referencia al array $a. La variable $b no tiene su propio valor, sino que hace referencia al valor de $a
        $b = &$a;
        $b[count($b)] = 77;
        
        //Al imprimir el contenido del array $a, veremos que Sí se ha introducido el valor (se ha utilizado la referencia $b para hacerlo)
        print_r($a,1); 
    ?>
</div>

<pre class="parte">
//**************************************************************************
//Utilizar una variable de referencia como paso por parámetro a una función
//**************************************************************************
$x = [1,2,3];
$y = [4,5,6];

//Atención al paso por referencia que se hace con el operador '&', en la definición del parámetro $pOtroArray
function modificaArrays($pUnArray, &$pOtroArray){
	//Este array que se va a modificar NO fue pasado como referencia
	$pUnArray[count($pUnArray)] = 77;
	//Este array que se va a modificar SÍ fue pasado como referencia
	$pOtroArray[count($pOtroArray)] = 77;

}

modificaArrays($x,$y);

// ¿Cómo se imprimirán los arrays?
</pre>

<div class="script-php">
    <?php
        $x = [1,2,3];
        $y = [4,5,6];
        
        //Atención al paso por referencia que se hace con el operador '&', en la definición del parámetro $pOtroArray
        function modificaArrays($pUnArray, &$pOtroArray){
            //Este array que se va a modificar NO fue pasado como referencia
            $pUnArray[count($pUnArray)] = 77;
            //Este array que se va a modificar SÍ fue pasado como referencia
            $pOtroArray[count($pOtroArray)] = 77;
        
        }
        
        modificaArrays($x,$y);
        
        //Al imprimir el contenido del array $x, veremos que NO se ha introducido el valor (la función hace la modificación sobre una copia distinta del array)
        print_r($x,1);
        
        //Al imprimir el contenido del array $y, veremos que Sí se ha introducido el valor (la función hace la modificación sobre el valor de $y porque se le pasa su referencia, no su valor)
        print_r($y,1);
    ?>
</div>