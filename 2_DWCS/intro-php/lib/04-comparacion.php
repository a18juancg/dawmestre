<?php include "cabecera.html" ?>

<pre class="parte">
    *******************************************************
    * Declarando variables                                *
    *******************************************************
    $a = 8;
    $b = 3;
    $c = 3;
</pre>

<?php
    // Declaración de variables
    $a = 8;
    $b = 3;
    $c = 3;
    $d = 3.0;
?>

<pre class="parte">
    // ¿Que imprime? ¿Que tipo de dato es?
    echo $a == $b;
</pre>

<div class="script-php">
    <?php echo $a == $b ?> 
</div>

<pre class="parte">
    'a' es distinto de 'b' ?
    echo $a != $b;
</pre>

<div class="script-php">
    <?php echo $a != $b ?> 
</div>

<pre class="parte">
    'a' é maior que 'b' ?
    echo $a > $b;
</pre>

<div class="script-php">
    <?php echo $a > $b ?> 
</div>

<pre class="parte">
    'a' é maior ou igual a 'c' ? 
    echo $a >= $b;
</pre>

<div class="script-php">
    <?php echo $a >= $b ?>
</div>

<pre class="parte">
    'c' é menor estricto que 'b' ?
    echo $c < $b;
</pre>

<div class="script-php">
    <?php echo $c < $b;?>
</div>

<pre class="parte">
    'b' é menor ou igual a 'c' ?
    echo $b <= $c;
</pre>

<div class="script-php">
    <?php echo $b <= $c ?>
</div>

<pre class="parte">
    'd' es exactamente igual a 'c' ?
    echo $d === $c;
</pre>

<div class="script-php">
    <?php echo $d === $c ?>
</div>