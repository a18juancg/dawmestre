<html>
    <head>
        <title>Exemplo de PHP</title>
        <style>
            .entrada-blog {background-color:#222}
            h1 {color:#aaa;}
            .entrada-blog p {color:#ccc;}
        </style>
    </head>
    <body>

        Parte de HTML estático.
        <br><br>

        <?php
            //Isto é un comentario nunha liña
            echo "Parte de HTML dinámico generado con PHP<br>";
            
            /*Isto é un comentario
             en varias liñas*/
            
            for($i=0;$i<10;$i++) {
                echo("<div class=entrada-blog>");
                    echo("<h1>Titulo ".$i."</h1>");
                    echo("<p>Cuerpo</p>");
                echo("</div>");
            }
        ?>
    </body>
</html>