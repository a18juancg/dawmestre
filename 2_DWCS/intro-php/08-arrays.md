# Arrays en PHP

[Un array en PHP](https://www.php.net/manual/es/language.types.array.php) es en realidad un mapa ordenado. Un mapa es un tipo de datos que asocia valores con claves. Este tipo se optimiza para varios usos diferentes; se puede emplear como un array, lista (vector), tabla asociativa, diccionario, colección, pila, cola, y posiblemente más. Ya que los valores de un array pueden ser otros arrays, también son posibles árboles y **arrays multidimensionales**.

### Algunos ejemplos al respecto

```php

//Un array nos permite asignar más de un valor en una sola variable

//Poderemos crear un array asignando sus valores de forma rápida, utilizando los corchetes:
$datos= ['casa','coche','gato'];

// Podemos crear un array con diferentes valores, indexados por una clave (como si fuera un índice en un libro)
// clave puede ser un ''integer'' o ''string'' 
// valor puede ser cualquier valor
$datos= [
    'propietario' => 'Antonio',
    'domicilio' => 'Santiago de Compostela',
    'edad' => 45
];

// Esta matriz é a mesma que ...
$datos = array(5 => 43, 32, 56, 'b' => 12, 60);
echo "{$datos[5]}"; // Amosará 43
echo "<br/>{$datos[6]}"; // Amosará 32
echo "<br/>{$datos[7]}";// Amosará 56
echo "<br/>{$datos[8]}";// Amosará 60
echo "<br/>{$datos['b']}<hr/>"; // Amosará 12

// ...esta outra matriz. 
array(5 => 43, "b" => 12, 7 => 56, 6 => 32, 60);
echo "{$datos[5]}"; // Amosará 43
echo "<br/>{$datos[6]}"; // Amosará 32
echo "<br/>{$datos[7]}";// Amosará 56
echo "<br/>{$datos[8]}";// Amosará 60
echo "<br/>{$datos['b']}"; // Amosará 12

//Si declaramos el siguiente array...
$produtos = array("Azúcar", "Aceite", "Arroz");

//¿Cómo listarías todos los productos? Investiga...
//¿Soportaría la forma que has deducido un cambio que supone introducir un nuevo elemento, o tendrías que hacer alguna modificación?
```


## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/08-arrays.php](http://localhost:9090/08-arrays.php)

### [Volver al índice](README.md#Conceptos-basicos)