# Sesiones en PHP

**El soporte para sesiones está habilitado por defecto en PHP**. Si no quieres construir PHP con soporte para sesiones, debes especificar la opción `--disable-session` en la configuración. Para usar la asignación de memoria compartida (mm) para el almacenamiento de sesiones, configure PHP con `--with-mm[=DIR]`.

Sobre estos aspectos se basa el diseño y las necesidades de nuestra solución. Actualmente se tienen a diseñar soluciones *stateless* o *session-less* con ánimo de facilitar el escalado horizontal y la elasticidad en un cluster.

## Uso básico

Las sesiones son **una forma sencilla de almacenar datos** para usuarios de manera individual usando un ID de sesión único. Esto se puede usar **para hacer persistente la información de estado entre peticiones de páginas**. Los ID de sesiones normalmente son enviados al navegador mediante [cookies]() de sesión, y el ID se usa para recuperar los datos de sesión existente. **La ausencia de un ID o una cookie de sesión** permite saber a PHP para crear una nueva sesión y generar un nuevo ID de sesión.

## Ejemplo de registrar una variable con `$_SESSION`

```php
session_start();
if (!isset($_SESSION['count'])) {
  $_SESSION['count'] = 0;
} else {
  $_SESSION['count']++;
}
```

## Ejemplo de Desregistrar una variable con `$_SESSION`

**Importante**: NO destruya completamente `$_SESSION` con `unset($_SESSION)` ya que esto deshabilitará el registro de las variables a través del array superglobal `$_SESSION`.

```php
session_start();
unset($_SESSION['count']);
```

## Iniciar manualmente o autoiniciar la sesión

Las sesiones se puede iniciar manualmente usando la función `session_start()`, si la directiva session.`auto_start` se establece a `1`, una sesión se iniciará automáticamente ante cualquier petición de arranque.

Las sesiones normalmente se cierran automáticamente cuando PHP termina de ejecutar un script, pero se pueden cerrar manualmente usando la función `session_write_close()`.

## Profundizar sobre el manejo de sesiones

Puedes ampliar tus conocimientos sobre el manejo de sesiones en la [documentación oficial de PHP sobre el tema](https://www.php.net/manual/es/book.session.php).

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/11-sesiones.php](http://localhost:9090/11-sesiones.php)

### [Volver al índice](README.md#Conceptos-basicos)