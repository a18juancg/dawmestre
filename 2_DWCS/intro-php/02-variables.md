# Tipos y Variables en PHP

PHP admite **diez tipos primitivos**.

### Cuatro tipos escalares:

* [boolean](https://www.php.net/manual/es/language.types.boolean.php)
* [integer](https://www.php.net/manual/es/language.types.integer.php)
* [float](https://www.php.net/manual/es/language.types.float.php) (número de punto flotante, también conocido como [double](https://www.php.net/manual/es/language.types.float.php))
* [string](https://www.php.net/manual/es/language.types.string.php)

### Cuatro tipos compuestos:

* [array](https://www.php.net/manual/es/language.types.array.php)
* [object](https://www.php.net/manual/es/language.types.object.php)
* [callable](https://www.php.net/manual/es/language.types.callable.php)
* [iterable](https://www.php.net/manual/es/language.types.iterable.php)

### Y finalmente dos tipos especiales:

* [resource](https://www.php.net/manual/es/language.types.resource.php)
* [NULL](https://www.php.net/manual/es/language.types.null.php)

### Aspectos reseñables

* Es importante conocer que aunque tenemos debilidad en la definición de los tipos, podemos hacer [forzado](https://www.php.net/manual/es/language.types.type-juggling.php) o conversión de éstos.
* Es importante conocer las [Tablas de comparación de tipos de PHP](https://www.php.net/manual/es/types.comparisons.php). Si no tenemos en cuenta esta transformación de tipos, puede que nos resulte complejo la identificación de un bug.

## Declaración de Variables en PHP

En PHP las variables se representan con un signo de dólar seguido por el nombre de la variable. El nombre de la variable es sensible a minúsculas y mayúsculas.

### Ejemplo de declaración de variables

```php
// Primer fragmento
// Declaración de variables, nótese que php es débilmente tipado
$a = 1;
$b = 3.34;
$c = "Hola Mundo";

// Consultar la construcción del lenguaje "echo": https://www.php.net/manual/es/function.echo.php
echo $a,"<br>",$b,"<br>",$c,"<br>";

echo($c / 2);

// Comprobación del tipo
echo(var_dump($a)."Tipo de a<br/>");
echo(var_dump($b)."Tipo de b<br/>");
echo(var_dump($c)."Tipo de c<br/>");
echo(var_dump($x)."Tipo de x<br/>");
```

[Ver Ejemplo integrado](lib/02-variables.php)

### Aspectos reseñables

* Siempre se asignan por valor y no por referencia (en tipos primitivos)
* Tendremos un conjunto de [variables predefinidas](https://www.php.net/manual/es/reserved.variables.php)
* Existe la posiblidad de tener [nombres de variables que a su vez son variables](https://www.php.net/manual/es/language.variables.variable.php) (aspecto avanzado)
* Debemos tener en cuenta que las variables **pueden ser definidas en diferentes ámbitos** (se tratará en un apartado específico más adelante)

## Impresión de variables

Sobre este punto es importante revisar la documentación sobre:

* [Construcción del lenguaje `echo`](https://www.php.net/manual/es/function.echo.php) que muestra una o más cadenas.
  
* La [función `print`](https://www.php.net/manual/es/function.print) que muestra una cadena.

* La [función `print_r`](https://www.php.net/manual/es/function.print-r) que muestra información sobre una variable en una forma que es legible por humanos.

* La [función `printf`](https://www.php.net/manual/es/function.printf) que imprime una cadena con [formato especificado](https://www.php.net/manual/es/function.sprintf.php).

* La [función `var_dump`](https://www.php.net/manual/es/function.var-dump) que muestra información sobre una variable.

Algunos ejemplos sobre impresión de variables:

```php
echo "<hr/>";
echo "Ola Mundo";

echo "<hr/>";
echo "<pre>Este texto exténdese 
por varias liñas. Os saltos de liña 
tamén se envían. En modo cli consérvanse, 
en modo embebido en html tiene que estar 
entre las etiquetas de texto preformateado</pre>";

echo "<hr/>";
echo "Este texto exténdese \npor varias liñas. Os saltos de liña\ntamén se envían.";

echo "<hr/>";
echo "Para escapar caracteres, débese indicar \"deste xeito\".";

// Pódense empregar variables dentro da sentenza echo
$saudo = "que tal";
$despedida = "ata logo";

echo "<hr/>";
echo "Ola, $saudo";  // Ola, que tal

echo "<hr/>";
// Tamén se poden empregar arrays
$cadea = array("valor" => "saúdo dende un array");
echo "Este é un {$cadea['valor']} "; //Este é un saúdo dende un array

// Se se empregan comiñas simples, móstrase o nome da variable, non o seu valor
echo "<hr/>";
echo 'Ola, $saudo'; // ola, $saudo

// Se non se engade ningún carácter, tamén é posible empregar echo para mostrar o valor das variables 
echo "<hr/>";
echo $saudo;             // que tal
echo "<hr/>";
echo $saudo,$despedida;  // que talata logo

// O uso de echo con múltiples parámetros é igual que realizar unha concatenación
echo "<hr/>";
echo 'Esta ', 'cadea ', 'está ', 'construída ', 'con moitos parámetros.', chr(10);
echo "<hr/>";
echo 'Esta ' . 'cadea ' . 'está ' . 'construída ' . 'empregando concatenacion.' . "\n";

//También se puede imprimir utilizando la siguiente sentencia, formateada. Investiga los diferentes formatos en Internet (%d, $f, etc...)
printf("O numero dous con diferentes formatos: %d %f %.2f",2,2,2);
```

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090
```

Accede a [http://localhost:9090/02-variables.php](http://localhost:9090/02-variables.php)

### [Volver al índice](README.md#Conceptos-basicos)