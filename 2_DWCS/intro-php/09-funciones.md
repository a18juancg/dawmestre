# Funciones en PHP

[Una función](https://www.php.net/manual/es/functions.user-defined.php) puede ser definida empleando una sintaxis como la siguiente:

```php
function foo($arg_1, $arg_2, /* ..., */ $arg_n)
{
    echo "Función de ejemplo.\n";
    return $valor_devuelto;
}
```

### Aspectos reseñables

* PHP no admite la **sobrecarga de funciones**, ni es posible 'desdefinir' ni redefinir funciones previamente declaradas.

* Los nombres de las fuciones son insensibles a mayúsculas-minúsculas, aunque es una buena idea llamar a las funciones tal y como aparecen en sus declaraciones

* En PHP es posible llamar a funciones recursivas

* PHP admite el paso de argumentos por valor (lo predeterminado)

* Una función puede definir valores predeterminados al estilo de C++ para argumentos escalares como sigue: `function hacer_café($tipo = "capuchino")`

```php
function hacer_yogur($tipo = "acidófilo", $sabor) {
    return "Hacer un tazón de yogur $tipo de $sabor.\n";
}

echo hacer_yogur("frambuesa");   // no funcionará como se esperaba
```

* PHP admite [funciones anónimas](https://www.php.net/manual/es/functions.anonymous.php) y [funciones de flecha](https://www.php.net/manual/es/functions.arrow.php) a partir de la versión `7.4`.

## Comprueba su funcionamiento usando el servidor http interno de PHP

```bash
# Accede a la raiz de este repositorio y ejecuta el siguiente comando
% php -S localhost:9090 -t intro-php/lib

PHP 7.3.11 Development Server started at Thu Oct  8 10:43:04 2020
Listening on http://localhost:9090

```

Accede a [http://localhost:9090/09-funciones.php](http://localhost:9090/09-funciones.php)

### [Volver al índice](README.md#Conceptos-basicos)