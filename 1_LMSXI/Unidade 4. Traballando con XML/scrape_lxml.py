# Execute in Python2

from lxml import html
import requests

page = requests.get('https://www.iessanclemente.net/novas/')
tree = html.fromstring(page.content)
#This will create a list of buyers:
h2 = tree.xpath('//h2/text()')
print('H2: ', h2)

#This will create a list of prices
h2_con_clase = tree.xpath('//h2[@class="entry-title"]/a/text()')
print('H2 con clase: ', h2_con_clase)
