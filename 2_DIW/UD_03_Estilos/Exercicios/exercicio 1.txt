<!--
    A partir do código HTML e CSS que se mostra, engádelle os selectores CSS que faltan para aplicar os estilos desexados.
    Cada regra CSS vai precedida dun comentario no que se detallan os elementos HTML aos que debe aplicarse.
-->

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8" />
		<title>Exercicio de selectores</title>
            <style type="text/css">
            /* Todos os elementos da páxina */
            { font: 1em/1.3 Arial, Helvetica, sans-serif; }

            /* Todos os parágrafos da páxina */
            { color: #555; }

            /* Todos los párrafos contidos en #primeiro */
            { color: #336699; }

            /* Todas as ligazóns (enlaces) da páxina */
            { color: #CC3300; }

            /* Os elementos "em" contidos en #primeiro */
            { background: #FFFFCC; padding: .1em; }

            /* Todos os elementos "em" de clase "especial" en toda a páxina */
            { background: #FFCC99; border: 1px solid #FF9900; padding: .1em; }

            /* Elementos "span" contidos en .normal */
            { font-weight: bold; }

            </style>
	</head>
	<body>
        <div id="primeiro">
            <p>Lorem ipsum dolor sit amet, <a href="#">consectetuer adipiscing elit</a>. Praesent blandit nibh at felis. Sed nec diam in dolor vestibulum aliquet. Duis ullamcorper, nisi non facilisis molestie, <em>lorem sem aliquam nulla</em>, id lacinia velit mi vestibulum enim.</p> 
        </div>
            
        <div class="normal">
            <p>Phasellus eu velit sed lorem sodales egestas. Ut feugiat. <span><a href="#">Donec porttitor</a>, magna eu varius luctus,</span> metus massa tristique massa, in imperdiet est velit vel magna. Phasellus erat. Duis risus. <a href="#">Maecenas dictum</a>, nibh vitae pellentesque auctor, tellus velit consectetuer tellus, tempor pretium felis tellus at metus.</p>
            
            <p>Cum sociis natoque <em class="especial">penatibus et magnis</em> dis parturient montes, nascetur ridiculus mus. Proin aliquam convallis ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc aliquet. Sed eu metus. Duis justo.</p>
            
            <p>Donec facilisis blandit velit. Vestibulum nisi. Proin volutpat, <em class="especial">enim id iaculis congue</em>, orci justo ultrices tortor, <a href="#">quis lacinia eros libero in eros</a>. Sed malesuada dui vel quam. Integer at eros.</p>
        </div>

	</body>
</html>