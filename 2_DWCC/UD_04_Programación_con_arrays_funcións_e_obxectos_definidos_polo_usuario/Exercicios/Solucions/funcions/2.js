//Ámbito bloque
function localScope() {
    let i = 0; 
    if (true) {
        let i=1; 
        console.log(i); 
    }
    console.log(i); 
}


//Ámbito local
function localScope() {
    var i=1; //Local
    console.log(i); //1 (local, por preferencia sobre a global)
}

//Ámbito global
var i = 9; 

// Hoisting

//Para evitar o problema aconsellase o 
"use strict";
