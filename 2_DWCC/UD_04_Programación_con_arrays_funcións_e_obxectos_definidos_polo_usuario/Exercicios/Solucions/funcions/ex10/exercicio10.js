function sumMult5(n){
  //Verificar se é multiplo senon buscamos o mais proximo
  if (!(n%5==0)){
      n = n - (n%5);
  } 

  if(n==0) {
    return 0;
  } else{
    return n+sumMult5(n-5);
  }
}


//Suma de todos os valores dende 0 ao valor n
function suma(n){
  if (n==0){
    return 0;
  }
  return n+suma(n-1);
}


