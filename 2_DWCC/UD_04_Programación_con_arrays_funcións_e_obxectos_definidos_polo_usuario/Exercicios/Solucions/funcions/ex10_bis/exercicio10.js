function sumMult5(n){
  //Verificar se é multiplo senon buscamos o mais proximo
  if (!(n%5==0)){
      n = n - (n%5);
  } 

  if(n==0) {
    return 0;
  } else{
    return n+sumMult5(n-5);
  }
}


//Suma de todos os valores dende 0 ao valor n
function suma(n){
  if (n==0){
    return 0;
  }
  return n+suma(n-1);
}


//Producto
function factorial(n){
  if (1==n){
    return 1;
  }
  return n*factorial(n-1);
}


//Producto de multiplos de 5
function fact5(n){
  if (!(n%5==0)){
    n = n - (n%5);
  } 

  if(n==5) {
    return 5;
  } else{
    return n*fact5(n-5);
  }
};

// function modulo
function modulo(a,b){
  if(a>b){
    return b;
  }else{
    var c=b-a;
    console.log(c);
    return modulo(a,c);
  }
}