/*
Realiza una simulación de lanzar un dado 100 veces y almacena en un array los resultados. Muestra
la información relativa a esa simulación: 
• Valor medio de los lanzamientos.
• Número con mayor número de ocurrencias.
• Número con menor número de ocurrencias.
• Una tabla en la que se muestre el valor del dado y el número de ocurencias. 
*/


const tamano= 100;
let lista = new Array(tamano);

//Rellenamos o array cun numero aleatorio
for (i=0; i<lista.length; i++){
  lista[i]=numeroAleatorio(1,6);
}
console.log("Valores da lista");
console.log(lista.toString())

//Valor medio dos lanzamentos.
let medio = valorMedio(lista);

// Número con mayor número de ocurrencias.
let maisFrecuente = numeroMaisRepetido(lista);
document.getElementById("maisFrecuente").innerHTML="Mais frecuente : "+maisFrecuente;

//Numero menos frecunte
let menosFrecuente =numeroMenosRepetido(lista);
document.getElementById("menosFrecuente").innerHTML="Menos frecuente : "+menosFrecuente;

//tabla 
let tablaValores = tablaValoresDado(lista);





const tablaValoresDado =function tablaValoresDado(lista){
    var saida ="";
    let frecuencia = new Array();
    frecuencia = lista.reduce((contadorNome, item)=>{
        contadorNome[item] = (contadorNome[item] || 0) +1; 
        return contadorNome;
    },{});

    for (item in frecuencia){
        saida+="<p>  " + item + "==> "+ frecuencia[item]+ "</p>";
    }


    document.getElementById("resultado").innerHTML=saida;




}

const numeroMaisRepetido = function (lista){
    let frecuencia = new Array();
    frecuencia = lista.reduce((contadorNome, item)=>{
        contadorNome[item] = (contadorNome[item] || 0) +1; 
        return contadorNome;
    },{});

    console.log(frecuencia);

    var valorDado=0;
    var maior=0;
    for (item in frecuencia){
        if (frecuencia[item] >= maior){
            valorDado = item;
            maior= frecuencia[item];
        }
    }


    console.log("Numero maior: "+valorDado);
    return valorDado;

}
const numeroMenosRepetido = function (lista){
    let frecuencia = new Array();
    frecuencia = lista.reduce((contadorNome, item)=>{
        contadorNome[item] = (contadorNome[item] || 0) +1; 
        return contadorNome;
    },{});

    console.log(frecuencia);

    var valorDado=0;
    var menor=tamano;
    for (item in frecuencia){
        if (frecuencia[item] <= menor){
            valorDado = item;
            menor= frecuencia[item];
        }
    }


    console.log("Numero menos repitdo: "+valorDado);
    return valorDado;

}


//Funcions
const numeroAleatorio = function numeroAleatorio(min, max) {
    return Math.round(Math.random() * (max - min) + min);
  };


function sumatorio(total, item){
    return total+item;
}

//Valor medio dos lanzamentos.
const valorMedio = function (lista){
    var total = lista.reduce(sumatorio);
    return total/lista.length;
}


