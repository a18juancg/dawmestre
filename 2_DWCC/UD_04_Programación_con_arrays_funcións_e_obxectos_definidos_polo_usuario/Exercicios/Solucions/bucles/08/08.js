/**
 * 
 * En JavaScript todos os arrays son de 1 dimensión, pero 
 * como cada elemento pode ser á súa vez outro array, neste 
 * caso pódense considerar como multidimensionais.
 */

var weather = new Array();
weather[0] = ["Spring", "Summer", "Autumn", "Winter"];
weather[1] = [15, 25, 20, 10]; //Temperatura máxima
weather[2] = [25, 10, 30, 20]; //Precipitación media en L/m2


//Primeira forma
for (let linea  in weather){
  //  console.log(weather[linea]);
    for (let item  in weather[linea]){
        console.log(weather[linea][item]);
    }
}

console.log("Segunda forma");

//Segunda forma
for (let linea of weather) {
    for (let item of linea) {
        console.log(item);
    }
}


//Terceira forma
for (var i=0; i<weather.length; i++) { //Percorre a dimensión 1
    for (var j=0; j<weather[i].length; j++) { //Percorre a dimensión 2
    console.log(weather[i][j]);
    }
    }