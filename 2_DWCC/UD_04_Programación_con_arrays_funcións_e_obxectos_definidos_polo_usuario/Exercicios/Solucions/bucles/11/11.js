//Crear nº aleatorios
var len = parseInt(prompt("Enter a number: "));
var numbers = [];
getRandomNumbers(len);
var result = "<HR>"+ numbers.toString() +"<HR>";

//Percorrer o array elemento a elemento
for (let i=0; i<numbers.length; i++) {
    result += "<br>numbers["+i+"] = "+numbers[i]+" -> ";
    if (isPair(numbers[i])) {
        result += "PAR";
    } else {
        result += "IMPAR";
    }
}

document.getElementById("result").innerHTML = result;

//Indicar se un nº é par
function isPair(num) { return num%2 == 0; } 

//Obter nº aleatorios entre 1-9
function getRandomNumbers(len) {
    for (var i=0; i<len; i++) {
        numbers[i] = Math.trunc(Math.random()*9) + 1;
    }
}