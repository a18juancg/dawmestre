//
var ages = [40, 17, 40, 85, 24];
//Elimina o 1º elemento dun array modificando este.
var primeiro = ages.shift();
console.log("Primeiro: "+primeiro);

//Elimina o último elemento dun array modificando este.
var res = ages.pop(); //24
console.log(ages); //[ 17, 40, 85]

//push(). Engade un elemento ó final do array. 
var res = ages.push(85, 24); //Retorna a lonxitude
console.log(ages);

//unshift(). Engade un elemento ó principio do array. Devolve a nova lonxitude.
var res = ages.unshift(85, 24); //5
console.log(ages); 

var ages = [40, 17, 40, 85, 24];

ages.splice(-2, 2);
ages.splice(-2);

var android = ["samsung", "huawei", "xiaomi"];
var others = ["iphone","blackberry"];
var  union= android.concat(others)
//Outra alternativa para unir
android.push(...others);

//Buscar o índice
var res = android.indexOf("huawei");
android.sort();

//Invertir o array
android.reverse();

//Iteracion:some
/* 
Comproba se algún dos elementos do array pasa unha proba (proporcionada por unha función pasada como parámetro). Este método executa a función para cada elemento
*/
var ages = [40, 17, 85, 24];
function checkMaiorIdade(age){
    return age>=18;
}
var resultado = ages.some(checkMaiorIdade);


//Iteracion: every 
//Comproba se todos os elementos do array pasan unha proba  
var ages = [40, 17, 85, 24];
var res = ages.every(checkMaiorIdade); //false

//Iteracion: find
var ages = [15, 17, 85, 24];
var res = ages.find(checkMaiorIdade); //
//Se non encontra: retorna undefined 

//Iteracion: findIndex
var ages = [15, 17, 85, 24];
var res = ages.findIndex(checkMaiorIdade);

//Iteracion: filter
var ages = [15, 17, 85, 24];
var res = ages.filter(checkMaiorIdade);

//Iteracion: map 
var listaprezos=[100, 40,50];
function valorIVE(prezo){
    return prezo*1.21;
}

var listaIVE= listaprezos.map(valorIVE);

//Iteracion: reduce
function sumatorio(total, item){
    return total+item;
}


var total = listaIVE.reduce(sumatorio);


//Iteracion: reduceRight
var total2 = listaIVE.reduceRight(sumatorio);


//Probas
var lista=[0,1,1,1,1,0,1]
function valorBinario(total, item){
    return total=total+item*2^item;
}


var valor = lista.reduce(valorBinario);
var valor2 = lista.reduceRight(valorBinario);
console.log(valor2);
console.log(valor);


