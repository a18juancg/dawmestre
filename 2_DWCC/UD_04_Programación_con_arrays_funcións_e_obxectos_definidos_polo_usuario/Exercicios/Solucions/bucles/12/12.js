const nomes=['Manuel','Víctor','Elena','María','Balbina','Saladina','Manuel','Efrén','Elena','Filomena','Luisa','Luis','Lois'];


const frecuencia = nomes.reduce((contadorNome, item)=>{
    contadorNome[item] = (contadorNome[item] || 0) +1; 
    return contadorNome;
},{});


// Outra forma de facelo
const frecuencia2= nomes.reduce (miFuncion,{});

function miFuncion (contadorNome, item){
  contadorNome[item] = (contadorNome[item] || 0) +1; 
  return contadorNome;
}