let cesta = ["plátano", "plátano", 
"fresa", "naranja", "uva", "uva", 
"fresa", "plátano"];

const cestaAgrupada= cesta.reduce (miFuncion,{});

function miFuncion (contadorNome, item){
  contadorNome[item] = (contadorNome[item] || 0) +1; 
  return contadorNome;
}

//Saída: {plátano: 3, fresa: 2, naranja: 1, uva: 2}