# Exercicio 02 

Partindo da clase base que se proporciona, facer as seguintes tarefas:

- Engadir getters/setters á clase base
- Crear unha subclase para representar atletas coas seguintes características:
- Terá atributos para a altura (en cm) e peso (en kg).
- Terá métodos getters/setters.
- Terá un método para devolver o valor de todos os atributos nun string.
- Terá un método para calcular o BMI (Body Mass Index).

- Terá un método para adelgazar e outro para engordar nos que, se non se indica un valor, os cambios no peso será de 1 kg.
Crear un obxecto da subclase atleta e probar todos os métodos.

```js
//Clase persoa
class Person {
    constructor(name) { this._name = name; }
    print() { return "Hello! My name is " +this._name; }
}
```
## solución

```js
//Clase persoa
class Person {
    constructor(name) { this._name = name; }
    print() { return "Hello! My name is " +this._name; }

    //Getters/Setters
    get name() { return this._name; }
    set name(x) { this._name = x; }
}

//Subclase atleta
class Athlete extends Person {

constructor(name,height,weight) {
        super(name);
        this._height = height;
        this._weight = weight;
    }

    
    //Getters/Setters
    get height() { return this._height; }
    get weight() { return this._weight; }
    set height(x) { this._height = x; }
    set weight(x) { this._weight = x; }
    
    //Devolver atributos
    print() { return (super.print() +"\nHeight: "+ this._height +" cm.\nWeight: "+ this._weight +" kg."); }

    //Calcular BMI
    calcBMI() { return (this._weight / (this._height*this._height/10000)).toFixed(2); }

    //Adelgazar
    loseWeight(kg) { this._weight = (kg == undefined) ? this._weight-1 : this._weight-kg; }

    //Engordar
    gainWeight(kg) { this._weight = (kg == undefined) ? this._weight+1 : this._weight+kg; }
}

//Crear obxecto da subclase atleta
var runner = new Athlete("Usain Bolt",195,94);
//Invocar métodos do obxecto
runner.loseWeight();
runner.gainWeight(3);
console.log(runner.print());
console.log("BMI: " + runner.calcBMI());


```