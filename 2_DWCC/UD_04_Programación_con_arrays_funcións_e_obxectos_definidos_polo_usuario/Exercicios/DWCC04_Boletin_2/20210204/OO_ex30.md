# Exercicio 30 - Círculo
Realiza un programa que permita crear una clase para definir objetos de tipo círculo. Esta clase  tiene como atributos el radio que es inicializado al crear el objeto. Además esta clase pose los métodos área y perímetro. Una vez realizada la clase muestra en la consola diferentes casos de prueba de este objeto y sus métodos.

Observaciones:
- El atributo debe ser declarado como un elemento privado
- El objeto dispone de setters y getters  
- El constructor tiene como parámetro el radio .
- Verifica que no se puede acceder al atributo definido como privado. 

