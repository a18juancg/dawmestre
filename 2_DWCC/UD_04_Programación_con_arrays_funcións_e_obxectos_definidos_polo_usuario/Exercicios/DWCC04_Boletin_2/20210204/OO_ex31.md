# Ejercicio 31 - IMC
Realiza un programa que permita crear una clase para resolver el IMC (Índice de Masa Corporal) . El IMC= Kg/Altura^2 .  La clase IMC tiene como  atributos el peso y la talla que son inicializados al crear el objeto. Además esta clase pose los métodos  calcularIMC que retorna el valor de la fórmula y otro método ques clasificacionIMC que nos indica a que rango pertenece el IMC. Los rangos del IMC se definen en la siguiente tabla: 

![tabla IMC](resource/tabla.png)

## Segunda parte
Crea un array cos obxectos de IMC e ordena a colección de menor a maior dado o IMC. 

## Terceira parte
Crea un array cos obxectos de IMC e ordena a colección de maior a menor dado o peso. 
