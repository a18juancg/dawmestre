# Ejercicio 28 – discos.
Necesitamos almacenar en un programa todos los discos de música que tenemos en casa. Ahora que sabemos crear nuestros propios objetos para crear estructuras de datos ópmias. Tienes que:

    • Crear un objeto “disco” que almacene la siguiente información:
        ◦ Nombre del disco
        ◦ Grupo de música
        ◦ Año de publicación
        ◦ Tipo de música ( prodrá ser : rock, pop, punk, indie, rap, trap o clásica). 
        ◦ Localización: Indica el número de la estantería.
        ◦ Prestado: Almacena un valor booleano (por defecto es falso). 
    • Además tendrá los siguientes métodos:
        ◦ Un constructor sin parámetros. Los campos de tipo cadena serán inicializados a cadena vacía y los campos numéritoc s
        ◦ Un método que permita definir las las cinco primeras propiedades.
        ◦ Un método que permita cambiar el estado de la propiedad Prestado. 
        ◦ Un método que muestre toda la información de un disco. 
    • Guarda todo en un archivo .js con la misma nomenclatura que el ejercicio que estás a realizar. 