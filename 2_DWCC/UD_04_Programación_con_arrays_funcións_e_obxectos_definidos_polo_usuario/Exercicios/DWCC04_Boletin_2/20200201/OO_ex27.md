# Ejercicio 27 – Coches
Crea unha clase en JavaScript que permita representar obxectos de coche. Estes obxectos teñen os seguintes atributos:  marca, modelo, combustible, kilometros e color.
Pídese que a clase teña as seguintes características.
* Constructor sen parámetros
* Constructor con parámetros. (Revisa que sucede ao definir dous constructores)
* Getters e Setters de todos os atributos. 
* Redefine o método toString para que a saida mostre a información co seguinte formato:
  ```
    _marca ==> Ford
    _modelo ==> fiesta
    _combustible ==> Gasoil
    _kilometros ==> 1010
    _color ==> negro
  ```
* Crea unha lista con varios coches e mostra a súa información.
* Realiza probas para comprobar o funcionamento do obxecto para elo podes empregar as saidas por consola. 
* 