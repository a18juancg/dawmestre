## [Valores de un Equipo Scrum](https://www.paradigmadigital.com/techbiz/los-51-valores-de-equipos-scrum-altamente-efectivos/)

## [Motivación extrínseca, intrínseca y transcendente](https://montsealtarriba.com/2016/09/14/los-3-factores-de-la-motivacion/)

## [Motivación - 3 factores Autonomía Maestría y Propósito](https://www.gestiopolis.com/3-factores-motivacion-autonomia-maestria-proposito/)

## [Búsqueda de tu Ikigai](https://omegaforums.net/threads/ikigai-worthy-life.71685/?mc_cid=756a54c3b2&mc_eid=cf8257ea2b)

## [TED - "Dan Buettne: Cómo vivir más de 100 años. Ikigai Página](blob:https://embed.ted.com/450380da-e2b7-462f-8b53-e3ad9a41c01f)

