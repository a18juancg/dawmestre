drop database if exists clientes;
create database clientes;
use clientes;

CREATE TABLE producto (
  codprod INT NOT NULL primary key,
  DESCRIPCION VARCHAR(30) NOT NULL,
  precio FLOAT(8,2),
  CantidadEnStock FLOAT(8,2)
);
INSERT INTO producto 
VALUES (1,'MESA DESPACHO MOD. GAVIOTA',550,50),(2,'SILLA DIRECTOR MOD. BUFALO',670,25),(3,'ARMARIO NOGAL DOS PUERTAS',460,20),
	(4,'MESA MODELO UNIÓN',340,15),(5,'ARCHIVADOR CEREZO',1050,20),(6,'CAJA SEGURIDAD MOD B222',280,15),
	(7,'DESTRUCTORA DE PAPEL A3',450,25),(8,'MODULO ORDENADOR MOD. ERGOS',550,25);


CREATE TABLE IF NOT EXISTS empleado (
  numemp int NOT NULL,
  APELLIDO VARCHAR(20),
  OFICIO VARCHAR(20),
  DIRECTOR SMALLINT(4),
  FECHA_ALTA DATE,
  SALARIO FLOAT(8,2),
  COMISION FLOAT(8,2),
  DEP_NO SMALLINT(4),
  TELEFONO VARCHAR(15),
  PRIMARY KEY (numemp)
);

INSERT INTO empleado VALUES 
(7839,'REY','PRESIDENTE',NULL,'1981-11-17',6000,NULL,10,'981587147'),
(7698,'GARRIDO','DIRECTOR',7839,'1981-05-01',3850,NULL,30,'981246798'),
(7782,'MARTINEZ','DIRECTOR',7839,'1981-06-09',2450,NULL,10,'981246798'),
(7499,'ALONSO','VENDEDOR',7698,'1981-02-20',1400,400,30,'654987412'),
(7521,'LOPEZ','EMPLEADO',7782,'1981-05-08',1350,NULL,10,'696547982'),
(7654,'MARTIN','VENDEDOR',7698,'1981-09-28',1500,160,30,'983214589'),
(7844,'CALVO','VENDEDOR',7698,'1981-09-08',1800,0,30,'766514578'),
(7876,'GIL','ANALISTA',7782,'1982-05-06',3350,NULL,20,'981125147'),
(7900,'JIMENEZ','EMPLEADO',7782,'1983-03-24',1400,NULL,20,NULL),
(8998,'CORTES','VENDEDOR',7698,'1999-02-20',1800,NULL,30,'654879255'),
(7902,'GARRIDO','VENDEDOR',7782,'2007-02-06',1000,0,NULL,NULL)
;

CREATE TABLE IF NOT EXISTS cliente (
  codcliente int NOT NULL primary key,
  NOMBRE VARCHAR(25) NOT NULL,
  LOCALIDAD VARCHAR(14) NOT NULL,
  numvendedor int,
  DEBE FLOAT(10,2),
  HABER FLOAT(10,2),
  LIMITE_CREDITO FLOAT(10,2),
  constraint fk_cliente_emp foreign key (numvendedor) references empleado(numemp)
);

INSERT INTO cliente VALUES 
(101,'DISTRIBUCIONES GOMEZ','MADRID',7499,0,0,5000),
(102,'LOGITRONICA S.L','BARCELONA',7654,0,0,5000),
(104,'TALLERES ESTESO S.A.','SEVILLA',7654,0,0,5000),
(105,'EDICIONES SANZ','BARCELONA',7499,0,0,5000),
(106,'SIGNOLOGIC S.A.','MADRID',7654,0,0,5000),
(107,'MARTIN Y ASOCIADOS S.L.','ARAVACA',7844,0,0,10000),
(108,'MANUFACTURAS ALI S.A.','SEVILLA',7654,0,0,5000),
(103,'INDUSTRIAS LACTEAS S.A.','LAS ROZAS',7844,0,0,10000)
;
	
CREATE TABLE pedido (
  codpedido int primary key,
  codcliente INT,
  FECHA_PEDIDO DATETIME,
  estado varchar(20),
  constraint fk_pedido_cliente foreign key (codcliente) references cliente(codcliente)
);
INSERT INTO pedido (codpedido,codcliente,FECHA_PEDIDO,estado) VALUES (1,103,'2017-10-06 00:00:00','Pendiente'),
(2,106,'2017-10-06 00:00:00','Confirmado'),(3,101,'2017-10-07 00:00:00','Recibido'),(4,105,'2017-10-16 00:00:00','Confirmado'),
(5,106,'2017-10-20 00:00:00','Enviado'),(6,105,'2017-10-20 00:00:00','Enviado'),(7,103,'2017-11-03 00:00:00','Recibido'),
(8,101,'2017-11-06 00:00:00','Devuelto'),(9,106,'2017-11-16 00:00:00','Recibido'),(10,105,'2017-11-26 00:00:00','Recibido'),
(11,102,'2017-12-08 00:00:00','Recibido'),(12,106,'2017-12-15 00:00:00','Recibido'),(13,105,'2017-12-06 00:00:00','Pendiente'),
(14,106,'2017-12-06 00:00:00','Recibido'),(15,101,'2018-01-07 00:00:00','Recibido'),(16,105,'2018-01-16 00:00:00','Recibido'),
(17,106,'2018-01-18 00:00:00','En proceso'),(18,105,'2018-01-20 00:00:00','Recibido');

CREATE TABLE DETALLEPEDIDO(
	codpedido int not null,
	codprod int not null,
	UNIDADES smallint,
	importe decimal(8,2),
	constraint pk_detallepedido primary key (codpedido,codprod),
	constraint fk_detalle_pedido foreign key (codpedido) references pedido(codpedido),
	constraint fk_detalle_producto foreign key (codprod) references producto(codprod)
);
insert into detallepedido(codpedido,codprod,unidades) 
values (1,2,3),(1,5,2),(2,1,4),(3,2,4),(4,4,8),(5,3,2),(6,7,3),(7,5,2),(8,1,6),(9,2,2),(10,4,3),(11,3,2),
	(12,1,3),(13,3,2),(14,2,4),(15,7,4),(16,3,7),(16,4,7),(17,2,6),(18,2,3);
update detallepedido det set importe=unidades*(select precio from producto pr where det.codprod=pr.codprod);
