use clientes;
	
-- Ejemplo 1
SET AUTOCOMMIT=0; 

BEGIN;
-- se actualiza el stock 
UPDATE producto SET CantidadEnStock = CantidadEnStock - 2 WHERE codprod=1;
 
-- se inserta la cabecera del pedido 
INSERT INTO Pedido(codpedido,codcliente,FECHA_PEDIDO,estado) VALUES (19,102,date(now()),'Pendiente'); 

-- se inserta el detalle del pedido 
INSERT INTO DetallePedido(codpedido,codprod,unidades)  VALUES (19,1,2); 

-- aceptar transacción 
COMMIT; 

select * from producto where codprod=1;
select * from pedido where codpedido>=19;
select * from detallepedido where codpedido>=19;

-- -- Ejemplo 2
SET AUTOCOMMIT=0; 
start transaction;
-- se actualiza el stock 
UPDATE producto SET CantidadEnStock = CantidadEnStock - 2 WHERE codprod=1;
 
-- se inserta la cabecera del pedido 
INSERT INTO Pedido(codpedido,codcliente,FECHA_PEDIDO,estado) VALUES (20,103,date(now()),'Pendiente'); 

-- En este punto vamos a simular que la sesión se cierra de forma inesperada reiniciando el servidor. 
-- ¿Qué ocurrirá con los cambios realizados en la BD?
 
select * from producto where codprod=1;
select * from pedido where codpedido>=19;

-- -- Ejemplo 3
SET AUTOCOMMIT=0; 
BEGIN;
-- se actualiza el stock 
UPDATE producto SET CantidadEnStock = CantidadEnStock - 2 WHERE codprod=1;

savepoint s1;
 
-- se inserta la cabecera del pedido 
INSERT INTO Pedido (codpedido,codcliente,FECHA_PEDIDO,estado) VALUES (21,104,date(now()),'Pendiente'); 
-- se inserta el detalle del pedido 
INSERT INTO DetallePedido(codpedido,codprod,unidades)  VALUES (21,1,2); 
-- Hacemos un rollback al savepoint s1 
rollback to s1;
-- ¿Qué ocurrirá con los cambios realizados en la BD?
select * from producto where codprod=1;
select * from pedido where codpedido>=19;

-- rechazar transacción 
ROLLBACK; 

select * from producto where codprod=1;
select * from pedido where codpedido>=19;