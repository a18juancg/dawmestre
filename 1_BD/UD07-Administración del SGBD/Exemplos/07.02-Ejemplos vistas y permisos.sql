USE empresa_ventas;

/* VISTAS */

CREATE VIEW  v_empleados_vendedores  as 
	select * 
	FROM empleados WHERE oficio LIKE 'VENDEDOR';

INSERT INTO v_empleados_vendedores VALUES(1002,'Rodrigo', 'VENDEDOR', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
    
INSERT INTO v_empleados_vendedores VALUES(1003,'Mesa', 'ANALISTA', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
    
-- como ya existe, usamos create or replace y la creamos de nuevo con check options:
CREATE or replace VIEW  v_empleados_vendedores  as 
	select * 
    FROM empleados 
    WHERE oficio LIKE 'VENDEDOR' with check option;    

INSERT INTO v_empleados_vendedores VALUES(1006,'Coj�n', 'VENDEDOR', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
/* S� permite  meter esta fila */

INSERT INTO v_empleados_vendedores VALUES(1007,'Bola', 'ANALISTA', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
/* NO permite  meter esta fila porque no pertenecer�a a la vista */


CREATE VIEW  v_empleados_departamentoInvestigacion as 
	select * 
	FROM empleados e JOIN departamentos d ON (e.IdDepartamento = d.IdDepartamento)
	WHERE d.NombreDepartamento LIKE 'INVESTIGACION' with check option;   
-- es incorrecto, tiene atributos duplicados en el SELECT

CREATE or replace VIEW  v_empleados_departamentoInvestigacion as 
	select e.*, d.idDepartamento as iddepto,d.NombreDepartamento, d.LOCALIDAD 
	FROM empleados e JOIN departamentos d ON (e.IdDepartamento = d.IdDepartamento) 
	WHERE d.NombreDepartamento LIKE 'INVESTIGACION' with check option;    
-- ES CORRECTO

INSERT INTO v_empleados_departamentoInvestigacion (IdEmpleado, APELLIDO, OFICIO, IdJefe, FECHA_ALTA, SALARIO, COMISION, IdDepartamento, TELEFONO) 
VALUES(1014,'Cama', 'VENDEDOR', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
-- No funciona porque no metemos datos del departamento y por tanto no cumplir�a la check option

-- creamos de nuevo la vista pero sin la check option
CREATE or replace VIEW  v_empleados_departamentoInvestigacion as 
	select e.*, d.idDepartamento as iddepto,d.NombreDepartamento, d.LOCALIDAD 
	FROM empleados e JOIN departamentos d ON (e.IdDepartamento = d.IdDepartamento) 
	WHERE d.NombreDepartamento LIKE 'INVESTIGACION';  

INSERT INTO v_empleados_departamentoInvestigacion (IdEmpleado, APELLIDO, OFICIO, IdJefe, FECHA_ALTA, SALARIO, COMISION, IdDepartamento, TELEFONO) 
VALUES(1014,'Cama', 'VENDEDOR', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
-- ahora s� que funciona



CREATE or replace VIEW  V_empleados_Departamentos AS 
	SELECT e.IdEmpleado, d.IdDepartamento 
    FROM empleados e JOIN departamentos d ON e.IdDepartamento = d.IdDepartamento;

INSERT INTO V_empleados_Departamentos  VALUES(1009, 20);
-- ERROR: Can not insert into join view 'empresa.vempleados_departamentos' without fields list
-- falla porque para insertar datos en una vista tengo que poner la lista de campos que inserto expl�citamente en el insert
INSERT INTO Vempleados_Departamentos (IdEmpleado, IdDepartamento)  VALUES(1011, 20); 
-- ERROR: Can not modify more than one base table through a join view 'empresa.vempleados_departamentos'
-- falla porque intento insertar datos en una relaci�n 1:n

CREATE or replace VIEW  V_empleados_Departamentos AS 
	SELECT e.*, d.IdDepartamento as iddepto,d.nombredepartamento
    FROM empleados e JOIN departamentos d ON (e.IdDepartamento = d.IdDepartamento);

INSERT INTO V_empleados_Departamentos (empleados.IdEmpleado, empleados.APELLIDO, empleados.OFICIO, empleados.IdJefe, empleados.FECHA_ALTA, empleados.SALARIO, empleados.COMISION, empleados.IdDepartamento, empleados.TELEFONO) 
VALUES(9004,'Cama', 'VENDEDOR', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
-- ERROR: Unknown column 'empleados.IdEmpleado'
-- falla porque no puede especificarse el nombre de la tabla en la lista de atributos
INSERT INTO V_empleados_Departamentos (IdEmpleado, APELLIDO, OFICIO, IdJefe, FECHA_ALTA, SALARIO, COMISION, IdDepartamento, TELEFONO) 
VALUES(9004,'Cama', 'VENDEDOR', 7839, '2015-04-22', 3000, 1500, 30, '981123499');
-- OK: funciona, estamos introduciendo valores en s�lo una tabla e indicamos el nombre de los atributos (sin precederlos del nombre de la tabla)


/* Permisos */
	
create user usuario1 identified by 'usuario1'; -- si no indicamos �mbito, asigna %
create user usuario1@'%' identified by 'usuario1'; --  da error porque ya existe
	GRANT ALL ON empresa_ventas.empleados to usuario1; -- asigna todos los permisos en la tabla empleados de la BD empresa_ventas
	select * from mysql.tables_priv where user='usuario1' and host='%';
	GRANT ALL ON empresa_ventas.* to usuario1; -- asigna TODOS los permisos en TODOS los objetos de la BD empresa_ventas al usuario1
	select * from mysql.db where user='usuario1' and host='%';
	GRANT ALL ON *.* to usuario1; -- asigna TODOS los permisos en TODOS los objetos de TODAS las bases de datos al usuario1
	select * from mysql.user where user='usuario1' and host='%';
create user usuario2@localhost identified by 'usuario2';
create user usuario2@192.168.10.111 identified by 'otrousuario2';-- si lo creo en otro ambito, es un usuario distinto
create user usuario2@'%.empresa.com' identified by 'usuario2';-- podr�a conectarse desde cualquier subdominio del dominio
    GRANT UPDATE, INSERT, SELECT ON empresa_ventas.empleados TO usuario2@localhost;
create user usuario3@localhost, usuario4@equipo.remoto.com identified by 'user'; -- crea los dos pero solo asigna el pasword al segundo
	GRANT UPDATE, INSERT, SELECT ON empresa_ventas.empleados TO usuario3@localhost IDENTIFIED BY 'usuario3', usuario4@equipo.remoto.com; -- puedo asignar permisos a varios usuarios a la vez
	GRANT UPDATE(apellidos), SELECT(emp_no, nombre) ON empresa_ventas.empleados TO usuario2@localhost;
    GRANT SELECT ON empresa_ventas.* TO usuario2@localhost REQUIRE ssl;-- para tener los permisos tendr�a que conectarse con SSL
    GRANT SELECT ON empresa_ventas.* TO usuario2@localhost WITH MAX_QUERIES_PER_HOUR 300 MAX_UPDATES_PER_HOUR 30; -- limitamos el n�mero de consultas y updates que puede ejecutar por hora
	GRANT ALL ON *.* TO usuario2@localhost; -- tiene todos los permisos pero no puede conceder permisos a otros usuarios
    GRANT ALL ON *.* TO usuario2@localhost WITH GRANT OPTION; -- tiene todos los permisos y puede reasignarlos

	-- asignaci�n de permisos en distintos �mbitos
	CREATE USER usuario5@'localhost' IDENTIFIED BY 'usuario5';
	CREATE USER usuario5@'10.0.0.2' IDENTIFIED BY 'usuario5';
	CREATE USER usuario5@'%' IDENTIFIED BY 'usuario5';

	GRANT ALL ON *.*  TO usuario5@'localhost' ;
	GRANT SELECT, UPDATE ON empresa_ventas.empleados  TO usuario5@'10.0.0.2';
	GRANT SELECT, UPDATE ON empresa_ventas.clientes  TO usuario5@'10.0.0.2';
	GRANT SELECT (IdEmpleado,apellido,oficio)  ON empresa_ventas.empleados  TO usuario5@'%' ;
	GRANT SELECT  ON empresa_ventas.departamentos  TO usuario5@'%' ;
