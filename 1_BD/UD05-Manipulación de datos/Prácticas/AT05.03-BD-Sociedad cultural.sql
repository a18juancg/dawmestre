/****** CREACIÓN DE LA BD SOCIEDAD_CULTURAL******/
drop database if exists sociedad_cultural;
CREATE DATABASE SOCIEDAD_CULTURAL;

USE SOCIEDAD_CULTURAL;

/****** CREACIÓN DE TÁBOAS ******/

CREATE TABLE COTA(
	codi tinyint NOT NULL,
	nome varchar(20) NOT NULL,
	importe decimal(7, 2) NOT NULL,
 CONSTRAINT PK_COTA_codi PRIMARY KEY (codi),
 CONSTRAINT UQ_COTA_nome UNIQUE (nome)
);


INSERT INTO COTA (codi, nome, importe) VALUES (11, 'DE HONRA', 100.00);
INSERT INTO COTA (codi, nome, importe) VALUES (12, 'FAMILIAR', 30.00);
INSERT INTO COTA (codi, nome, importe) VALUES (13, 'HABITUAL', 50.00);
INSERT INTO COTA (codi, nome, importe) VALUES (99, 'GRATUITA', 0.00);



CREATE TABLE AULA(
	numero int NOT NULL,
	descricion varchar(30) NOT NULL,
	superficie int NOT NULL,
	estado char(1) NOT NULL,
 CONSTRAINT PK_AULA_numero PRIMARY KEY (numero),
 CONSTRAINT UQ_AULA_descricion UNIQUE (descricion)
);


INSERT INTO AULA (numero, descricion, superficie, estado) VALUES (1, 'PISTA DE TENIS', 270, 'B');
INSERT INTO AULA (numero, descricion, superficie, estado) VALUES (2, 'COCIÑA', 100, 'R');
INSERT INTO AULA (numero, descricion, superficie, estado) VALUES (3, 'AULA TALLER', 150, 'B');
INSERT INTO AULA (numero, descricion, superficie, estado) VALUES (4, 'AULA SUR', 80, 'M');
INSERT INTO AULA (numero, descricion, superficie, estado) VALUES (5, 'AULA NORTE', 50, 'B');



CREATE TABLE PROVINCIA(
	codi varchar(2) NOT NULL,
	nome varchar(50) NOT NULL,
	CONSTRAINT PK_PROVINCIA_codi PRIMARY KEY (codi),
	CONSTRAINT UQ_PROVINCIA_nome UNIQUE (nome)
);


INSERT INTO PROVINCIA (codi, nome) VALUES ('01', 'Álava');
INSERT INTO PROVINCIA (codi, nome) VALUES ('02', 'Albacete');
INSERT INTO PROVINCIA (codi, nome) VALUES ('03', 'Alacante');
INSERT INTO PROVINCIA (codi, nome) VALUES ('04', 'Almería');
INSERT INTO PROVINCIA (codi, nome) VALUES ('05', 'Ávila');
INSERT INTO PROVINCIA (codi, nome) VALUES ('06', 'Badajoz');
INSERT INTO PROVINCIA (codi, nome) VALUES ('07', 'Illes Balears');
INSERT INTO PROVINCIA (codi, nome) VALUES ('08', 'Barcelona');
INSERT INTO PROVINCIA (codi, nome) VALUES ('09', 'Burs');
INSERT INTO PROVINCIA (codi, nome) VALUES ('10', 'Cáceres');
INSERT INTO PROVINCIA (codi, nome) VALUES ('11', 'Cádiz');
INSERT INTO PROVINCIA (codi, nome) VALUES ('12', 'Castellón');
INSERT INTO PROVINCIA (codi, nome) VALUES ('13', 'Ciudad Real');
INSERT INTO PROVINCIA (codi, nome) VALUES ('14', 'Córdoba');
INSERT INTO PROVINCIA (codi, nome) VALUES ('15', 'A Coruña');
INSERT INTO PROVINCIA (codi, nome) VALUES ('16', 'Cuenca');
INSERT INTO PROVINCIA (codi, nome) VALUES ('17', 'Girona');
INSERT INTO PROVINCIA (codi, nome) VALUES ('18', 'Granada');
INSERT INTO PROVINCIA (codi, nome) VALUES ('19', 'Guadalajara');
INSERT INTO PROVINCIA (codi, nome) VALUES ('20', 'Guipúzcoa');
INSERT INTO PROVINCIA (codi, nome) VALUES ('21', 'Huelva');
INSERT INTO PROVINCIA (codi, nome) VALUES ('22', 'Huesca');
INSERT INTO PROVINCIA (codi, nome) VALUES ('23', 'Jaén');
INSERT INTO PROVINCIA (codi, nome) VALUES ('24', 'León');
INSERT INTO PROVINCIA (codi, nome) VALUES ('25', 'Lleida');
INSERT INTO PROVINCIA (codi, nome) VALUES ('26', 'La Rioja');
INSERT INTO PROVINCIA (codi, nome) VALUES ('27', 'Lugo');
INSERT INTO PROVINCIA (codi, nome) VALUES ('28', 'Madrid');
INSERT INTO PROVINCIA (codi, nome) VALUES ('29', 'Málaga');
INSERT INTO PROVINCIA (codi, nome) VALUES ('30', 'Murcia');
INSERT INTO PROVINCIA (codi, nome) VALUES ('31', 'Navarra');
INSERT INTO PROVINCIA (codi, nome) VALUES ('32', 'Ourense');
INSERT INTO PROVINCIA (codi, nome) VALUES ('33', 'Asturias');
INSERT INTO PROVINCIA (codi, nome) VALUES ('34', 'Palencia');
INSERT INTO PROVINCIA (codi, nome) VALUES ('35', 'Las Palmas');
INSERT INTO PROVINCIA (codi, nome) VALUES ('36', 'Pontevedra');
INSERT INTO PROVINCIA (codi, nome) VALUES ('37', 'Salamanca');
INSERT INTO PROVINCIA (codi, nome) VALUES ('38', 'Santa Cruz de Tenerife');
INSERT INTO PROVINCIA (codi, nome) VALUES ('39', 'Cantabria');
INSERT INTO PROVINCIA (codi, nome) VALUES ('40', 'Sevia');
INSERT INTO PROVINCIA (codi, nome) VALUES ('41', 'Sevilla');
INSERT INTO PROVINCIA (codi, nome) VALUES ('42', 'Soria');
INSERT INTO PROVINCIA (codi, nome) VALUES ('43', 'Tarrana');
INSERT INTO PROVINCIA (codi, nome) VALUES ('44', 'Teruel');
INSERT INTO PROVINCIA (codi, nome) VALUES ('45', 'Toledo');
INSERT INTO PROVINCIA (codi, nome) VALUES ('46', 'Valencia');
INSERT INTO PROVINCIA (codi, nome) VALUES ('47', 'Valladolid');
INSERT INTO PROVINCIA (codi, nome) VALUES ('48', 'Vizcaya');
INSERT INTO PROVINCIA (codi, nome) VALUES ('49', 'Zamora');
INSERT INTO PROVINCIA (codi, nome) VALUES ('50', 'Zaraza');
INSERT INTO PROVINCIA (codi, nome) VALUES ('51', 'Ceuta');
INSERT INTO PROVINCIA (codi, nome) VALUES ('52', 'Melilla');
INSERT INTO PROVINCIA (codi, nome) VALUES ('99', 'No española');



CREATE TABLE SOCIO(
	numero int NOT NULL,
	nif char(9) NULL,
	nss char(12) NULL,
	nome varchar(30) NOT NULL,
	ape1 varchar(30) NOT NULL,
	ape2 varchar(30) NULL,
	telefono1 char(9) NULL,
	telefono2 char(9) NULL,
	data_nac date NOT NULL,
	tipo_via_enderezo varchar(6) NOT NULL,
	nome_via_enderezo varchar(30) NULL,
	num_casa_enderezo varchar(30) NOT NULL,
	piso_enderezo varchar(5) NULL,
	localidade_enderezo varchar(40) NULL,
	codpostal_enderezo char(5) NULL,
	cod_provincia_enderezo varchar(2) NULL,
	tipo varchar(30) NOT NULL,
	profesion varchar(30) NULL,
	abonada char(1) NOT NULL default 'N',
	cod_cota tinyint NOT NULL,
CONSTRAINT PK_SOCIO_numero PRIMARY KEY (numero),
CONSTRAINT UQ_SOCIO_nif UNIQUE (nif)
);


INSERT INTO SOCIO (numero, nif, nss, nome, ape1, ape2, telefono1, telefono2, 
                   data_nac, tipo_via_enderezo, nome_via_enderezo, num_casa_enderezo, piso_enderezo, 
				   localidade_enderezo, codpostal_enderezo, cod_provincia_enderezo, 
				   tipo, profesion, abonada, cod_cota) 
VALUES (1000, '11111112A', '111111111112', 'MARÍA', 'GRAÑA', 'UMIA', '981111112', NULL, 
		'1975-05-05', 'RÚA', 'DO OLVIDO', '3', '2ºA', 
		'FERROL', '15011', '15', 'HONORÍFICO', NULL, 'S', 11);

INSERT INTO SOCIO (numero, nif, nss, nome, ape1, ape2, telefono1, telefono2, 
                   data_nac, tipo_via_enderezo,nome_via_enderezo, num_casa_enderezo, piso_enderezo, 
				   localidade_enderezo, codpostal_enderezo, cod_provincia_enderezo, 
				   tipo, profesion, abonada, cod_cota) 
VALUES (1001, '22222223B', '222222222223', 'MANUEL', 'SIEIRO', 'CAMPOS', '981222223', '639222223', 
		'1950-10-10', 'RÚA','SAN ROQUE', '15', NULL, 
		'A CORUÑA', '15025', '15', 'FAMILIAR', 'CARTEIRO', 'N', 12);

INSERT INTO SOCIO (numero, nif, nss, nome, ape1, ape2, telefono1, telefono2, 
                   data_nac, tipo_via_enderezo, nome_via_enderezo, num_casa_enderezo, piso_enderezo, 
				   localidade_enderezo, codpostal_enderezo, cod_provincia_enderezo, 
				   tipo, profesion, abonada, cod_cota) 
VALUES (1002, '33333334C', '333333333334', 'JORGE', 'DEL CARMEN', 'LÉREZ', NULL, '639333334', 
		'1990-06-11', 'PRAZA','DE ESPAÑA', '25-26', '11ºB', 
		'FERROL', '15006', '15', 'COMÚN', 'AVOGADO', 'S', 99);

INSERT INTO SOCIO (numero, nif, nss, nome, ape1, ape2, telefono1, telefono2, 
                   data_nac, tipo_via_enderezo, nome_via_enderezo, num_casa_enderezo, piso_enderezo, 
				   localidade_enderezo, codpostal_enderezo, cod_provincia_enderezo, 
				   tipo, profesion, abonada, cod_cota) 
VALUES (1003, '44444445D', '444444444445', 'CARLA', 'VIEITO', 'GIL', NULL, NULL, 
		'1985-07-12', 'RÚA', 'SAN ROQUE', '30', 'BAIXO', 
		'A CORUÑA', '15006', '15', 'HONORÍFICO', 'ESTUDANTE', 'S', 11);

INSERT INTO SOCIO (numero, nif, nss, nome, ape1, ape2, telefono1, telefono2, 
                   data_nac, tipo_via_enderezo, nome_via_enderezo, num_casa_enderezo, piso_enderezo, 
				   localidade_enderezo, codpostal_enderezo, cod_provincia_enderezo, 
				   tipo, profesion, abonada, cod_cota) 
VALUES (1004, '55555556E', '555555555556', 'SARA', 'RÍOS', 'SILVEIRA', NULL, '639555556', 
		'1989-05-30', 'PRAZA','DE ESPAÑA', '25-26', '11ºB', 
		'FERROL', '15006', '15', 'COMÚN', 'SECRETARIA', 'S', 99);

CREATE TABLE EMPREGADO(
	numero int NOT NULL,
	nif char(9) NOT NULL,
	nss char(12) NOT NULL,
	nome varchar(30) NOT NULL,
	ape1 varchar(30) NOT NULL,
	ape2 varchar(30) NULL,
	tipo_via_enderezo varchar(6) NOT NULL,
	nome_via_enderezo varchar(30) NULL,
	num_casa_enderezo varchar(30) NOT NULL,
	piso_enderezo varchar(5) NULL,
	localidade_enderezo varchar(40) NULL,
	codpostal_enderezo char(5) NULL,
	cod_provincia_enderezo varchar(2) NULL,
	tel_fixo char(9) NULL,
	tel_mobil char(9) NULL,
	salario_mes decimal(18, 2) NULL,
	car char(3) NOT NULL,
 CONSTRAINT PK_EMPREGADO_numero PRIMARY KEY (numero),
 CONSTRAINT UQ_EMPREGADO_nif UNIQUE (nif),
 CONSTRAINT UQ_EMPREGADO_nss UNIQUE (nss)
);


INSERT INTO EMPREGADO (numero, nif, nss, nome, ape1, ape2, tipo_via_enderezo, nome_via_enderezo, 
					  num_casa_enderezo, piso_enderezo, localidade_enderezo, codpostal_enderezo,
					  cod_provincia_enderezo, tel_fixo, tel_mobil, salario_mes, car) 
VALUES (100, '11111111A', '121212121212', 'MARÍA', 'GARCÍA', 'PÉREZ', 'RÚA', 'NOVA', 
		'10', '3º', 'SANTIA DE COMPOSTELA', '15125', 
		'15', '981111111', '639111111', 900.00, 'PRF');
INSERT INTO EMPREGADO (numero, nif, nss, nome, ape1, ape2, tipo_via_enderezo, nome_via_enderezo, 
					  num_casa_enderezo, piso_enderezo, localidade_enderezo, codpostal_enderezo,
					  cod_provincia_enderezo, tel_fixo, tel_mobil, salario_mes, car) 
VALUES (200, '22222222B', '131313131313', 'CARLOS', 'RE', 'PENA', 'PRAZA','DA CONSTITUCIÓN', 
		'30', '4º', 'A CORUÑA', '15002', 
		'15', '981222222', '639222222', 800.00, 'PRF');
INSERT INTO EMPREGADO (numero, nif, nss, nome, ape1, ape2, tipo_via_enderezo, nome_via_enderezo, 
					  num_casa_enderezo, piso_enderezo, localidade_enderezo, codpostal_enderezo,
					  cod_provincia_enderezo, tel_fixo, tel_mobil, salario_mes, car) 
VALUES (300, '33333333C', '141414141414', 'JUANA', 'POSE', 'VARELA',  'AV.', 'DE CASTELA', 
		'100', '5ºD', 'FERROL', '15104', '15', '981333333', '639333333', 1500.90, 'PRF');
INSERT INTO EMPREGADO (numero, nif, nss, nome, ape1, ape2, tipo_via_enderezo, nome_via_enderezo, 
					  num_casa_enderezo, piso_enderezo, localidade_enderezo, codpostal_enderezo,
					  cod_provincia_enderezo, tel_fixo, tel_mobil, salario_mes, car) 
VALUES (400, '44444444D', '151515151515', 'JOSÉ', 'NZÁLEZ', 'ÍNSUA', 'RÚA', 'MAIOR', 
		'25', '1ºIZQ', 'SANTIAGO', '15145', '15', '981444444', '639444444', 600.00, 'ADM');



CREATE TABLE PROFESORADO(
	num_prof int NOT NULL,
	especialidade varchar(50) NOT NULL,
 CONSTRAINT PK_PROFESORADO_num_prof PRIMARY KEY (num_prof,especialidade)
);


INSERT INTO PROFESORADO (num_prof, especialidade) VALUES (100, 'DEPORTES');
INSERT INTO PROFESORADO (num_prof, especialidade) VALUES (200, 'COCIÑA');
INSERT INTO PROFESORADO (num_prof, especialidade) VALUES (300, 'INFORMÁTICA');



CREATE TABLE ADMINISTRATIVO(
	num_adm int NOT NULL,
	horas_extras tinyint NOT NULL default 0,
 CONSTRAINT PK_ADMINISTRATIVO_num_adm PRIMARY KEY (num_adm)
);


INSERT INTO ADMINISTRATIVO (num_adm, horas_extras) VALUES (400, 50);


CREATE TABLE ACTIVIDADE(
	identificador int NOT NULL,
	nome varchar(50) NOT NULL,
	data_ini datetime NOT NULL,
	data_fin datetime NOT NULL,
	num_prazas tinyint NOT NULL,
	prezo decimal(7, 2) NOT NULL,
	observacions varchar(100) NULL,
	num_profesorado_imparte int NOT NULL,
	num_aula int NOT NULL,
 CONSTRAINT PK_ACTIVIDADE_identificador PRIMARY KEY (identificador),
 CONSTRAINT UQ_ACTIVIDADE_nome UNIQUE (nome)
);



INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (10, 'TENIS PARA PRINCIPIANTES', '2014-02-10 16:00', '2014-10-10 20:00', 15, 301.55, 'Precísase raqueta e 1 bote de pelotas', 100, 1);
INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (20, 'REPOSTARÍA', '2015-02-15 17:00', '2015-03-15 19:00', 20, 50.00, NULL, 200, 2);
INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (30, 'XADREZ', '2014-03-20 16:30', '2014-06-20 17:30', 10, 80.00, NULL, 100, 4);
INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (40, 'INICIACIÓN Á INFORMÁTICA', '2015-03-01 16:30','2015-05-01 17:30', 20, 0.00, 'Impartirase SW_libre', 300, 3);
INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (1, 'IOGA BASICO', '2018-02-01 16:00', '2018-04-30 20:00', 15, 30, NULL, 100, 4);
INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (2, 'IOGA AVANZADO', '2018-05-01 17:00', '2018-08-15 19:00', 20, 50, 'Require pelota e esterilla', 100, 4);
INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (3, 'PLASTICA', '2018-02-15 15:30', '2018-05-15 17:30', 10, 40.00, NULL, 200,1);
INSERT INTO ACTIVIDADE (identificador, nome, data_ini, data_fin, num_prazas, prezo, observacions, num_profesorado_imparte, num_aula) 
VALUES (4, 'CESTERÍA', '2018-03-01 16:30','2015-05-01 17:30', 20, 45, NULL, 300, 4);


CREATE TABLE SOCIO_REALIZA_ACTI(
	num_socio int NOT NULL,
	id_actividade int NOT NULL,
	pagada char(1) NOT NULL default 'N',
 CONSTRAINT PK_SOCIO_REALIZA_ACTI PRIMARY KEY (num_socio, id_actividade)
);


INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1000, 10, 'S');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1001, 10, 'N');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1001, 30, 'S');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1002, 40, 'N');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1003, 30, 'S');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1004, 1, 'S');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1004, 2, 'S');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1004, 3, 'S');
INSERT INTO SOCIO_REALIZA_ACTI (num_socio, id_actividade, pagada) 
VALUES (1004, 4, 'N');


CREATE TABLE PROFE_CURSA_ACTI(
	num_profesorado int NOT NULL,
	id_actividade int NOT NULL,
 CONSTRAINT PK_PROFE_CURSA_ACTI PRIMARY KEY (num_profesorado, id_actividade)
);

INSERT INTO PROFE_CURSA_ACTI (num_profesorado, id_actividade) VALUES (100, 40);
INSERT INTO PROFE_CURSA_ACTI (num_profesorado, id_actividade) VALUES (200, 30);

/*** CHECKS - VERIFICACIÓNS ***************/
ALTER TABLE ACTIVIDADE 
ADD  CONSTRAINT CHK_control_datas CHECK  ((data_fin >= data_ini));


/*** CLAVES FORÁNEAS OU ALLEAS ****************/
ALTER TABLE SOCIO   
ADD  CONSTRAINT FK_SOCIO_COTA_cod_cota
    FOREIGN KEY(cod_cota)
	REFERENCES COTA (codi)
	ON UPDATE CASCADE;


ALTER TABLE SOCIO  
ADD  CONSTRAINT FK_SOCIO_PROVINCIA_cod_provincia_enderezo 
	FOREIGN KEY(cod_provincia_enderezo)
	REFERENCES PROVINCIA (codi)
	ON UPDATE CASCADE;



ALTER TABLE EMPREGADO  
ADD  CONSTRAINT FK_EMPREGADO_PROVINCIA_cod_provincia_enderezo
	FOREIGN KEY(cod_provincia_enderezo)
	REFERENCES PROVINCIA (codi)
	ON UPDATE CASCADE;



ALTER TABLE PROFESORADO  
ADD  CONSTRAINT FK_PROFESORADO_EMPREGADO_num_prof 
	FOREIGN KEY(num_prof)
	REFERENCES EMPREGADO (numero);


ALTER TABLE ADMINISTRATIVO 
ADD  CONSTRAINT FK_ADMINISTRATIVO_EMPREGADO_num_adm
	FOREIGN KEY(num_adm)
	REFERENCES EMPREGADO (numero);


ALTER TABLE ACTIVIDADE  
ADD  CONSTRAINT FK_ACTIVIDADE_PROFESORADO_num_profesorado_imparte
	FOREIGN KEY(num_profesorado_imparte)
	REFERENCES PROFESORADO (num_prof)
	ON UPDATE CASCADE;


ALTER TABLE SOCIO_REALIZA_ACTI 
ADD  CONSTRAINT FK_SOCIO_REALIZA_ACTI_ACTIVIDADE_id_actividade
	FOREIGN KEY(id_actividade)
	REFERENCES ACTIVIDADE (identificador)
	ON UPDATE CASCADE;


ALTER TABLE SOCIO_REALIZA_ACTI  
ADD  CONSTRAINT FK_SOCIO_REALIZA_ACTI_SOCIO_num_socio
	FOREIGN KEY(num_socio)
	REFERENCES SOCIO (numero)
	ON UPDATE CASCADE;

ALTER TABLE ACTIVIDADE  
add constraint FK_ACTIVIDADE_AULA_num_aula
FOREIGN KEY(num_aula)
REFERENCES AULA (numero);

ALTER TABLE PROFE_CURSA_ACTI 
ADD  CONSTRAINT FK_PROFE_CURSA_ACTI_ACTIVIDADE_id_actividade
	FOREIGN KEY(id_actividade)
	REFERENCES ACTIVIDADE (identificador);


ALTER TABLE PROFE_CURSA_ACTI  
ADD  CONSTRAINT FK_PROFE_CURSA_ACTI_PROFESORADO_num_profesorado 
	FOREIGN KEY(num_profesorado)
	REFERENCES PROFESORADO (num_prof);

