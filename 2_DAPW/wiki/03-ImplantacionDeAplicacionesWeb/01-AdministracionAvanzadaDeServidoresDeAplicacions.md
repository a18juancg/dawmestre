﻿# Servidores de Aplicacións

## Introducción

Trataremos los siguientes aspectos:

* Características, arquitectura e estructura de ficheiros e directorios dun servidor de aplicacións. 

* Instalación, configuración  e uso de Tomcat sobre SO libre.

## Servidores de Aplicacións Web

Unha aplicación web defínese como unha aplicación informática que se executa nun entorno web, de xeito que é unha aplicación cliente-servidor xunto cun protocolo de comunicación establecido previamente:

    • Cliente: navegador.
    • Servidor: servidor web
    • Comunicación: protocolo HTTP
    
Un **servidor de aplicacións* é un paquete software que proporciona servicios ás aplicacións como poden ser seguridade, servizos de datos, soporte para transaccións, balanceo de carga e xestión de sistemas distribuidos. 
Os principais beneficios da aplicación da tecnoloxía de servidores de aplicación son a centralización e a diminución da complexidade no desenvolvemento de aplicacións.

Un servidor de aplicacións xeralmente xestiona a maior parte (ou a totalidade) das funcións de lóxica de negocio e de acceso aos datos da aplicación.  Este tipo de servidores non só serven para atender peticións http, senon que ademais son capaces de entender instruccións de linguaxes avanzados da web e traducirlas ou ben son capaces de acceder a recursos doutros servidores. 

Ese proceso faise de forma transparente ao usuario, é dicir o usuario pide o servicio a través, normalmente, do seu navegador e o servidor de aplicaciones atende a petición, e interpreta o código da aplicación a fin de traducirlo e mostrar ao usuario o resultado de forma entendible polo seu navegador.

Á forma de traballar dun servidor de aplicacións, coñécese normalmente como arquitectura de tres capas (a veces fálase de máis capas). 

Unha primeira capa é a do navegador que é capaz de traducir código do lado do cliente (HTML, JavaScript, CSS,...). Esa capa debe dispoñer de todos os compoñentes necesarios para facer esa labor no ordenador do usuario.

A segunda capa está formada polo servidor de aplicacións na súa labor de traducir código no lado do servidor (JSP, PHP, Ruby on Rails, Cold Fussion...) e convertilo ó formato entendible polo navegador.

A terceira capa son todos os servicios aos que accede o servidor de aplicacións para poder realizar a tarefa encomendada á aplicación.

A imaxe amosa o funcionamento dun sevidor de aplicacións PHP:

![Ilustración sevidor de aplicacións PHP](img/servidorAplicacionsPHP.png)

O termo foi inicialmente empregado para servidores da plataforma Java Enterprise Edition (J2EE), sendo o máis popular Apache Tomcat, pero existindo tamén outros coma WebLogic, de Oracle, pero na actualidade tamén se estende a outras tecnoloxías. P.ex. Microsoft fala do seu produto Internet Information Server como un servidor de aplicacións. E tamén se poden atopar servidores de aplicación de código aberto e comerciais doutros provedores, p.ex. Zope (aplicacións escritas en Python).
## Tomcat
**Apache Tomcat é un servidor de aplicacións Java** (OpenSource) desarrollado baixo o proxecto Jakarta na Apache Software Foundation para aloxar [servlets](https://es.wikipedia.org/wiki/Java_Servlet) e [Java Server Pages (JSP)](https://es.wikipedia.org/wiki/JavaServer_Pages).

Tomcat pode empregarse como servidor web independiente ou ben coma unha extensión do servidor web xa instalado (no noso caso empregaremos esta opción).
Entre as súas características destacan;
É un servidor multiplataforma (Linux, Unix, Windows...)
Estable e lixeiro
Máis fácil de administrar e configurar que “servidores de aplicacións completos”

**Tomcat está formado por varios compoñentes que se organizan de forma xerárquica:**

![](img/arquitecturaTomcat.png)

* **Server**: Primeiro elemento contedor. Pode conter un ou varios Services.

* **Service**: Asocia un ou máis Connectors cun único Engine

* **Connector**: É unha asociación cun porto IP para manexar as peticións e as respostas cos clientes. Por defecto hai un conector HTTP creado.

* **Engine**: é un colector dun ou mais Hosts. É posible configurar Virtual Hosts. Recibe as peticións dos conectores e trasladaas ao Host correspondente.

* **Host**: Define un servidor virtual e pode conter unha ou máis aplicacións web (*webapps*). Cada unha delas represéntase por un Context.

* **Context**: Cada *webapp* executándose dentro dun Host, está representada por un *Context*.

Na instalación por defecto, existe un host configurado, chamado localhost. As aplicacións do host colócanse en `$CATALINA_BASE\webapps`

## Xerarquía de directorios de Apache Tomcat

* `bin`: contén os scripts de arranque, parada.

* `common`: clases comúns que poden empregar *Catalina* e as aplicacións web.

* `conf`: ficheros XML e a corresponente DTD para a configuración de Apache Tom-
cat.

* `logs`: logs do contenedor de servlets e das aplicacións.

* `server`: clases empregadas polo contenedor de servlets

* `shared`: clases compartidas por todas as aplicacións web.

* `webapps`: directorio que conten as aplicacións web.

* `work`: almacenamento temporal de ficheiros e directorios.

## Catalina, Coyote e Jasper

A partir da versión 4.x Tomcat lanzouse co contenedor de servlets *Catalina*, co contenedor HTTP *Coyote* e cun motor para JSP chamado *Jasper*.

As principais características destes tres compoñentes son:

* **Catalina**: Implementa as especificacións de servlets e JSP. Para Apache Tomcat o elemento principal é unha base de datos de nomes de usuarios, password e roles. Catalina integrase onde xa existe información de autenticación como describe a especificación de servlets.

* **Coyote**: Compoñente conector que admite o protocolo HTTP 1.1 para o servidor web e que escoita nun porto TCP especificado polo servidor e envía a solicitude ó motor Tomcat para que éste a procese e envie unha resposta o cliente.

* **Jasper**: Analiza arquivos JSP para compilar o código Java e, si se producen cambios, éste volveos a compilar.


## Reconocimento

Los autores originales de este exto fueron la Profesora Ignacia González Yáñez.

![Reconocimiento y licencia](img/licencia-03.01.png)
