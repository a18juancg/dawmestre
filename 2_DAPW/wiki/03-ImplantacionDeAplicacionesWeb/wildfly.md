﻿

## **El servidor de aplicaciones Jboss/Wildfly.**

**El servidor JBoss es un proyecto de código abierto, con el que se consigue un servidor de aplicaciones basado en J2EE, e implementado al 100 % en Java.**

Mientras el Tomcat es un Servlet Container, JBoss es un Application Server, que soporta funciones de J2EE, las más importantes son los EJB's y el clustering. Tomcat por sí solo simplemente sirve para JSP's y servlets.

JBoss es un servidor de aplicaciones basado en Java mientras que Tomcat es un contenedor de servlets.

Uno de los rasgos más importantes de JBoss es su apoyo a la implementación "en caliente". Lo que significa es que implementar un nuevo EJB es tan simple como copiar el archivo correspondiente en el directorio correspondiente. Si esto se hace mientras el bean ya está cargado, JBOSS lo descarga automáticamente, y entonces carga la nueva versión.

JBoss está compuesto por dos partes: un "Servlet Engine" y un "EJB Engine", dentro del "Servlet Engine" se ejecutan exclusivamente las clásicas aplicaciones de un servidor (JSP's y Servlets), mientras el "EJB Engine(Container)" es reservado para aplicaciones desarrolladas alrededor de EJB's o Enterpise Java Bean's.

Entre las características destacadas de JBoss destacamos las siguientes:

-   Producto de licencia de código abierto sin coste adicional.
    
-   Cumple los estándares.
    
-   Confiable a nivel empresa.
    
-   Incrustable, orientado a arquitectura de servicios.
    
-   Flexibilidad consistente.
    
-   Servicios de middleware para cualquier objeto de Java.
    

El creador de la primera versión de JBoss fué Marc Fleury quién fundó una empresa de servicios llamada JBoss Inc., adquirida en 2006 por Red Hat.

-   **JBoss** **Enterprise Application Platform**.
    
    -   JBoss EAP es el nombre del servidor de aplicaciones Java EE que Red Hat produce y soporta. Implementa Java EE 7
        
    -   Diseñado para construir, desplegar y albergar servicios, y aplicaciones Java.
        
-   **JBoss AS/Wildfly**
    
    -   WildFly y JBoss AS son las versiones comunitaras de JBoss.
        
    -   El término "JBoss Application Server" se usaba desde el principio, sin embargo para evitar confusiones con la versión de soporte (JBoss EAP,), se renombró a WildFly.
        
    -   JBoss AS/WildFly están orientados a pruebas y desarolladores.
        
    -   Algunas versiones son en realidad bastante estables y _podrían_ ejecutarse en producción, pero sin soporte de Red Hat, sólo de la propia comunidad.
        
-   **JBoss** **Web Server**
    
    -   JBoss Web Server está penssado para desplegar aplicaiones basadas en Java Server Pages (JSP), Java Servlet, PHP y CGI.
        
    -   Está pensado para aplicaciones medianas y grandes.
        
    -   Combina el servidor web Apache y el servidor de aplicaciones Tomcat bajo una suscripción de soporte
        

  

# **Instalación y configuración básica.**

Vamos a partir de una máquina Ubuntu 20.04 LTS, en la que realizaremos el proceso de instalación y cónfiguración básica del servidor JBoss/Wildfly y que vamos a estructurar en los siguientes pasos:

1.  **Descargar e instalar el** **Java Development Kit** **(****JDK****)**: Es necesario tener instalado JDK ya que se trata de un servidor de aplicaciones implementado al 100 % en Java, y puede ser ejecutado en cualquier sistema con JDK versión 1.5 o superior.
    
    `# apt update`
    `# apt install default-jdk`
    
2.  **Crear el usuario de** **Wildfly**: Es recomendable ejecutar Wildfly con una cuenta de usuario no root, con privilegios mínimos. Para ello crearemos un grupo `wildfly` y un usuario llamado `wildfly` que agregaremos al grupo creado;
    
    `sudo groupadd -r wildfly`
    `sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly`
    
3.  **Descargar e instalar de** **Wildfly 22.1.0**: Se pueden descargar las distintas versiones del servidor Wildfly del siguiente enlace,    [Wildfly](https://wildfly.org/downloads/) en este caso hemos decidido descargar el paquete `wildfly-22.1.0.Final.zip`.
    
    Para proceder a su instalación primeramente lo descargamos a la carpeta `/tmp`:

`sudo wget https://download.jboss.org/wildfly/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz -P /tmp`

A continuación descomprimimos el archivo y lo movemos al directorio `/opt`:

`sudo tar xvf /tmp/wildfly-$WILDFLY_VERSION.tar.gz -C /opt/`

Creamos el enlace simbólico para Wildfly:

`sudo ln -s /opt/wildfly-$WILDFLY_VERSION /opt/wildfly`

Le pasamos la propiedad del directorio al usuario `wildfly`:

`sudo chown -RH wildfly: /opt/wildfly`

8.  **Configurar** `system.d` **y archivo** `widfly.conf`. Creamos el directorio para guardar el archivo de configuración:

**`sudo mkdir -p /etc/wildfly`**

Copiamos el archivo de configuración al directorio:

`sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/`

Copiamos el script `launch.sh` script al directorio `/opt/wildfly/bin/`:

`sudo cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/`

Le ponemos el atributo ejecutable a los ficheros del directorio `bin`:

`sudo sh -c 'chmod +x /opt/wildfly/bin/*.sh'`

Finalmente, copiamos el archivo del servicio al directorio`/etc/systemd/system/`:

**`sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/`**

Recargamos `system.d`, arrancamos el servicio Wildfly y comprobamos que esté funcionado:

`sudo systemctl daemon-reload`
`sudo systemctl start wildfly`
`sudo systemctl status wildfly`

3.  **Configurar la autenticación de Wildfly**. Agregamos un usuario ejecutando el script `add.user.sh`:
    
    `sudo /opt/wildfly/bin/add-user.sh`
    
    Nos aparecerá una pregunta, y seleccionamos la opción `a`.
    
    `What type of user do you wish to add?` 
     `a) Management User (mgmt-users.properties)` 
     `b) Application User (application-users.properties)`
    `(a):`
    
    A continuación añadimos un usuario, en el ejemplo `usuario/usuario`:
    

`Enter the details of the new user to add.`
`Using realm 'ManagementRealm' as discovered from the existing property files.`
`Username : usuario`
`Password recommendations are listed below. To modify these restrictions edit the add-user.properties configuration file.
,,,,,,,,`
`Added user 'usuario' to file '/opt/wildfly-19.1.0.Final/standalone/configuration/mgmt-users.properties'`
`Added user 'usuario' to file '/opt/wildfly-19.1.0.Final/domain/configuration/mgmt-users.properties'`
`Added user 'usuario' with groups  to file '/opt/wildfly-19.1.0.Final/standalone/configuration/mgmt-groups.properties'`
`Added user 'usuario' with groups  to file '/opt/wildfly-19.1.0.Final/domain/configuration/mgmt-groups.properties'`
`Is this new user going to be used for one AS process to connect to another AS process?` 
`e.g. for a slave host controller connecting to the master or for a Remoting connection for server to server EJB calls.`
`yes/no? y`
`To represent the user add the following to the server-identities definition <``secret` `value``="``dXN1YXJpbw``=="`  `/>`

Por último, ya podemos verificar la instalación de Wildfly, tecleando:

`sudo systemctl status wildfly`

O accediendo desde el navegador en las direcciones:

`http://<``dirección_IP``>:8080 ó http://localhost:8080`

4.  **Acceder a la consola de administración de** **Wildfly**: Asegurarse que Widlfly se ha iniciado y de que conseguimos acceder a la consola Wildfly desde las siguientes direcciones:
    
    `http://<``dirección_IP``>:9990 ó http://localhost:9990`
    
    Y accedemos con el usuario creado anteriormente.

Para realizar la instalación y configuración básica del servidor JBoss 6.0, debemos seguir, de forma secuencial, todos y cada uno de los siguientes pasos:

1.  **Descargar e instalar el** **Java Development Kit** **(****JDK****)**.
    
2.  **Crear el usuario de** **Wildfly**.
    
3.  **Descargar e instalar de** **Wildfly 19.1.0**.
    
4.  **Configurar** `system.d` **y archivo** `widfly.conf`.
    
5.  **Configurar la autenticación de Wildfly**.
    
6.  **Acceder a la consola de administración de** **Wildfly**.
    

# **3.2.- Despliegue de aplicaciones empresariales.**

La estructura de una aplicación web en su forma más sencilla, debe contener la siguiente estructura de directorios:

-   META-INF/
    
-   manifest.mf
    
-   WEB-INF/
    
-   classes/
    
-   src/
    
-   lib/
    
-   web.xml
    

Conteniendo la carpeta **META-INF**,el archivo **manifest.mf**, que contiene la lista de contenidos de la aplicación, y que son generados al momento de crearla. El directorio **WEB-INF** contiene todos los archivos necesarios para ejecutar la aplicación, y estructura su contenido en las carpetas **classes** que contiene las clases compiladas para la aplicación, **lib** con las librerías necesarias para la aplicación y **src**, con el código fuente de la aplicación.

Una vez que la aplicación JEE está correctamente construida, se realiza el empaquetado con el comando:

`# jar cvf nombre_aplicacion.jar carpetas/ficheros_a_empaquetar`

Una vez tenemos la aplicación .jar para desplegarla, únicamente la copiamos a la carpeta "_$JBOSS_HOME/server/default/deploy_" y el propio JBoss nos dará un mensaje similar a _deploy, ctxPath = / nombre_aplicacion_, lo que quiere decir que la aplicación ha sido desplegada correctamente; esto se conoce como despliegue en caliente.

# **Estructura de carpetas de una aplicación empresarial. Archivo EAR.**

En el mundo Java EE tenemos tres posibles tipos de aplicaciones: aplicaciones web, objetos distribuidos EJBs y aplicaciones empresariales, que no son más que un conjunto de las dos anteriores aplicaciones.

Una aplicación empresarial Java EE (archivo .EAR) es un conjunto de módulos, siendo un módulo una aplicación web completa (empaquetada en un archivo .war) o conjunto de objetos distribuidos EJBs (empaquetados en un archivo .jar).

Podemos resumir que la estructura del archivo EAR es:

-   **`/*.`****`war`****:** Archivos war.
    
-   **`/*.jar`****:** Archivos (ejb) jar.
    
-   **`/META-INF/application.xml`****:** Descriptor de despliegue del módulo EAR, en donde se dan de alta y se declaran el nombre y descripción de la aplicación que se despliega, y los diferentes módulos web y EJB que forman la aplicación.
    

Vamos a suponer una estructura lo más sencilla posible para una aplicación web como la siguiente, y que es la que constituye el archivo "_aplicacion.__war_":
![enter image description here](img/figura1.png)  

_donde observamos una página estática "index.html" y un descriptor del despliegue "__web.xml__", a partir de esta estructura pretendemos construir nuestro propio archivo_ _EAR_ _que contendrá un solo archivo_ _WAR_ _con una página HTML estática._

_Una vez situados en la carpeta "aplicacion", mediante el comando # jar cvf aplicacion.__war_ _* generaremos el archivo .__WAR_ _correspondiente a la aplicación; podremos comprobar que se trata de un formato similar a los archivos .zip probando a abrirlo con un programa compresor._

_Para construir el archivo .__EAR__, como mínimo, tendremos que crear un descriptor de despliegue al que llamaremos "aplicacion.xml", para ello creamos una carpeta llamada "temporal" en donde situamos el archivo "aplicacion.__war__"; en la misma ruta creamos una carpeta llamada "META-INF" donde vamos a crear el descriptor; quedando la estructura del siguiente modo:_
![enter image description here](img/figura2.png)

Nos situamos dentro de la carpeta "temporal" y creamos el archivo .ear mediante el comando:

`# jar cvf aplicacion.ear *`

y tendremos así el archivo .ear correspondiente a la aplicación creada.
