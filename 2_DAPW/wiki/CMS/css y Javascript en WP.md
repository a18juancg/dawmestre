﻿
# incluir Javascript y CSS en WordPress
**Existen varias maneras de incluir código Javascript y CSS en WordPress**. Por ejemplo, podemos incluir un script o reglas CSS en el [header o en el footer](https://wpdirecto.com/insertar-codigo-header-footer/) de nuestro tema. También podemos insertar scripts mediante el archivo functions.php creando _action hooks_, podemos agregarlos en plugins dedicados a esto mismo y de muchas, pero muchas maneras. El hecho de escoger el método equivocado a la hora de incluir un script de Javascript o [reglas de CSS](https://wpdirecto.com/anadir-codigo-css/) en nuestro WordPress puede darnos muchos dolores de cabeza, sobre todo en lo relativo al mantenimiento de nuestro sitio. Si no conoces los códigos añadidos, puedes llegar incluso a duplicarlos. Por eso, **en este tutorial te mostraré la mejor manera de incluir Javascript y CSS en WordPress**.

WordPress cuenta con un método para incluir Javascript y CSS en WordPress. **Es decir, podemos utilizar las funciones wp_register_script(), wp_enqueue_script() y wp_register_style(), wp_enqueue_style() para hacer que nuestros códigos, tanto Javascript como CSS, se pongan en cola en el sistema de archivos JS y CSS de nuestro WordPress**. Son funciones muy completas que aceptan parámetros como el nombre y la ubicación física del archivo, dependencias, versión y destino donde se debe cargar el script.

## Incluir Javascript en WordPress de la manera correcta

A continuación, te mostraré cómo incluir un archivo de Javascript en nuestro WP de la manera correcta.  **En mi caso, yo he añadido el código que puedes ver más abajo al  [fichero functions.php](https://wpdirecto.com/recuperar-y-cambiar-contrasena-a-traves-de-functions-php/)  de mi tema**. Este código incluirá un fichero js que me ayudará a validar los campos de mi formulairio de contacto.  **El fichero utiliza código jQuery por lo que tendré que incluir la dependencia a la librería también**.

    function incluir_javascript() {
     wp_register_script('validation_script', plugins_url('validate.js', __FILE__), array('jquery'),'3.3.1', true); 
     wp_enqueue_script('validation_script');
    } 
    add_action( 'wp_enqueue_scripts', 'incluir_javascript' );

la función cuenta con múltiples parámetros. Estos son:

-   **$handle:**  Nombre del script
-   **$src:**  Ubicación del fichero (opcional)
-   **$deps:**  Array de dependencias a ejecutar (opcional)
-   **$ver:**  Número de versión del script (opcional)
-   **$in_footer:**  Si es true, el script se pondrá en cola en el footer. Si es false, se hará en el header (opcional)

## Incluir CSS en WordPress de la manera correcta

Habiendo visto lo anterior,  **vamos a hacer lo mismo con un fichero de CSS**. Es decir, vamos a emplear el método correcto para añadir nuevos estilos en el tema que estemos utilizando actualmente. Para ello, e igual que antes, abrimos el fichero functions.php de nuestro y agregamos el siguiente código:

    function incluir_css() { 
    wp_register_style('styles', plugins_url('styles.css', __FILE__)); 
    wp_enqueue_style('styles');
    } 
    add_action( 'wp_enqueue_scripts', 'incluir_css' );

Por supuesto, modifica los parámetros de la función wp_register_style en base a tus necesidades. Ojo, los parámetros son los mismos que he explicado anteriormente.
