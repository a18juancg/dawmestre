﻿
# Depuración y verificación del funcionamiento.

Depurar códigos web no es del todo cómodo. Un error de codificación en la página y ésta queda totalmente en blanco.

Para resolver esto podemos:

-   Habilitar los avisos de error de  PHP **solo mientras probamos**  (`E_ALL`). No es siempre posible.
-   Ir dejando marcas, usando la función  `echo “Marca 1”;`  con mensajes indicativos de por donde avanza la ejecución para determinar donde se corta.
-   Herramientas para el navegador web Firefox que nos pueden ayudar a depurar errores. Encontrarás un enlace al respecto al pie de esta sección.
-   Quitar la arroba (@) que impide el envío de errores de delante del nombre de la función, si alguna lo tiene. Luego no debemos olvidar volvérselo a poner.

**Al finalizar las pruebas volveremos a poner la @ y deshabilitar la opción  E_ALL  de nuestro código por razones de seguridad.**

En ocasiones el error no está en la línea que nos marca el mensaje, sino en la anterior; por lo que hay que buscar también en ellas.

Antes de proceder a la publicación final del sitio es necesario comprobar el correcto funcionamiento de todos los componentes del mismo, para lo que siempre es útil realizar una lista de comprobación en la que vayamos registrando cada componente y cada módulo que hayamos instalado. En función de la complejidad del sitio la lista de comprobación será más o menos larga y es posible que en un sitio sencillo podamos prescindir de ella cuando tengamos mucha experiencia, lo que no es nada recomendable.

Hay que comprobar que la navegación, los enlaces, objetos y los distintos módulos instalados funcionan a la perfección. Lo mismo con todos los elementos gráficos y multimedia.

**No nos limitaremos sólo al fragmento involucrado en la modificación**  que hayamos realizado, porque se puede ver afectado alguna otra variable o sección que hayamos alterado sin percatarnos. Debemos ser eficientes y profesionales, lo que supone ser exhaustivo en las pruebas.

La verificación de funcionamiento debe realizarse empleando, sino todos, por lo menos los navegadores web más habituales:  Firefox,  Internet Explorer  (las versiones actuales),  Chrome, Safari,  Opera,  etc.

