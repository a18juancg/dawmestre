﻿***ADAPTACION CMS: APARIENCIA E FUNCIONALIDADE***

Aprenderanse os seguintes conceptos e manexo de destrezas:

-   Identificar a estrutura de directorios de temas en WordPress.
    
-   Coñecer o funcionamento e necesidade de cada un dos directorios e arquivos do tema.
    
-   Crear temas fillos a partir de temas xa existentes.
    
-   Modificar os temas fillos.
    
-   Modificar os estilos nos temas fillos.
    
-   Engadir nova funcionalidade nos temas fillos.


1.  **Estrutura do directorio de temas**
    

Os temas son arquivos e estilos que funcionan en conxunto para crear a presentación dun sitio en WordPress. O uso de temas, permite separar os estilos de presentación e os arquivos de tema do sistema, permitindo ao sitio actualizar a versións novas de WordPress sen cambios drásticos na presentación visual. Ademais, tamén permite a personalización da presentación, converténdoa en algo único.

O conxunto de arquivos que conforman un tema almacénase nun directorio e este directorio á súa vez está almacenado noutro directorio dentro do directorio de instalación de WordPress. É dicir, todos os temas atópanse baixo o seguinte directorio:
*/dir_instalacion_WordPress/wp-content/themes/*

**Estrutura xeral**

WordPress baséase en tres tipos principais de arquivos ademais das imaxes:

-   Folla de estilos: denominada style.css, a cal controla a presentación, é dicir, o estilo, das páxinas web.
    
-   Arquivo opcional de funcións: denominado functions.php, que funciona como un, ou unha serie, de plugins para o tema.
    
-   Arquivos de patróns de tema: estes arquivos controlan a forma na que se xera información, obtida da base de datos, para mostrala coma unha páxina web.
    

Na seguinte imaxe móstrase o contido do tema que ven configurado como o tema por defecto en WordPress, o tema _Twenty Fourteen_. Aquí podemos ver os tres tipos de arquivos que se acaban de comentar.
![enter image description here](imgs/Themes_2014.png)

**Follas de estilo**

Ademais da propia información de estilo, o arquivo style.css debe prover de detalles acerca do tema en forma de comentarios. Non está permitido que dous temas conteñan os mesmos detalles que figuran nas súas cabeceiras de comentario, xa que isto dará lugar a problemas no diálogo de selección do tema. Polo tanto, se creamos un tema da copia de outro tema existente, debemos de asegurarnos de cambiar antes de nada esta información.

A seguinte imaxe é un exemplo das primeiras liñas da folla de estilo, é dicir da cabeceira da folla de estilo, do tema _Twenty Fourteen_.
![enter image description here](imgs/style_css.png)

**Arquivo de funcións**

Un tema pode usar opcionalmente o arquivo functions.php que se atopa no subdirectorio do tema. Basicamente actúa coma un plugin e se está presente no tema que está usando, é cargado automaticamente durante a inicialización de WordPress.

Recoméndase o uso deste arquivo para:

-   Definir as funcións utilizadas en varios arquivos do tema.
    
-   Establecer unha pantalla de administración, dando aos usuarios as opcións de cores, estilos e outros aspectos do tema.
    

A seguinte imaxe mostra un extracto deste arquivo para o tema _Twenty Fourteen_.
![enter image description here](imgs/funcions_php.png)

**Arquivos patróns de tema**

Os patróns son ficheiros de código PHP que se usan para xerar as páxinas solicitadas polos usuarios. WordPress permite definir diferentes patróns para os distintos aspectos do blog. Aínda así, non é esencial ter todos os diferentes arquivos de patróns para que o blog teña funcionalidade completa. Por exemplo, como caso extremo, pode usarse soamente un único arquivo patrón, chamado index.php como o patrón para todas as páxinas xeradas e mostradas polo blog.
![enter image description here](imgs/index_php.png)

Dentro destes arquivos podemos clasificalos en:

-   Patróns básicos.
    
-   Patróns baseados en consultas.
    

Patróns básicos

Como mínimo un tema debe constar de dous arquivos que están almacenados directamente no directorio do tema:

-   style.css
    
-   index.php
    

O ficheiro de patrón index.php é moi flexible, xa que pode incluír todas as referencias á cabeceira, barra lateral, pé de páxina, contido, categorías, e outras páxinas web xeradas polo usuario no sitio, ou ben estar subdividido en ficheiros de patróns modulares, onde cada un asume un parte da carga de traballo.

Os ficheiros básicos usados normalmente na subdivisión, e que están almacenados no directorio do tema, son:

-   header.php
    
-   sidebar.php
    
-   footer.php
    
-   comments.php
    
-   comments-popup.php

Patróns baseados en consultas

WordPress pode cargar patróns diferentes para distintos tipos de consultas. Hai dúas formas para facer isto:

-   Usar a xerarquía de patróns: neste caso hai que prover arquivos de patróns de propósito especial que automaticamente serán usadas para sobreescribir index.php. Por exemplo, se o tema prové dun patrón chamado category.php e se se está a procurar dunha categoría, entón category.php será cargado en lugar de index.php. Se category.php non está presente, entón index.php será usado como de costume.
    
-   Usar Etiquetas Condicionais: basicamente comproba se unha determinada condición é verdadeira dentro da lóxica principal do programa de WordPress, entón pódese cargar un patrón particular ou poñer algún texto particular na pantalla baseado na condición.
    

Por exemplo, para xerar unha folla de estilos diferente nun post se se atopou unha determinada categoría, o código podería ser como se mostra a continuación:

![enter image description here](imgs/category.png)
