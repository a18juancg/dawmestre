# Configuración do servidor de ficheiros `vsftpd`

## Configuración de `vsftpd`

A configuración de vsftpd faise mediante a modificación do ficheiro /etc/vsftpd.conf, este ficheiro está composto de directivas, cada unha nunha liña co seguinte formato:

```
<directiva>=<valor>
```

Sen espazos entre a directiva, o signo igual e o valor.

## Opcións de conexión e control de acceso

### anonymous_enable

Utilízase para definir se se permitirán os accesos anónimos ao servidor. Establecerase o valor YES ou NO de acordo ao que se requira.

```
anonymous_enable=YES
```

### local_enable

Establece se se van a permitir os accesos autenticados dos usuarios locais do sistema. Establecerase o valor YES ou NO de acordo ao que se requira.

```
local_enable=YES
```


### ftpd_banner

Esta opción vén incluída na configuración predeterminada. Serve para establecer a mensaxe de benvida que será amosada cada vez que un usuario acceda ao servidor. Pode establecerse calquera frase breve, pero sen signos de puntuación.

```
ftpd_banner=Benvido ao servidor FTP da nosa empresa 
```

## Control de acceso por usuarios

Para indicar que un conxunto de usuarios poidan ou non acceder ao servidor FTP se empregan en conxunto as directivas `userlist_enable`, `userlist_deny` e `userlist_file`. 

* `userlist_file` indica o ficheiro no que se listan os usuarios aos que se lles vai permitir ou denegar o acceso.

* `userlist_enable` indica se a funcionalidade está ou non activada. Por defecto toma o valor NO.

* `userlist_deny` indica se os usuarios que se listan no ficheiro van ser aceptados ou rexeitados, sempre que `userlist_enable` tome e valor `YES`. Se `userlist_deny` toma o valor `YES`, aos usuarios que aparecen no ficheiro se lles denegará o acceso (todos os demais terán acceso); se toma o valor `NO`, a ditos usuarios se lles permitirá o acceso (a todos os demais lles será denegado o acceso).

	Por exemplo:
	
```
userlist_enable=YES
userlist_deny=NO
userlist_file=/etc/usuarios_ftp_permitidos
```
	
Aos usuarios listados no arquivo `/etc/usuarios_ftp_permitidos` se lles permitirá o acceso, mentres que a todos os demais usuarios do sistema se lles denegará.

## Permisos de escritura

### `write_enable`

Establece se se permite a escritura no servidor, pode tomar os valores YES ou NON.

```
write_enable=YES
```

O directorio raíz dun usuario (é dicir, o de maior nivel ao que pode acceder) non pode ter permisos de escritura, en caso contrario, o servidor non iniciará.

### `anon_upload_enable`

Especifica se os usuarios anónimos teñen permitido subir contido ao servidor. Polo xeral non é unha función desexada, polo que é habitual que estea desactivada.

```
anon_upload_enable=NON
```

### `anon_mkdir_write_enable`

Especifica se os usuarios anónimos terán permitido crear directorios no servidor. Do mesmo xeito que a anterior, polo xeral non é unha función desexada, polo que é habitual desactivala.

```
anon_mkdir_write_enable=NON
```

## `anon_root`
Especifica o directorio ao cal ten acceso o usuario anónimo.

```
anon_root=/ftp
```

Por defecto, o directorio ao que acceden os usuarios anónimos é `/srv/ftp`.

## Opcións `chroot`

Un grave problema de seguridade dos servidores FTP é que os usuarios locais teñen acceso a toda a árbore de directorios do servidor (aínda que non teñan permiso de escritura). Para evitar iso se "engaiola" aos usuarios no seu directorio home a través de chroot.
chroot en Sistemas Operativos Unix é unha operación que cambia o directorio raíz por defecto para o programa que se está executando e todos os seus fillos. Un programa executado neste tipo de contorno non pode acceder a ficheiros externos a ese directorio.

As opcións son:

* `chroot_local_user`: Engaiola aos usuarios no seu directorio persoal.

* `chroot_list_enable`: Habilita a lista de usuarios que son excluídos de ser engaiolados.

* `chroot_list_file`: Indica o ficheiro coa lista de usuarios que son excluídos de ser engaiolados.

```
chroot_local_user=YES
chroot_list_enable=YES
chroot_list_file=/etc/vsftpd/chroot_list
```

É necesario crear o ficheiro indicado en `chroot_list_file` para que funcione correctamente o servidor.

Podes consultar máis opcións de configuración na [seguinte ligazón](http://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-rg-es-4/s1-ftp-vsftpd-conf.html). 

## Reconocimento

Los autores originales de este exto fueron la Profesora Amalia Falcón Docampo, la Profesora Laura Fernández Nocelo y la Profesora Marta Rey López.

![Reconocimiento y licencia](img/licencia-CSIFC03_MP0614_V000402_UD04_A02_Configuracion_FTP.png)