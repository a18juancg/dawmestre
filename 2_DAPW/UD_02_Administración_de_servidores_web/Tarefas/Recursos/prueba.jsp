<%@ page import ="java.util.Calendar" %>
<html>
  <head>
    <title>PRUEBA JSP</title>
  </head>
  <body>
    <h1>Obteniendo la longitud de una cadena</h1>
    <%
        String s1 = "Longitud de una cadena!";
          out.println("\"" + s1 + "\"" + " es de  "  + 
                 s1.length() +   " caracteres ");
     %>
    <h1>Funciones de fecha/hora</h1>
    <p>
       Hoy es <%=Calendar.getInstance().getTime()%>
    </p>
    <%
       String saludo;
       int hora= Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
       if (hora<12) {
          saludo = "Buenos días";
       } else if ( hora >= 12 && hora < 21) {
          saludo = "Buenas tardes";
       } else {
          saludo = "Buenas noches";
       }
    %>
    <p> <%=saludo%> </p>
  </body>
</html>