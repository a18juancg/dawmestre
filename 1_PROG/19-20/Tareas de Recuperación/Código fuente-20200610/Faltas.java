import java.util.Scanner;

public class P1 {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int n;
        String s, res;
        do {
            n  = cin.nextInt();
            if(n!=0) {
                cin.nextLine();
                s = cin.nextLine() + " ";
                res = "";
                for(int j=1;j<=n;j++) {
                    if(s.indexOf(j + " ")==-1) res += j + " ";
                }
                System.out.println((res.length()==0)?"SIN FALTAS":res);
            }
        } while(n!=0);
    }
}
