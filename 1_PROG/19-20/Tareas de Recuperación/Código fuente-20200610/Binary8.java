public class Binary8 {
    String num;

    Binary8(String num) { 
        this.num = "";
        for(int i=0; i<8-num.length(); i++) this.num += "0";
        this.num += num; 
    }
    Binary8(Binary8 b) { this.num = b.num; }
    
    public String toString() { return num; }
    public String getNum() { return num; }

    public void not() {
        String nuevo = "";
        for(int i=0; i<num.length(); i++) nuevo += (this.num.charAt(i) == '0')?"1":"0";
        this.num = nuevo;
    }

    public void and(Binary8 b) {
        String nuevo = "";
        for(int i=0; i<8; i++) 
            nuevo += (Integer.parseInt(""+num.charAt(i)) & Integer.parseInt(""+b.num.charAt(i)));
        this.num = nuevo;
    }    

    public void and(String n) { this.and(new Binary8(n)); }
    
    public void or(Binary8 b) {
        String nuevo = "";
        for(int i=0; i<8; i++) 
            nuevo += (Integer.parseInt(""+num.charAt(i)) | Integer.parseInt(""+b.num.charAt(i)));
        this.num = nuevo;
    }    

    public void or(String n) { this.or(new Binary8(n)); }    

    public void xor(Binary8 b) {
        String nuevo = "";
        for(int i=0; i<8; i++) 
            nuevo += (Integer.parseInt(""+num.charAt(i)) ^ Integer.parseInt(""+b.num.charAt(i)));
        this.num = nuevo;
    }    

    public void xor(String n) { this.xor(new Binary8(n)); }    

    public static void main(String[] args) {
        Binary8 b1 = new Binary8("101010");
        Binary8 b2 = new Binary8("1011001");
        System.out.println("b1: " + b1);        
        System.out.println("b2: " + b2);        
        b1.not();
        System.out.println("not b1: " + b1);
        b1.and(b2);
        System.out.println("b1 and b2: " + b1);
        b1.or(b2);
        System.out.println("b1 or b2: " + b1);
        b1.xor(b2);
        System.out.println("b1 xor b2: " + b1);
    }
}