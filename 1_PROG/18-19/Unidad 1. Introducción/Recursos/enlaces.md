## [Museo de la Historia de la Computación (video)](https://www.youtube.com/watch?v=5pthCezXX-Q)

## [TRANSISTORIZED! (video)](https://www.youtube.com/watch?time_continue=54&v=f3IUVvJ2XgI)

## [The UNIX Operating System (video)](https://www.youtube.com/watch?v=tc4ROCJYbm0)

## [Piratas de Silicon Valley (video)](https://vimeo.com/181125915)

## [Micro Men (video)](https://www.youtube.com/watch?v=LHIpCcmcFEI)

## [Código Linux (video)](https://www.youtube.com/watch?v=cwptTf-64Uo&feature=youtu.be)

## [Revolution OS (video)](https://www.youtube.com/watch?v=BboCnT9skB4)



